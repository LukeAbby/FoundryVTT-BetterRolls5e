import { getGame, waitForReady } from "./utils/readyTools";
import redDamageCritTemplate from "../templates/red-damage-crit.html";

import { BRSettings } from "./settings.js";
import { BetterRollsChatCard } from "./chat-message.js";
import { addItemSheetButtons, BetterRolls } from "./betterrolls5e.js";
import { ItemUtils, Utils } from "./utils/index.js";
import { addBetterRollsContent } from "./item-tab.js";
import { patchCoreFunctions } from "./patching/index.js";
import { migrate } from "./migration.js";

// Attaches BetterRolls to actor sheet
Hooks.on("renderActorSheet5e", (app, html, data) => {
	const triggeringElement = ".item .item-name h4";
	const buttonContainer = ".item-properties";

	// this timeout allows other modules to modify the sheet before we do
	setTimeout(async () => {
		await waitForReady();

		if (game.settings.get("betterrolls5e", "rollButtonsEnabled")) {
			await addItemSheetButtons(app.object, html, data, triggeringElement, buttonContainer);
		}
	}, 0);
});

// Attaches BetterRolls to item sheet
Hooks.on("renderItemSheet5e", async (app, html, _) => {
	await addBetterRollsContent(app, html);
});

Hooks.once("init", async () => {
	BRSettings.init();
	patchCoreFunctions();

	// Setup template partials
	await loadTemplates([redDamageCritTemplate]);
});

Hooks.on("ready", async () => {
	await migrate();

	// Make a combined damage type array that includes healing
	const { damageTypes, healingTypes } = CONFIG.DND5E;
	CONFIG.betterRolls5e.combinedDamageTypes = mergeObject(duplicate(damageTypes), healingTypes);

	// Updates crit text from the dropdown.
	let critText = BRSettings.critString;
	if (critText.includes("br5e.critString")) {
		critText = game.i18n.localize(critText);
		await game.settings.set("betterrolls5e", "critString", critText);
	}

	// Set up socket
	game.socket?.on("module.betterrolls5e", async (data) => {
		if (data?.action === "roll-sound") {
			await Utils.playDiceSound();
		}
	});

	// Initialize Better Rolls
	window.BetterRolls = BetterRolls();

	Hooks.call("readyBetterRolls");
});

// Create flags for item when it's first created
Hooks.on("preCreateItem", (item: Item5e) => ItemUtils.ensureFlags(item));

// Modify context menu for damage rolls (they break)
Hooks.on("getChatLogEntryContext", (_, options) => {
	// We can't wait here because we need to update the options synchronously
	const game = getGame();

	const contextDamageLabels = [
		game.i18n.localize("DND5E.ChatContextDamage"),
		game.i18n.localize("DND5E.ChatContextHealing"),
		game.i18n.localize("DND5E.ChatContextDoubleDamage"),
		game.i18n.localize("DND5E.ChatContextHalfDamage"),
	];

	const optionCondition = (li: JQuery) =>
		!!canvas?.tokens?.controlled.length && li.find(".dice-roll").length > 0 && li.find(".red-full").length === 0;

	for (let i = options.length - 1; i >= 0; i--) {
		const option = options[i];
		if (contextDamageLabels.includes(option.name)) {
			option.condition = optionCondition;
		}
	}
});

// Bind to any newly rendered chat cards at runtime
// For whatever reason, this callback is sometimes called with unattached html elements
Hooks.on("renderChatMessage", BetterRollsChatCard.bind);
Hooks.on("getChatLogEntryContext", BetterRollsChatCard.addOptions);

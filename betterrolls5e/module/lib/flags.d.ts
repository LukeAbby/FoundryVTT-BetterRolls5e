import type { InitializedEntry } from "../renderer";
import type { ModelFields } from "../fields";
import type { CustomItemRoll } from "../custom-roll";
import type { Optional, FullPartialDeep, FullPartial } from "./utils";

export {};

declare global {
	namespace ChatMessage5e {
		export namespace Flags {
			export type BetterRolls5e = {
				version: Optional<string>;
				actorId: Optional<string>;
				itemId: Optional<string>;
				tokenId: Optional<string>;
				isCrit: Optional<boolean>;
				entries: InitializedEntry[];
				properties: CustomItemRoll["properties"];
				params: CustomItemRoll["params"];
				fields: Optional<ModelFields[]>;
			};
		}
	}

	namespace Actor5e {
		export namespace Flags {
			export type DND5e = FullPartialDeep<{
				weaponCriticalThreshold: number | `${number}`;
				spellCriticalThreshold: number | `${number}`;

				diamondSoul: boolean;
				elvenAccuracy: boolean;
				halflingLucky: boolean;
			}>;
		}
	}

	interface FlagConfig {
		ChatMessage: {
			dnd5e?: FullPartial<{
				itemData: Item5e["data"];
				meleeCriticalDamageDice: number;

				roll: FullPartial<{
					type: DND5e.RollTypes;
					itemId: string;
				}>;
			}>;

			core: {
				canPopout: boolean;
			};

			betterrolls5e?: ChatMessage5e.Flags.BetterRolls5e;
		};

		Actor: {
			dnd5e?: Actor5e.Flags.DND5e;
		};
	}
}

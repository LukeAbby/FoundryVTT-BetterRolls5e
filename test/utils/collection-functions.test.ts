import { pick, pickBy, findLast, findLastIndex } from "../../betterrolls5e/module/utils/collection-functions";

const obj = {
	foo: 123,
	bar: {
		nested: "properties",
	},
	fortune: 10,
	favor: ["x", "y", "z"],

	undefinedProp: undefined,
	nullProp: null,
} as const;

describe("pick", () => {
	test("pick all is identity", () => {
		expect(pick(obj, Object.keys(obj))).toStrictEqual(obj);
	});

	test("pick bar prop", () => {
		expect(pick(obj, ["bar"])).toStrictEqual({
			bar: {
				nested: "properties",
			},
		});
	});

	test("picking nonexistant keys", () => {
		expect(pick(obj, ["nonexistantProp", "otherNotExist"])).toStrictEqual({});
	});

	test("pick nullish values", () => {
		expect(pick(obj, ["undefinedProp", "nullProp"])).toStrictEqual({
			undefinedProp: undefined,
			nullProp: null,
		});
	});
});

describe("pickBy", () => {
	test("pick starts with f", () => {
		expect(pickBy(obj, (_, k): k is `f${string}` => k.startsWith("f"))).toStrictEqual({
			foo: 123,
			fortune: 10,
			favor: ["x", "y", "z"],
		});
	});
});

const arr = [4, 3, 7, 11] as const;
type TestArr = typeof arr;
type ArrValue<Arr> = Arr extends readonly (infer V)[] ? V : never;
type Predicate<T extends ArrValue<TestArr>> = (value: ArrValue<TestArr>, index: number, array: TestArr) => value is T;

const shouldFindSevenPredicate: Predicate<7> = (value, index, array): value is 7 =>
	(value === 4 || value === 11 || value === 7) && index !== array.length - 1;

const shouldFindNonePredicate: Predicate<never> = (value): value is never => false;

describe("findLastIndex", () => {
	test("find index for 7", () => {
		expect(findLastIndex(arr, shouldFindSevenPredicate)).toStrictEqual(arr.indexOf(7));
	});

	test("find no index", () => {
		expect(findLastIndex(arr, shouldFindNonePredicate)).toStrictEqual(-1);
	});
});

describe("findLast", () => {
	test("find 7", () => {
		expect(findLast(arr, shouldFindSevenPredicate)).toStrictEqual(7);
	});

	test("find none", () => {
		expect(findLast(arr, shouldFindNonePredicate)).toBeUndefined();
	});
});

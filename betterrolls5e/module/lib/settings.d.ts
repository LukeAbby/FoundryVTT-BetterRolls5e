export {};

export namespace BetterRollsSettings {
	export interface Migration {
		status: boolean;
		version: string;
	}

	export type D20Mode = 1 | 2 | 3 | 4;
	export type DamagePromptEnabled = boolean;
	export type RollButtonsEnabled = boolean;
	export type ImageButtonEnabled = boolean;
	export type AltSecondaryEnabled = boolean;
	export type ApplyActiveEffects = boolean;
	export type QuickDefaultDescriptionEnabled = boolean;
	export type D20RollIconsEnabled = boolean;
	export type DefaultRollArt = string;
	export type RollTitlePlacement = string;

	export type DamageTitlePlacement = "0" | "1" | "2" | "3";
	export type DamageContextPlacement = "0" | "1" | "2" | "3";
	export type DamageRollPlacement = "0" | "1" | "2" | "3";

	export type ContextReplacesTitle = boolean;
	export type ContextReplacesDamage = boolean;

	export type CritBehavior =
		| "0" // No Extra Damage
		| "1" // Roll Critical Damage Dice
		| "2" // Roll Base Damage, Max Critical
		| "3" // Max Base & Critical Damage
		| "4"; // Max Base Damage, Roll Critical Damage

	export type CritString = string;
	export type ChatDamageButtonsEnabled =
		| "0" // Disabled
		| "1" // Enabled
		| "2"; // GM Only
	export type PlayRollSounds = boolean;
	export type HideDC =
		| "0" // Never
		| "1" // NPCs only
		| "2"; // Always
}

declare global {
	namespace ClientSettings {
		interface Values {
			"betterrolls5e.migration": BetterRollsSettings.Migration;
			"betterrolls5e.d20Mode": BetterRollsSettings.D20Mode;
			"betterrolls5e.damagePromptEnabled": BetterRollsSettings.DamagePromptEnabled;
			"betterrolls5e.rollButtonsEnabled": BetterRollsSettings.RollButtonsEnabled;
			"betterrolls5e.imageButtonEnabled": BetterRollsSettings.ImageButtonEnabled;
			"betterrolls5e.altSecondaryEnabled": BetterRollsSettings.AltSecondaryEnabled;
			"betterrolls5e.applyActiveEffects": BetterRollsSettings.ApplyActiveEffects;
			"betterrolls5e.quickDefaultDescriptionEnabled": BetterRollsSettings.QuickDefaultDescriptionEnabled;
			"betterrolls5e.d20RollIconsEnabled": BetterRollsSettings.D20RollIconsEnabled;
			"betterrolls5e.defaultRollArt": BetterRollsSettings.DefaultRollArt;
			"betterrolls5e.rollTitlePlacement": BetterRollsSettings.RollTitlePlacement;

			"betterrolls5e.damageTitlePlacement": BetterRollsSettings.DamageTitlePlacement;
			"betterrolls5e.damageContextPlacement": BetterRollsSettings.DamageContextPlacement;
			"betterrolls5e.damageRollPlacement": BetterRollsSettings.DamageRollPlacement;

			"betterrolls5e.contextReplacesTitle": BetterRollsSettings.ContextReplacesTitle;
			"betterrolls5e.contextReplacesDamage": BetterRollsSettings.ContextReplacesDamage;

			"betterrolls5e.critBehavior": BetterRollsSettings.CritBehavior;
			"betterrolls5e.critString": BetterRollsSettings.CritString;
			"betterrolls5e.chatDamageButtonsEnabled": BetterRollsSettings.ChatDamageButtonsEnabled;
			"betterrolls5e.playRollSounds": BetterRollsSettings.PlayRollSounds;
			"betterrolls5e.hideDC": BetterRollsSettings.HideDC;

			"maestro.enableItemTrack": boolean;
		}
	}
}

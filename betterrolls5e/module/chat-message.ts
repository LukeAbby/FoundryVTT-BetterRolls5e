import { waitForReady } from "./utils/readyTools";
import redOverlayHeaderTemplate from "../templates/red-overlay-header.html";
import redOverlayMultirollTemplate from "../templates/red-overlay-multiroll.html";
import redOverlayDamageTemplate from "../templates/red-overlay-damage.html";
import redOverlayDamageCritOnlyTemplate from "../templates/red-overlay-damage-crit-only.html";

import type { Optional } from "./lib/utils";
import { CustomItemRoll, CustomRoll } from "./custom-roll.js";
import { migrateChatMessage } from "./migration.js";
import { BRSettings } from "./settings.js";
import { betterRollsError, betterRollsInfo, betterRollsVerbose, getGame, i18n, Utils } from "./utils/index.js";

import {
	isCombinedDamageType,
	combinedDamageTypeErrorMessage,
	throwWithContext,
	isDND5eAbility,
	isValidRollState,
} from "./utils/guards.js";

type GetBindingsReturn = {
	_messageId: string | null;
	roll: CustomItemRoll;
	speaker: Optional<StoredDocument<Actor5e>>;
};

type ContextMenuEntry = {
	name: string;
	icon: string;
	callback: (li: JQuery) => void;
	condition?: (li: JQuery) => boolean;
};

type DialogPosition = {
	x: Optional<number>;
	y: Optional<number>;
};

/**
 * Class that encapsulates a better rolls card at runtime.
 * When a chat message enters the chat it should be bound
 * with BetterRollsChatCard.bind().
 */
export class BetterRollsChatCard {
	/** Min version to enable the card on, to prevent breakage */
	static min_version = "1.4";

	private roll: CustomItemRoll;
	private speaker: Optional<StoredDocument<Actor5e>>;
	private _messageId: string | null;

	constructor(message: ChatMessage, html: JQuery) {
		const { _messageId, roll, speaker } = this._getBindings(message);

		this._messageId = _messageId;
		this.roll = roll;
		this.speaker = speaker;

		this._initBindings(message, html);
	}

	get message() {
		const game = getGame();

		// TODO see if this can be refactored away to store like `this._message` or if it has to be updated on every get.
		return this.id != null ? game.messages?.get(this.id) : null;
	}

	get id() {
		return this._messageId;
	}

	/**
	 * Initializes data. Used in the constructor or by BetterRollsChatCard.bind().
	 * @param message - The property BetterRoll is overridden on the input message.
	 */
	private updateBinding(message: BetterRolls.MaybeInitializedChatMessage, html: JQuery) {
		// IMPLEMENTATION WARNING: DO NOT STORE html into the class properties (NO this.html AT ALL)
		// Foundry will sometimes call renderChatMessage() multiple times with un-bound HTML,
		// and we can't do anything except rely on closures to handle those events.

		const { _messageId, roll, speaker } = this._getBindings(message);

		this._messageId = _messageId;
		this.roll = roll;
		this.speaker = speaker;

		this._initBindings(message, html);
	}

	_getBindings(message: BetterRolls.MaybeInitializedChatMessage): GetBindingsReturn {
		const game = getGame();

		const speakerActor = message.data.speaker.actor;

		return {
			_messageId: message.id,
			roll: CustomItemRoll.fromMessage(message),
			speaker: speakerActor != null ? game.actors?.get(speakerActor) : null,
		};
	}

	_initBindings(message: BetterRolls.MaybeInitializedChatMessage, html: JQuery) {
		const game = getGame();

		betterRollsVerbose("initializing bindings", "\nmessage:", message, "\nhtml:", html);

		// eslint-disable-next-line no-param-reassign
		message.BetterRoll = this.roll;

		// Hide Save DCs
		const actor = this.speaker;
		if ((!actor && !game.user?.isGM) || actor?.permission !== 3) {
			html.find(".hideSave").text(i18n("br5e.hideDC.string"));
		}

		// Setup the events for card buttons (the permanent ones, not the hover ones)
		this._setupCardButtons(html);

		// Setup hover buttons on first hover (for optimization)
		// Just like with html, we cannot save hoverInitialized to the object
		let hoverInitialized = false;
		html.on("mouseenter", async () => {
			if (!hoverInitialized) {
				hoverInitialized = true;
				await this._setupOverlayButtons(html);
				this._onHover(html);

				betterRollsInfo("Hover Buttons Initialized");
			}
		});
	}

	/**
	 * Inflates an existing chat message, adding runtime elements and events to it. Does nothing if the message is not the correct type.
	 * @param message - The property BetterRolls and BetterRollsCardBinding is used and overridden otherwise on the input message.
	 */
	static async bind(
		message: BetterRolls.MaybeInitializedChatMessage,
		html: JQuery
	): Promise<BetterRollsChatCard | null> {
		await waitForReady();

		const chatCard = html.find(".red-full");
		if (chatCard.length === 0) {
			return null;
		}

		// If the card needs to be migrated, skip the binding
		if (await migrateChatMessage(message)) {
			return null;
		}

		// Check if the card already exists
		const existingBinding = message.BetterRollsCardBinding;
		if (existingBinding != null) {
			betterRollsInfo("Retrieved existing card");
			existingBinding.updateBinding(message, chatCard);

			// Pulse the card to make it look more obvious
			// Wait for the event queue before doing so to allow CSS calculations to work,
			// otherwise the border color will be incorrectly transparent
			setTimeout(() => {
				// @ts-expect-error while re-exporting gsap globally would be ideal this is blocked by the fact that Draggable is defined globally in foundry AND gsap.
				// See: https://gitlab.com/foundrynet/foundryvtt/-/issues/6543
				gsap.from(html.get(), {
					"border-color": "red",
					"box-shadow": "0 0 6px inset #ff6400",
					duration: 2,
				});
			}, 0);

			// Scroll to bottom if the last card had updated
			const last = game.messages?.contents[game.messages.size - 1];
			if (last?.id === existingBinding.id) {
				setTimeout(() => {
					// @ts-expect-error ts(2445) expect an error because we're accessing `scrollBottom`.
					ui.chat.scrollBottom();
				}, 0);
			}

			return existingBinding;
		}

		const newCard = new BetterRollsChatCard(message, chatCard);

		// eslint-disable-next-line no-param-reassign
		message.BetterRollsCardBinding = newCard;

		return newCard;
	}

	/**
	 * Adds right click menu options
	 */
	static addOptions(_: JQuery, options: ContextMenuEntry[]) {
		const game = getGame();

		const getBinding = (li: JQuery) => {
			const message = game.messages?.get(li.data("messageId")) as BetterRolls.ChatMessage;
			return message?.BetterRollsCardBinding;
		};

		options.push({
			name: i18n("br5e.chatContext.repeat"),
			icon: '<i class="fas fa-redo"></i>',
			condition: (li: JQuery) => {
				const binding = getBinding(li);
				return !!binding?.roll.canRepeat();
			},
			callback: async (li: JQuery) => await getBinding(li)?.roll.repeat({ event }),
		});
	}

	/**
	 * Internal method to setup the temporary buttons used to update advantage or disadvantage,
	 * as well as those that that affect damage
	 * entries, like crit rolls and damage application.
	 */
	private async _setupOverlayButtons(html: JQuery) {
		// Add reroll button
		if (this.roll?.canRepeat()) {
			const templateHeader = await renderTemplate(redOverlayHeaderTemplate, {});
			html.find(".card-header").append($(templateHeader));
		}

		// Multiroll buttons (perhaps introduce a new toggle property?)
		if (this.roll) {
			const templateMulti = await renderTemplate(redOverlayMultirollTemplate, {});

			// Add multiroll overlay buttons to the DOM.
			for (const entry of this.roll.entries) {
				if (entry.type === "multiroll" && !entry.rollState && entry.entries?.length === 1) {
					const element = html.find(`.red-dual[data-id=${entry.id}] .dice-row.red-totals`);
					element.append($(templateMulti));
				}
			}

			// Handle clicking the multi-roll overlay buttons
			html.find(".multiroll-overlay-br button").on("click", async (event) => {
				event.preventDefault();
				event.stopPropagation();

				const button = event.currentTarget;
				const id = $(button).parents(".red-dual").attr("data-id");
				const { action } = button.dataset;
				if (action === "rollState") {
					const rollState = button.dataset.state;

					if (!isValidRollState(rollState)) {
						betterRollsError(
							`Expected all elements matching the selector ".multiroll-overlay-br button" to have a valid "data-state" rollstate! Violating element:`,
							button
						);

						return;
					}

					if (await this.roll.updateRollState(id, rollState)) {
						await this.roll.update();
					}
				}
			});
		}

		// Setup augment crit and apply damage button
		const templatePath = BRSettings.chatDamageButtonsEnabled
			? redOverlayDamageTemplate
			: redOverlayDamageCritOnlyTemplate;
		const templateDamage = await renderTemplate(templatePath, {});
		const dmgElements = html
			.find(".dice-total .red-base-die, .dice-total .red-extra-die")
			.parents(".dice-row")
			.toArray();
		const customElements = html.find("[data-type=custom] .red-base-die").toArray();

		// Add chat damage buttons
		[...dmgElements, ...customElements].forEach((htmlElement) => {
			const element = $(htmlElement);
			element.append($(templateDamage));

			// Remove crit button if already rolled
			// TODO: Move this elsewhere. There's a known bug when crit settings are changed suddenly
			// If Crit (setting) is disabled, then re-enabled, crit buttons don't get re-added
			const id = element.parents(".dice-roll").attr("data-id");
			const entry = this.roll?.getEntry(id);
			if (entry != null && !this.roll?.canCrit(entry)) {
				element.find(".crit-button").remove();
			}
		});

		// Handle apply damage overlay button events
		html.find(".apply-damage-buttons button").on("click", async (ev) => {
			await waitForReady();

			ev.preventDefault();
			ev.stopPropagation();

			// find out the proper dmg thats supposed to be applied
			const dmgElement = $(ev.target).parent().parent().parent().parent();
			const damageAttr = dmgElement.find(".dice-total").attr("data-damagetype");
			const damageType = damageAttr === "" || typeof damageAttr === "undefined" ? undefined : damageAttr;

			if (typeof damageType !== "undefined" && !isCombinedDamageType(damageType)) {
				throwWithContext({
					message: [
						`The attribute "data-damagetype" was not valid on a ".dice-total" element.`,
						combinedDamageTypeErrorMessage(damageType),
					],
					context: ["Element with incorrect attribute:", dmgElement],
					errorConstructor: TypeError,
				});
			}

			let dmg: Optional<number> = Number(dmgElement.find(".red-base-die").text());

			if (dmgElement.find(".red-extra-die").length > 0) {
				const critDmg = dmgElement.find(".red-extra-die").text();
				const dialogPosition: DialogPosition = {
					x: ev.originalEvent?.screenX,
					y: ev.originalEvent?.screenY,
				};

				dmg = await this._resolveCritDamage(dmg, Number(critDmg), dialogPosition);
			}

			// getting the modifier depending on which of the buttons was pressed
			const modifier = Number($(ev.target).closest("button").attr("data-modifier"));

			// applying dmg to the targeted token and sending only the span that the button sits in
			for (const actor of Utils.getTargetActors() ?? []) {
				await this.applyDamage(actor, damageType, dmg ?? 0, modifier);
			}

			setTimeout(() => {
				// @ts-expect-error I have no idea where this property is coming from.
				const displayState = canvas.hud?.token._displayState;

				if (displayState != null && displayState !== 0) {
					canvas?.hud?.token.render();
				}
			}, 50);
		});

		// Handle crit button application event
		html.find(".crit-button")
			.off()
			.on("click", async (ev) => {
				ev.preventDefault();
				ev.stopPropagation();
				const group = $(ev.target).parents(".dice-roll").attr("data-group");

				const couldForce = await this.roll.forceCrit(group);
				betterRollsVerbose(
					"Forcing crit:",
					"\ntarget:",
					ev.currentTarget,
					"\ngroup:",
					group,
					"\ncould force:",
					couldForce
				);

				if (couldForce) {
					await this.roll.update();
				}
			});

		// Enable Hover Events (to show/hide the elements)
		this._onHoverEnd(html);
		html.on("mouseenter", this._onHover.bind(this, html));
		html.on("mouseleave", this._onHoverEnd.bind(this, html));
	}

	async applyDamage(
		actor: Actor5e,
		damageType: BetterRolls.CombinedDamageTypes | undefined,
		damage: number,
		modifier: number
	) {
		if (damageType === "temphp" && modifier < 0) {
			const healing = Math.abs(modifier) * damage;
			const actorData = actor.data.data;
			if (actorData.attributes.hp.temp > 0) {
				const overwrite = await Dialog.confirm({
					title: i18n("br5e.chat.damageButtons.tempOverwrite.title"),
					content: i18n("br5e.chat.damageButtons.tempOverwrite.content", {
						original: actorData.attributes.hp.temp,
						new: healing,
					}),
				});

				if (!overwrite) return;
			}

			await actor.update({ "data.attributes.hp.temp": healing });
		} else {
			await actor.applyDamage(damage, modifier);
		}
	}

	_onHover(html: JQuery) {
		const { hasPermission } = this.roll;
		html.find(".die-result-overlay-br").show();

		// Apply Damage / Augment Crit
		const controlled = (canvas?.tokens?.controlled.length ?? -1) > 0;
		html.find(".multiroll-overlay-br").toggle(hasPermission);
		html.find(".crit-button").toggle(hasPermission);
		html.find(".apply-damage-buttons").toggle(controlled);
	}

	_onHoverEnd(html: JQuery) {
		html.find(".die-result-overlay-br").attr("style", "display: none;");
	}

	/**
	 * Displays a dialog if both dmg and critdmg have a value, otherwise just returns the first not null one.
	 */
	private async _resolveCritDamage(
		dmg: Optional<number>,
		critdmg: Optional<number>,
		position: DialogPosition
	): Promise<Optional<number>> {
		// TODO check how dmg = 0 and critdmg = 0 are meant to go through these cases
		if (dmg && critdmg) {
			return await new Promise((resolve, _) => {
				const options = {
					left: position.x,
					top: position.y,
					width: 100,
				};

				const data = {
					title: i18n("br5e.chat.damageButtons.critPrompt.title"),
					content: "",
					buttons: {
						one: {
							icon: '<i class="fas fa-check"></i>',
							label: i18n("br5e.chat.damageButtons.critPrompt.yes"),
							callback: () => {
								resolve(dmg + critdmg);
							},
						},
						two: {
							icon: '<i class="fas fa-times"></i>',
							label: i18n("br5e.chat.damageButtons.critPrompt.no"),
							callback: () => {
								resolve(dmg);
							},
						},
					},
					default: "two",
				};

				new Dialog(data, options).render(true);
			});
		}

		return dmg || critdmg;
	}

	/**
	 * Bind card button events. These are the clickable action buttons.
	 */
	private _setupCardButtons(html: JQuery) {
		html.find(".card-buttons").off();
		html.off().on("click", async (event) => {
			const button = event.target.closest("button");
			if (button == null) {
				return;
			}

			event.preventDefault();
			button.disabled = true;

			const { action } = button.dataset;
			try {
				await this._performAction(action, button, event);
			} finally {
				// Re-enable the button after a delay
				setTimeout(() => {
					button.disabled = false;
				}, 0);
			}
		});
	}

	async _performAction(action: string | undefined, button: HTMLButtonElement, event: BasicEvent = {}): Promise<void> {
		await waitForReady();

		const data = button.dataset;

		if (action === "save") {
			const actors = Utils.getTargetActors({ required: true }) ?? [];
			const { ability } = data;
			if (!isDND5eAbility(ability)) {
				betterRollsError(
					`Expected all card buttons to have a valid D&D 5e Ability! Got data-ability=${JSON.stringify(
						ability
					)} Violating element:`,
					button
				);

				return;
			}

			const params = Utils.eventToAdvantage(event);
			for (const actor of actors) {
				await CustomRoll.rollAttribute(actor, ability, "save", params);
			}
		} else if (action === "damage") {
			const group = encodeURIComponent(data.group ?? "");
			if (await this.roll.rollDamage(group)) {
				await this.roll.update();
			}
		} else if (action === "repeat") {
			await this.roll.repeat({ event });
		} else if (action === "apply-active-effects") {
			if (window.DAE == null) {
				ui.notifications.warn(i18n("br5e.error.noDAE"));

				return;
			}

			const { roll } = this;
			let item: Optional<Item5e> = await roll.getItem();
			if (data.ammo) {
				const target = item?.data.data.consume?.target;
				item = target != null ? (item?.parent?.items.get(target) as Item5e) : null;
			}

			let targets: Optional<Array<Token5e | StoredDocument<Actor5e>> | UserTargets>;
			if (item?.data.data?.target?.type === "self" && canvas?.tokens?.controlled?.length) {
				targets = Utils.getTargetTokens();
			} else {
				targets = game.user?.targets.size != null ? game.user?.targets : Utils.getTargetTokens();
			}

			window.DAE.doEffects(item, true, targets, {
				whisper: false,
				spellLevel: roll.params.slotLevel,
				damageTotal: roll.totalDamage,
				critical: roll.isCrit,
				itemCardId: this.id,
			});
		}
	}
}

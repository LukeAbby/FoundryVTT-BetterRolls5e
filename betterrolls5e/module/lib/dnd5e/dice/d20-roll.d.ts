export type AdvantageMode = "advantage" | "disadvantage" | "none";

export type D20RollOptions = Partial<{
	/** What advantage modifier to apply to the roll (none, advantage, disadvantage) */
	advantageMode: AdvantageMode;

	/** The value of d20 result which represents a critical success */
	critical: number;

	/** The value of d20 result which represents a critical failure */
	fumble: number;

	/** Assign a target value against which the result of this roll should be compared */
	targetValue: number;

	/** Allow Elven Accuracy to modify this roll? */
	elvenAccuracy?: boolean;

	/** Allow Halfling Luck to modify this roll? */
	halflingLucky?: boolean;

	/** Allow Reliable Talent to modify this roll? */
	reliableTalent?: boolean;
}>;

export type ConfigureDialogData = Partial<{
	/** @param title - The title of the shown dialog window */
	title: string;

	/** @param defaultRollMode - The roll mode that the roll mode select element should default to */
	defaultRollMode: number;

	/** @param defaultAction - The button marked as default */
	defaultAction: typeof D20Roll.ADV_MODE[keyof typeof D20Roll.ADV_MODE];

	/** @param chooseModifier - Choose which ability modifier should be applied to the roll? */
	chooseModifier: boolean;

	/** @param defaultAbility - For tool rolls, the default ability modifier applied to the roll */
	defaultAbility: string;

	/** @param template - A custom path to an HTML template to use instead of the default */
	template: string;
}>;

/**
 * A type of Roll specific to a d20-based check, save, or attack roll in the 5e system.
 */
export default class D20Roll<D extends object> extends Roll<D> {
	/**
	 * @param formula - The string formula to parse
	 * @param data - The data object against which to parse attributes within the formula
	 * @param options - Extra optional arguments which describe or modify the D20Roll
	 */
	constructor(formula: string, data: D, options: D20RollOptions);

	/* -------------------------------------------- */

	/**
	 * Advantage mode of a 5e d20 roll
	 */
	static ADV_MODE: {
		NORMAL: 0;
		ADVANTAGE: 1;
		DISADVANTAGE: -1;
	};

	/**
	 * The HTML template path used to configure evaluation of this Roll
	 */
	static EVALUATION_TEMPLATE: string;

	/* -------------------------------------------- */

	/**
	 * A convenience reference for whether this D20Roll has advantage
	 */
	get hasAdvantage(): boolean;

	/**
	 * A convenience reference for whether this D20Roll has disadvantage
	 */
	get hasDisadvantage(): boolean;

	/* -------------------------------------------- */
	/*  D20 Roll Methods                            */
	/* -------------------------------------------- */

	/**
	 * Apply optional modifiers which customize the behavior of the d20term
	 */
	private configureModifiers(): void;

	/* -------------------------------------------- */

	/* -------------------------------------------- */
	/*  Configuration Dialog                        */
	/* -------------------------------------------- */

	/**
	 * Create a Dialog prompt used to configure evaluation of an existing D20Roll instance.
	 * @param data - Dialog configuration data
	 * @param options - Additional Dialog customization options
	 * @returns A resulting D20Roll object constructed with the dialog, or null if the dialog was closed
	 */
	configureDialog(
		{ title, defaultRollMode, defaultAction, chooseModifier, defaultAbility, template }: ConfigureDialogData,
		options: Dialog.Options
	): Promise<this | null>;

	/* -------------------------------------------- */

	/**
	 * Handle submission of the Roll evaluation configuration Dialog
	 * @param html - The submitted dialog content
	 * @param advantageMode - The chosen advantage mode
	 * @returns This damage roll.
	 */
	private _onDialogSubmit(html: JQuery, advantageMode: AdvantageMode): this;
}

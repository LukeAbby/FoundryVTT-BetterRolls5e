type WrapperTypes = typeof libWrapper.WRAPPER | typeof libWrapper.MIXED | typeof libWrapper.OVERRIDE;
type PerfTypes = typeof libWrapper.PERF_AUTO | typeof libWrapper.PERF_FAST | typeof libWrapper.PERF_AUTO;

type LibWrapperOptions = Partial<{
	chain: boolean;
	perf_mode: PerfTypes;
}>;

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export type LibWrappedFunction<F extends AnyFunction> = (wrapped: F, ...args: any[]) => any;

export type LibWrapUnwrap<G extends LibWrappedFunction<F>, F extends AnyFunction = AnyFunction> = G extends (
	wrapped: F,
	...args: infer Args
) => infer Return
	? (...args: Args) => Return
	: never;

// The class name is out of our control.
// eslint-disable-next-line @typescript-eslint/naming-convention
export declare class libWrapper {
	public static WRAPPER: "WRAPPER";
	public static MIXED: "MIXED";
	public static OVERRIDE: "OVERRIDE";

	public static PERF_NORMAL: "NORMAL";
	public static PERF_FAST: "FAST";
	public static PERF_AUTO: "AUTO";

	public static is_fallback: boolean;

	static version_at_least(major: number, minor: number, patch: number): boolean;

	static register<F extends AnyFunction>(
		module: string,
		target: string,
		fn: LibWrappedFunction<F>,
		type?: WrapperTypes,
		options?: LibWrapperOptions
	): number;
}

let isReady = false;

// We only want one promise to be created and we want it to start resolving immediately.
const _waitForReady = new Promise<void>((resolve) => {
	if ("ready" in game && game?.ready) {
		isReady = true;
		resolve();

		return;
	}

	Hooks.once("ready", () => {
		isReady = true;
		resolve();
	});
});

export const waitForReady = () => _waitForReady;

const hasNotInitialized = `seems to not yet have been initialized! This likely indicates that a UI has interacted with earlier than expected.`;

export function getGame(): Game {
	if (game instanceof Game) {
		return game;
	}

	throw new Error(`game ${hasNotInitialized}`);
}

export function assertReady() {
	if (!isReady) {
		throw new Error(`The world ${hasNotInitialized}`);
	}
}

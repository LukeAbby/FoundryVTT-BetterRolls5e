/* eslint-disable no-console */

declare const BUILD_REPLACES_VERBOSE: boolean;

export function betterRollsVerbose(...args: OptionalParams): void {
	if (BUILD_REPLACES_VERBOSE) {
		betterRollsInfo("Verbose log:", ...args);
	}
}

export function betterRollsInfo(...args: OptionalParams): void {
	betterRollsLog("info", ...args);
}

export function betterRollsWarn(...args: OptionalParams): void {
	betterRollsLog("warn", ...args);
}

export function betterRollsError(...args: OptionalParams): void {
	betterRollsLog("error", ...args);
}

type OptionalParams = Parameters<typeof console.log> extends [message?: unknown, ...optionalParams: infer O]
	? O
	: never;

function betterRollsLog<L extends "info" | "warn" | "error">(level: L, ...args: OptionalParams) {
	console[level](`%cBetter Rolls 5e%c |`, "color: #F1C40F;", "", ...args);
}

export function logDeprecated(deprecatedFunction: string): void {
	betterRollsLog("warn", `WARNING: ${deprecatedFunction} is deprecated!`);
}

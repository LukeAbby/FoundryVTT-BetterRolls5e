import { getSettings } from "../settings.js";
import { libWrapper, type LibWrapUnwrap, type LibWrappedFunction } from "./libWrapper.js";

import { i18n, toInt, Utils } from "../utils/index.js";
import { CustomRoll } from "../custom-roll.js";
import { throwWithContext } from "../utils/guards.js";

import type { CustomItemRoll } from "../custom-roll";
import type { ExtendedOptions, RollOptions, SourceFunctions } from "./baseTypes.js";
import type { D20RollOptions } from "../lib/dnd5e/dice5e";

type ItemRollOptions = Partial<
	ExtendedOptions<RollOptions> & {
		configureDialog: boolean;
		createMessage: boolean;
		event: BasicEvent;
	}
>;

/**
 * Override for Item5e.roll(). This is an OVERRIDE however we still want
 * a passthrough. We need to be lower on priority than Midi.
 * @returns
 */
async function itemRoll(this: Item5e, defaultRoll: SourceFunctions.DND5e.Item.Roll, options: ItemRollOptions) {
	// Handle options, same defaults as core 5e
	const fullOptions: ItemRollOptions = mergeObject(
		{
			configureDialog: true,
			createMessage: true,
			event,
		},
		options,
		{ recursive: false }
	);
	const { rollMode, createMessage, vanilla } = fullOptions;
	const altKey = fullOptions.event?.altKey;

	// Case - If the image button should roll a vanilla roll, UNLESS vanilla is defined and is false
	const { imageButtonEnabled, altSecondaryEnabled } = getSettings();
	if (vanilla || (!imageButtonEnabled && vanilla !== false) || (altKey && !altSecondaryEnabled)) {
		return await defaultRoll.call(this, fullOptions);
	}

	const preset = altKey ? 1 : 0;
	const card = window.BetterRolls.rollItem(this, { preset, event: fullOptions.event });
	return await card.toMessage({ rollMode, createMessage });
}

export type ItemRoll = LibWrapUnwrap<typeof itemRoll>;

/**
 * Override for Item5e.rollAttack(). Only kicks in if options.chatMessage is off.
 * It is basically an exact copy of the built in rollAttack, except that it doesn't consume ammo.
 * Unfortunately D&D does not allow a rollAttack() to not consume ammo.
 * @returns
 */
export async function itemRollAttack<D extends Item5eRollData>(
	this: Item5e,
	defaultRoll: SourceFunctions.DND5e.Item.RollAttack<D>,
	options: Partial<ExtendedOptions<D20RollOptions<D>>>
) {
	// Call the default version if chatMessage is enabled
	if (options?.chatMessage !== false) {
		return await defaultRoll.call(this, options);
	}

	const flags = this.actor.data.flags.dnd5e ?? {};
	if (!this.hasAttack) {
		throwWithContext({
			message: ["The provided item does not have an attack and so an attack roll cannot be placed."],
			context: ["Item:", this],
		});
	}
	// We know this can't be null because we throw an error if `this.hasAttack` is false.
	// eslint-disable-next-line @typescript-eslint/no-non-null-assertion
	const result = this.getAttackToHit()!;

	const title = `${this.name} - ${game.i18n.localize("DND5E.AttackRoll")}`;

	// get the parts and rollData for this item's attack

	const { parts, rollData } = result;

	// Compose roll options
	const rollConfig = mergeObject(
		{
			parts,
			actor: this.actor,
			data: rollData,
			title,
			flavor: title,
			dialogOptions: {
				width: 400,
				top: options.event != null ? (options.event.clientY ?? 0) - 80 : null,
				left: window.innerWidth - 710,
			},
			messageData: {
				speaker: ChatMessage.getSpeaker({ actor: this.actor }),
				"flags.dnd5e.roll": { type: "attack", itemId: this.id },
			},
		},
		options,
		{ recursive: true }
	) as D20RollOptions<typeof rollData>;

	rollConfig.event = options.event;

	// Expanded critical hit thresholds
	if (this.data.type === "weapon" && flags.weaponCriticalThreshold) {
		rollConfig.critical = toInt(flags.weaponCriticalThreshold);
	} else if (this.data.type === "spell" && flags.spellCriticalThreshold) {
		rollConfig.critical = toInt(flags.spellCriticalThreshold);
	}

	// Elven Accuracy
	if (["weapon", "spell"].includes(this.data.type)) {
		// includes is not bivariant so we broaden `this.abilityMod` to allow it to compile.
		// eslint-disable-next-line @typescript-eslint/no-explicit-any
		if (flags.elvenAccuracy && ["dex", "int", "wis", "cha"].includes(this.abilityMod as any)) {
			rollConfig.elvenAccuracy = true;
		}
	}

	// Apply Halfling Lucky
	if (flags.halflingLucky) {
		rollConfig.halflingLucky = true;
	}

	// Invoke the d20 roll helper
	const roll = await game.dnd5e.dice.d20Roll(rollConfig);
	return roll;
}

export type ItemRollAttack = LibWrapUnwrap<typeof itemRollAttack>;

async function itemRollToolCheck<D extends Item5eRollData>(
	this: Item5e,
	original: SourceFunctions.DND5e.Item.RollToolCheck<D>,
	options: Partial<ExtendedOptions<D20RollOptions<D>>>
) {
	if (options?.chatMessage === false || options?.vanilla) {
		return await original.call(this, options);
	}

	const evt = options?.event ?? (event as BasicEvent | undefined);
	const preset = evt?.altKey ? 1 : 0;
	const card = window.BetterRolls.rollItem(this, { preset, ...options });
	return await card.toMessage();
}

export type ItemRollToolCheck = LibWrapUnwrap<typeof itemRollToolCheck>;

async function actorRollSkill<D extends Actor5eRollData>(
	this: Actor5e,
	original: SourceFunctions.DND5e.Actor.RollSkill<D>,
	skillId: DND5e.Skills,
	options: Partial<ExtendedOptions<D20RollOptions<D>>>
): Promise<AsyncReturnType<typeof original> | CustomItemRoll> {
	if (options?.chatMessage === false || options?.vanilla) {
		return await original.call(this, skillId, options);
	}

	const roll = await original.call(this, skillId, {
		...options,
		fastForward: true,
		chatMessage: false,
		...Utils.getAdvantage(options),
	});

	return await CustomRoll._fullRollActor(this, i18n(CONFIG.DND5E.skills[skillId]), roll);
}

export type ActorRollSkill = LibWrapUnwrap<typeof actorRollSkill>;
export type ActorRollSkillOutput = AsyncReturnType<typeof CustomRoll._fullRollActor>;

async function actorRollAbilityTest<D extends Actor5eRollData>(
	this: Actor5e,
	original: SourceFunctions.DND5e.Actor.RollAbilityTest<D>,
	ability: DND5e.Abilities,
	options: Partial<ExtendedOptions<D20RollOptions<D>>>
) {
	if (options?.chatMessage === false || options?.vanilla) {
		return await original.call(this, ability, options);
	}

	const roll = await original.call(this, ability, {
		...options,
		fastForward: true,
		chatMessage: false,
		...Utils.getAdvantage(options),
	});

	const label = `${i18n(CONFIG.DND5E.abilities[ability])} ${i18n("br5e.chat.check")}`;
	return await CustomRoll._fullRollActor(this, label, roll);
}

export type ActorRollAbilityTest = LibWrapUnwrap<typeof actorRollAbilityTest>;
export type ActorRollAbilityTestOutput = AsyncReturnType<typeof CustomRoll._fullRollActor>;

async function actorRollAbilitySave<D extends Actor5eRollData>(
	this: Actor5e,
	original: SourceFunctions.DND5e.Actor.RollAbilitySave<D>,
	ability: DND5e.Abilities,
	options: Partial<ExtendedOptions<D20RollOptions<D>>>
) {
	if (options?.chatMessage === false || options?.vanilla) {
		return await original.call(this, ability, options);
	}

	const roll = await original.call(this, ability, {
		...options,
		fastForward: true,
		chatMessage: false,
		...Utils.getAdvantage(options),
	});

	const label = `${i18n(CONFIG.DND5E.abilities[ability])} ${i18n("br5e.chat.save")}`;
	return await CustomRoll._fullRollActor(this, label, roll);
}

export type ActorRollAbilitySave = LibWrapUnwrap<typeof actorRollAbilitySave>;
export type ActorRollAbilitySaveOutput = AsyncReturnType<typeof CustomRoll._fullRollActor>;

export function patchCoreFunctions() {
	if (!libWrapper.is_fallback && !libWrapper.version_at_least?.(1, 4, 0)) {
		Hooks.once("ready", () => {
			const version = "v1.4.0.0";
			ui.notifications.error(i18n("br5e.error.libWrapperMinVersion", { version }));
		});

		return;
	}

	const actorProto = "CONFIG.Actor.documentClass.prototype";

	override("CONFIG.Item.documentClass.prototype.roll", itemRoll);
	override("CONFIG.Item.documentClass.prototype.rollAttack", itemRollAttack);
	override("CONFIG.Item.documentClass.prototype.rollToolCheck", itemRollToolCheck);

	libWrapper.register("betterrolls5e", `${actorProto}.rollSkill`, actorRollSkill, "MIXED");
	libWrapper.register("betterrolls5e", `${actorProto}.rollAbilityTest`, actorRollAbilityTest, "MIXED");
	libWrapper.register("betterrolls5e", `${actorProto}.rollAbilitySave`, actorRollAbilitySave, "MIXED");
}

/**
 * A version of libwrapper OVERRIDE that tries to get the original function.
 * We want Better Rolls and Core 5e to be swappable between each other,
 * and for other modules to wrap over it.
 * @param fn - A function that takes the original as the first parameter
 */
function override<TargetFunction extends AnyFunction>(target: string, fn: LibWrappedFunction<TargetFunction>) {
	libWrapper.register<TargetFunction>("betterrolls5e", target, fn, "OVERRIDE", { chain: true });
}

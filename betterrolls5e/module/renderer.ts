import redHeaderTemplate from "../templates/red-header.html";
import redDescriptionTemplate from "../templates/red-description.html";
import redMultirollTemplate from "../templates/red-multiroll.html";

// `red-damage-crit.html` is a dependency of red-damageroll. It's not being dropped because of an import in index.html
// TODO: develop a plugin to bundle handlebars imports.
import redDamageRollTemplate from "../templates/red-damageroll.html";

import redSaveButtonTemplate from "../templates/red-save-button.html";
import redDamageButtonTemplate from "../templates/red-damage-button.html";
import redAEButtonTemplate from "../templates/red-ae-button.html";
import redFullRollTemplate from "../templates/red-fullroll.html";

import { getSettings } from "./settings.js";
import { betterRollsError, betterRollsVerbose, i18n, type SaveData, Utils } from "./utils/index.js";
import { isCombinedDamageType, combinedDamageTypeErrorMessage, throwWithContext } from "./utils/guards.js";

import type { ProcessRollResult } from "./utils/utils.js";
import type { CustomItemRoll } from "./custom-roll.js";
import type { Settings } from "./settings.js";
import type { InvariantUnionDeep, Optional } from "./lib/utils.js";

export type RenderModelEntries = {
	header: HeaderDataProps;
	description: DescriptionDataProps;
	multiroll: MultiRollDataProps;
	"button-save": ButtonSaveProps;
	damage: DamageEntry;
	raw: RawProps;
	crit: CritDataProps;
};

export type InitializedRenderModelEntries = MergeInto<
	{
		header: object;
		description: object;
		multiroll: object;
		"button-save": object;
		"damage-group": DamageGroup; // only exists at initialization
		damage: {
			group: string;
		};
		raw: object;
		crit: {
			revealed?: true;
			group: string;
		};
	},
	{ id: string }
>;

type RenderEntries = keyof RenderModelEntries;
type InitializedEntries = keyof RenderModelEntries | "damage-group";

/**
 * Union type of all possible render model types, separatable by the type property.
 */
export type RenderModelEntry<K extends RenderEntries = RenderEntries> = InvariantUnionDeep<
	K extends unknown ? RenderModelEntries[K] : never
>;

export type MaybeInitializedEntry<K extends RenderEntries = RenderEntries> = InvariantUnionDeep<
	K extends unknown ? RenderModelEntries[K] & Partial<InitializedRenderModelEntries[K]> : never
>;

export type InitializedEntry<K extends InitializedEntries = InitializedEntries> = InvariantUnionDeep<
	K extends unknown
		? (K extends keyof RenderModelEntries ? RenderModelEntries[K] : object) & InitializedRenderModelEntries[K]
		: never
>;

/**
 * Model data for rendering the header template.
 * Not part of the entry list
 */
export type HeaderDataProps = {
	type: "header";

	/** image path to show in the box */
	img: Optional<string>;

	/** header title text */
	title: string;
	slotLevel?: DND5e.SpellLevels;
};

/** Model data for rendering description or flavor text */
export type DescriptionDataProps = {
	type: "description";
	isFlavor?: Partial<boolean>;
	content: string;
};

export type MultiRollDataEntry = ProcessRollResult & {
	d20Result?: Optional<number>;
};

export type MultiRollDataProps = {
	type: "multiroll";
	title: Optional<string>;
	rollState: Optional<RollState>;

	/** Damage group used to identify what damage entries would be affected by a crit */
	group?: Optional<string>;
	critThreshold: Optional<number>;

	/** Whether elven accuracy applies to this attack */
	elvenAccuracy: Optional<boolean>;

	/** If its an attack or custom */
	rollType: Optional<string>;

	/** The full roll formula to show. This is the entries + bonus */
	formula: Formula;

	/** Was the crit status forced */
	forceCrit: Optional<boolean>;
	isCrit: boolean;

	/** Main d20 roll. Bonuses are added to this */
	entries: MultiRollDataEntry[];

	/** Any bonuses to add to the roll (that only get rolled once) */
	bonus: Roll;

	triggersCrit?: Optional<boolean>;
};

export type DamageIndex = number | "versatile" | "other";

/**
 * Model data for rendering damage information.
 */
export type DamageDataProps = DamageBaseProps & {
	type: "damage";
	damageIndex: Optional<DamageIndex>;
};

export type OtherDataProps = {
	type: "other";
};

export type DamageBaseProps = {
	title: Optional<string>;

	/** If its something like bludgeoning or piercing */
	damageType: Optional<string>;
	context: Optional<string>;

	/** Used for things like savage */
	extraCritDice: number | false;
	baseRoll: Evaluated<Roll>;
	critRoll: Optional<Roll>;
	_critBackup?: Optional<Roll>;
};

export type DamageFullProps = {
	damageIndex: Optional<DamageIndex>;
};

/**
 * Model data for rendering bonus crit information.
 */
export type CritDataProps = {
	type: "crit";

	title: Optional<string>;

	/** If its something like bludgeoning or piercing */
	damageType: Optional<string>;
	context: Optional<string>;
	critRoll?: Optional<Roll>;

	/** Has the crit entry been revealed */
	revealed?: Optional<boolean>;

	/** Used to cache if the entry has already been rolled. Do not set yourself. */
	_diceRolled?: true;
};

/**
 * Union type of all fields that relate to damage
 */
export type DamageEntry = InvariantUnionDeep<DamageDataProps | CritDataProps>;

/**
 * Model data for save buttons
 */
export type ButtonSaveProps = {
	type: "button-save";
	hideDC: boolean;
} & Partial<SaveData>;

export type DamageGroup = {
	id: string;
	type: "damage-group";

	/** id of the associated attack roll */
	attackId: Optional<string>;
	prompt: boolean;
	isCrit: boolean;

	/** whether crit can be unset or not */
	forceCrit?: boolean;
	entries: InitializedEntry<"damage" | "crit">[];
};

export type RawProps = {
	type: "raw";
} & ({ html: string } | { content: { html: string } } | { content: string });

/**
 * A collection of functions used to build html from metadata.
 */
export class Renderer {
	/**
	 * Renders a model by checking the type. Calls the other render functions depending on what it is
	 */
	static async renderModel(
		model?: string | InitializedEntry,
		settings: Optional<Settings> = null
	): Promise<Optional<string>> {
		if (typeof model === "string" || model == null) {
			return model;
		}

		betterRollsVerbose("Rendering model:", model, "\nSettings:", settings);

		switch (model.type) {
			case "header":
				return await Renderer.renderHeader(model);
			case "description":
				return await Renderer.renderDescription(model);
			case "multiroll":
				return await Renderer.renderMultiRoll(model, settings);
			case "damage":
			case "crit":
				return await Renderer.renderDamage(model, settings);
			case "damage-group":
				return await Renderer.renderDamageGroup(model, settings);
			case "button-save":
				return await Renderer.renderSaveButton(model);
			case "raw":
				if (model.html != null) {
					return model.html;
				}

				if (typeof model.content === "string") {
					return model.content;
				}

				return model.content?.html;
			default: {
				const modelType = (model as { type?: unknown })?.type;
				betterRollsError(`Given unknown render model type ${modelType}`);
			}
		}

		return null;
	}

	/**
	 * Renders the header template
	 */
	static renderHeader(properties: InitializedEntry<"header">) {
		const { img, title, slotLevel } = properties;

		return renderTemplate(redHeaderTemplate, {
			id: properties.id,
			item: { img: img ?? "icons/svg/mystery-man.svg", name: title },
			slotLevel,
		});
	}

	/**
	 * Renders the description template
	 */
	static renderDescription(properties: DescriptionDataProps) {
		return renderTemplate(redDescriptionTemplate, properties);
	}

	/**
	 * Renders a multiroll, which represent most D20 rolls.
	 */
	static async renderMultiRoll(properties: MultiRollDataProps, settings: Optional<Settings>): Promise<string> {
		const { rollTitlePlacement, d20RollIconsEnabled } = getSettings(settings);
		const title = rollTitlePlacement !== "0" ? properties.title : null;

		// Show D20 die icons if enabled
		let { entries } = properties;
		if (d20RollIconsEnabled) {
			entries = entries.map((e) => ({ ...e, d20Result: Utils.findD20Result(e.roll) }));
		}

		// Create roll templates
		const tooltips = await Promise.all(properties.entries.map((e) => e.roll.getTooltip()));
		const bonusTooltip = await properties.bonus?.getTooltip();

		// Render final result
		return await renderTemplate(redMultirollTemplate, {
			...properties,
			title,
			entries,
			tooltips,
			bonusTooltip,
		});
	}

	/**
	 * Renders damage html data
	 */
	static async renderDamage(
		properties: InitializedEntry<"damage" | "crit">,
		settings: Optional<Settings>
	): Promise<Optional<string>> {
		const { damageType, critRoll, context } = properties;
		if (damageType != null && !isCombinedDamageType(damageType)) {
			throwWithContext({
				message: [combinedDamageTypeErrorMessage(damageType)],
				errorConstructor: TypeError,
			});
		}

		const isVersatile = properties?.damageIndex === "versatile";

		const baseRoll = properties?.baseRoll;
		if (baseRoll?.terms.length === 0 && critRoll?.terms.length === 0) {
			return null;
		}

		const baseTooltips = await Promise.all([baseRoll?.getTooltip(), critRoll?.getTooltip()]);
		const tooltips = baseTooltips.filter((t): t is string => t !== "" && t != null);

		const allSettings = getSettings(settings);
		const { critString } = allSettings;

		const {
			damageTitlePlacement,
			damageRollPlacement,
			damageContextPlacement,
			contextReplacesTitle,
			contextReplacesDamage,
		} = allSettings;

		const labels: Record<"1" | "2" | "3", string[]> = {
			1: [],
			2: [],
			3: [],
		};

		let dtype: Optional<string>;

		let titleString = properties.title ?? "";

		if (damageType != null) {
			dtype = CONFIG.betterRolls5e.combinedDamageTypes[damageType];

			if (titleString == null && damageType in CONFIG.DND5E.healingTypes) {
				// Show "Healing" prefix only if it's not inherently a heal action
				titleString = "";
			} else if (!titleString && damageType in CONFIG.DND5E.damageTypes) {
				// Show "Damage" prefix if it's a damage roll
				titleString += i18n("br5e.chat.damage");
			}
		} else if (properties.type === "crit") {
			// 5e crits don't have a damage type, so we mark as critical and have the user figure it out
			titleString += i18n("br5e.critString.choices.5");
		}

		// Title
		let pushedTitle = false;
		if (
			damageTitlePlacement !== "0" &&
			titleString &&
			!(contextReplacesTitle && context && damageTitlePlacement === damageContextPlacement)
		) {
			labels[damageTitlePlacement].push(titleString);
			pushedTitle = true;
		}

		// Context (damage type and roll flavors)
		const bonusContexts = Utils.getRollFlavors(baseRoll, critRoll).filter((c) => c !== context);
		if (damageContextPlacement !== "0" && (context || bonusContexts.length > 0)) {
			const contextStr = [context, bonusContexts.join("/")].filter((c) => c).join(" + ");
			if (damageContextPlacement === damageTitlePlacement && pushedTitle) {
				const title = labels[damageContextPlacement][0];

				let fullTitle = title ? `${title} ` : "";
				fullTitle += `(${contextStr})`;

				labels[damageContextPlacement][0] = fullTitle;
			} else {
				labels[damageContextPlacement].push(contextStr);
			}
		}

		// Damage type
		const damageStringParts: string[] = [];
		if (dtype != null) {
			damageStringParts.push(dtype);
		}

		if (isVersatile) {
			damageStringParts.push(`(${CONFIG.DND5E.weaponProperties.ver})`);
		}

		const damageString = damageStringParts.join(" ");
		if (
			damageRollPlacement !== "0" &&
			damageString.length > 0 &&
			!(contextReplacesDamage && context && damageRollPlacement === damageContextPlacement)
		) {
			labels[damageRollPlacement].push(damageString);
		}

		const { 1: damagetop, 2: damagemid, 3: damagebottom } = labels;

		return await renderTemplate(redDamageRollTemplate, {
			id: properties.id,
			group: properties.group,
			damageRollType: properties.type,
			tooltips,
			base: Utils.processRoll(baseRoll),
			crit: Utils.processRoll(critRoll),
			crittext: critString,

			damagetop: damagetop.join(" - "),
			damagemid: damagemid.join(" - "),
			damagebottom: damagebottom.join(" - "),

			formula: baseRoll?.formula ?? critRoll?.formula,
			damageType,
			maxRoll: baseRoll
				? (
						await new Roll(baseRoll.formula).evaluate({ maximize: true, async: true })
				  ).total
				: null,
			maxCrit: critRoll
				? (
						await new Roll(critRoll.formula).evaluate({ maximize: true, async: true })
				  ).total
				: null,
		});
	}

	/**
	 * Renders an html save button
	 */
	static async renderSaveButton(properties: ButtonSaveProps) {
		const abilityLabel =
			properties.ability !== "" && properties.ability != null ? CONFIG.DND5E.abilities[properties.ability] : null;

		return await renderTemplate(redSaveButtonTemplate, {
			abilityLabel,
			...properties,
		});
	}

	/**
	 * Renders an html damage button
	 */
	static async renderDamageGroup(properties: DamageGroup, settings: Optional<Settings>) {
		const results: Array<Promise<string | null | undefined>> = [];

		for (const entry of properties.entries) {
			if (["damage", "crit"].includes(entry.type) && properties.prompt) {
				continue;
			}

			// Create the template, only do so if not of type crit unless crit is revealed
			if (entry.type !== "crit" || entry.revealed) {
				const damageEntry: InitializedEntry<"damage"> = { ...entry, group: properties.id.toString() };
				results.push(this.renderModel(damageEntry, settings));
			}
		}

		if (properties.prompt) {
			const { id } = properties;
			const button = renderTemplate(redDamageButtonTemplate, { id });
			results.push(button);
		}

		const renderedResults = await Promise.all(results);
		return renderedResults.join("");
	}

	/**
	 * Renders a full card
	 */
	static async renderCard(data: CustomItemRoll) {
		const templates: Array<string | null | undefined> = [];

		let previous: InitializedEntry | null = null;
		for (const entry of data.entries) {
			if (entry == null) {
				continue;
			}

			// If its a new attack/damage group, add a divider
			const previousIsDamage =
				previous?.type != null && ["damage", "crit", "damage-group"].includes(previous?.type);

			const currentIsGroup = ["multiroll", "button-save", "damage-group"].includes(entry.type);

			if (previousIsDamage && currentIsGroup) {
				templates.push("<hr/>");
			}

			templates.push(await Renderer.renderModel(entry));
			previous = entry;
		}

		// Render apply active effects button if enabled
		const actor = await data.getActor();
		const item = await data.getItem();

		if (window.DAE && data.settings.applyActiveEffects) {
			const hasEffects = item?.data.effects.find((ae) => !ae.data.transfer);
			if (hasEffects) {
				const button = await renderTemplate(redAEButtonTemplate, {});
				templates.push(button);
			}

			const consume = item?.data.data.consume;
			if (consume != null) {
				const hasAmmo = consume.type === "ammo" && consume.target != null;
				if (hasAmmo) {
					const ammo = actor?.items.get(consume.target) as Item5e | undefined;
					const ammoHasEffects = ammo?.data.effects.find((ae) => !ae.data.transfer);

					if (ammoHasEffects) {
						const button = await renderTemplate(redAEButtonTemplate, {
							ammo: true,
							context: ammo?.data.name,
						});
						templates.push(button);
					}
				}
			}
		}

		return await renderTemplate(redFullRollTemplate, {
			item,
			actor,
			tokenId: data.tokenId,
			isCritical: data.isCrit,
			templates,
			properties: data.properties,
		});
	}
}

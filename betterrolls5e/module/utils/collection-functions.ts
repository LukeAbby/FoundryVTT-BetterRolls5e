import type { Optional } from "../lib/utils";

/**
 * Returns a new object containing a subset of source
 * using the keys in props. Equivalent to lodash's pick method.
 * @returns subset of source
 */
export function pick<
	PickKeys extends string,
	Source extends Record<SourceKeys | PickKeys, Values>,
	SourceKeys extends string,
	Values
>(source: Source, props: readonly PickKeys[]): Pick<Source, PickKeys & keyof Source> {
	const result: Partial<Pick<Source, PickKeys & keyof Source>> = {};
	for (const prop of props) {
		if (prop in source) {
			result[prop] = source[prop];
		}
	}

	return result as Pick<Source, PickKeys & keyof Source>; // The object is now not partial.
}

export function pickBy<
	PickKeys extends string,
	Source extends Record<SourceKeys | PickKeys, Values>,
	SourceKeys extends string,
	Values
>(
	source: Source,
	predicate: (v: Source[keyof Source], k: string) => k is PickKeys
): Pick<Source, PickKeys & keyof Source> {
	const props: PickKeys[] = [];
	for (const [key, value] of Object.entries(source) as [SourceKeys | PickKeys, Source[keyof Source]][]) {
		if (predicate(value, key)) {
			props.push(key);
		}
	}

	return pick<PickKeys, Source, SourceKeys, Values>(source, props);
}

type ArrValue<Arr> = Arr extends readonly (infer V)[] ? V : never;

/**
 * This method is like findIndex except that it iterates
 * from right to left.
 */
export function findLastIndex<C extends readonly unknown[]>(
	array: Optional<C>,
	predicate: (v: ArrValue<C>, i: number, a: C) => boolean
): number {
	if (array?.length == null) {
		return -1;
	}

	for (let index = array.length - 1; index >= 0; index--) {
		if (predicate(array[index] as ArrValue<C>, index, array)) {
			return index;
		}
	}

	return -1;
}

/**
 * This method is like find except that it iterates
 * from right to left.
 */
export function findLast<C extends readonly unknown[], T extends ArrValue<C>>(
	collection: C,
	predicate: (v: ArrValue<C>, i: number, a: C) => v is ArrValue<C>
) {
	const index = findLastIndex(collection, predicate);
	return index > -1 ? (collection[index] as T) : undefined;
}

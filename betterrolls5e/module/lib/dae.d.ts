import type { FullPartial, Optional } from "./utils";

type DoEffectsOptions = FullPartial<{
	whisper: boolean;
	spellLevel: number;
	damageTotal: number;
	itemCardId: string;
	critical: boolean;
	fumble: boolean;
	effectsToApply: [];
	removeMatchLabel: boolean;
}>;

export namespace DAE {
	export type Window = {
		doEffects(
			item: Optional<Item5e>,
			activate: boolean,
			targets: Optional<Array<Token5e | StoredDocument<Actor5e>> | UserTargets>,
			options: DoEffectsOptions
		): void;
	};
}

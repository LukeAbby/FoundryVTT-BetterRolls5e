import { CustomRoll, CustomItemRoll } from "./custom-roll.js";
import { i18n, Utils, ItemUtils, betterRollsError, waitForReady } from "./utils/index.js";
import { getSettings } from "./settings.js";

import type { Optional } from "./lib/utils";
import type { ModelFields } from "./fields";
import type { GetRollStateOptions } from "./utils/utils";
import type { CustomItemRollParams } from "./custom-roll";

export const attackTypes = ["mwak", "rwak", "msak", "rsak"];

// Returns whether an item makes an attack roll
export function isAttack(item: Item5e) {
	return item.data.data.actionType != null && attackTypes.includes(item.data.data.actionType);
}

// Returns whether an item requires a saving throw
export function isSave(item: Item5e): item is ItemOfType<"spell"> {
	const itemData = item.data.data;
	const ability = itemData.save?.ability;

	return itemData.actionType === "save" || (ability != null && ability !== "");
}

export function isCheck(item: Item5e) {
	return item.data.type === "tool" || typeof item.data.data.proficient === "number";
}

/**
 * Function for adding Better Rolls content to html data made after a sheet is rendered.
 * @param actor - The actor object
 * @param html - The target html to add content to
 * @param triggeringElement - Container for the element that must be clicked for the extra buttons to be shown.
 * @param buttonContainer - Container for the element the extra buttons will display in.
 * @param itemButton - Selector for the item button.
 */
export async function addItemContent(
	actor: Actor5e,
	html: JQuery,
	triggeringElement = ".item .item-name h4",
	buttonContainer = ".item-properties"
) {
	await waitForReady();

	if (game.settings.get("betterrolls5e", "rollButtonsEnabled") && triggeringElement && buttonContainer) {
		await addItemSheetButtons(actor, html, null, triggeringElement, buttonContainer);
	}
}

function getQuickDescriptionDefault() {
	return getSettings().quickDefaultDescriptionEnabled;
}

export const betterRolls5eConfig = {
	validItemTypes: ["weapon", "spell", "equipment", "feat", "tool", "consumable"],
	allFlags: {
		weaponFlags: {
			quickDesc: {
				type: "Boolean",
				get value() {
					return getQuickDescriptionDefault();
				},
				get altValue() {
					return getQuickDescriptionDefault();
				},
			},
			quickAttack: { type: "Boolean", value: true, altValue: true },
			quickSave: { type: "Boolean", value: true, altValue: true },
			quickDamage: { type: "Array", value: [] as boolean[], altValue: [] as boolean[], context: [] as string[] },
			quickVersatile: { type: "Boolean", value: false, altValue: false },
			quickProperties: { type: "Boolean", value: true, altValue: true },
			quickCharges: {
				type: "Boolean",
				value: { quantity: false, use: false, resource: true },
				altValue: { quantity: false, use: true, resource: true },
			},
			quickTemplate: { type: "Boolean", value: true, altValue: true },
			quickOther: { type: "Boolean", value: true, altValue: true, context: "" },
			quickFlavor: { type: "Boolean", value: true, altValue: true },
			quickPrompt: { type: "Boolean", value: false, altValue: false },
		},
		equipmentFlags: {
			quickDesc: { type: "Boolean", value: true, altValue: true },
			quickAttack: { type: "Boolean", value: true, altValue: true },
			quickSave: { type: "Boolean", value: true, altValue: true },
			quickDamage: { type: "Array", value: [] as boolean[], altValue: [] as boolean[], context: [] as string[] },
			quickProperties: { type: "Boolean", value: true, altValue: true },
			quickCharges: {
				type: "Boolean",
				value: { quantity: false, use: false, resource: true },
				altValue: { quantity: false, use: false, resource: true },
			},
			quickOther: { type: "Boolean", value: true, altValue: true, context: "" },
			quickFlavor: { type: "Boolean", value: true, altValue: true },
			quickPrompt: { type: "Boolean", value: false, altValue: false },
		},
		consumableFlags: {
			quickDesc: { type: "Boolean", value: true, altValue: true },
			quickAttack: { type: "Boolean", value: true, altValue: true },
			quickSave: { type: "Boolean", value: true, altValue: true },
			quickDamage: { type: "Array", value: [] as boolean[], altValue: [] as boolean[], context: [] as string[] },
			quickProperties: { type: "Boolean", value: true, altValue: true },
			// Consumables consume uses by default in vanilla 5e
			quickCharges: {
				type: "Boolean",
				value: { quantity: false, use: true, resource: true },
				altValue: { quantity: false, use: true, resource: true },
			},
			quickTemplate: { type: "Boolean", value: true, altValue: true },
			quickOther: { type: "Boolean", value: true, altValue: true, context: "" },
			quickFlavor: { type: "Boolean", value: true, altValue: true },
			quickPrompt: { type: "Boolean", value: false, altValue: false },
		},
		toolFlags: {
			quickDesc: {
				type: "Boolean",
				get value() {
					return getQuickDescriptionDefault();
				},
				get altValue() {
					return getQuickDescriptionDefault();
				},
			},
			quickCheck: { type: "Boolean", value: true, altValue: true },
			quickProperties: { type: "Boolean", value: true, altValue: true },
			quickFlavor: { type: "Boolean", value: true, altValue: true },
			quickPrompt: { type: "Boolean", value: false, altValue: false },
		},
		lootFlags: undefined,
		classFlags: undefined,
		spellFlags: {
			quickDesc: { type: "Boolean", value: true, altValue: true },
			quickAttack: { type: "Boolean", value: true, altValue: true },
			quickSave: { type: "Boolean", value: true, altValue: true },
			quickDamage: { type: "Array", value: [] as boolean[], altValue: [] as boolean[], context: [] as string[] },
			quickVersatile: { type: "Boolean", value: false, altValue: false },
			quickProperties: { type: "Boolean", value: true, altValue: true },
			quickCharges: {
				type: "Boolean",
				value: { use: true, resource: true },
				altValue: { use: true, resource: true },
			},
			quickTemplate: { type: "Boolean", value: true, altValue: true },
			quickOther: { type: "Boolean", value: true, altValue: true, context: "" },
			quickFlavor: { type: "Boolean", value: true, altValue: true },
			quickPrompt: { type: "Boolean", value: false, altValue: false },
		},
		featFlags: {
			quickDesc: { type: "Boolean", value: true, altValue: true },
			quickAttack: { type: "Boolean", value: true, altValue: true },
			quickSave: { type: "Boolean", value: true, altValue: true },
			quickDamage: { type: "Array", value: [] as boolean[], altValue: [] as boolean[], context: [] as string[] },
			quickProperties: { type: "Boolean", value: true, altValue: true },
			// Feats consume uses by default in vanilla 5e
			quickCharges: {
				type: "Boolean",
				value: { use: true, resource: true, charge: true },
				altValue: { use: true, resource: true, charge: true },
			},
			quickTemplate: { type: "Boolean", value: true, altValue: true },
			quickOther: { type: "Boolean", value: true, altValue: true, context: "" },
			quickFlavor: { type: "Boolean", value: true, altValue: true },
			quickPrompt: { type: "Boolean", value: false, altValue: false },
		},
		backpackFlags: undefined,
	},

	combinedDamageTypes: {} as Record<BetterRolls.CombinedDamageTypes, string>,

	chatDamageButtonsEnabled: {
		disabled: "0",
		enabled: "1",
		gmOnly: "2",
	} as const,

	hideDC: {
		never: "0",
		npcs: "1",
		always: "2",
	} as const,

	critBehavior: {
		noExtraDamage: "0",
		rollCritical: "1",
		rollBaseMaxCritical: "2",
		maxBaseMaxCritical: "3",
		maxBaseRollCritical: "4",
	} as const,

	d20Mode: {
		singleRolls: 1,
		dualRolls: 2,
		tripleThreat: 3,
		query: 4,
	} as const,
};

CONFIG.betterRolls5e = betterRolls5eConfig;

type Data = object; // Stubbed. Should be expanded if ever used.

/**
 * Adds buttons and assign their functionality to the sheet
 * @param triggeringElement - this is the html selector string that opens the description - mostly optional for different sheetclasses
 * @param buttonContainer - this is the html selector string to which the buttons will be prepended - mostly optional for different sheetclasses
 */
export async function addItemSheetButtons(
	actor: Actor5e,
	html: JQuery,
	data: Optional<Data>,
	triggeringElement = "",
	buttonContainer = ""
) {
	// Do not modify the sheet if the user does not have permission to use the sheet
	if (actor.permission < 3) {
		return;
	}

	// Setting default element selectors

	let triggeringElementSelector = triggeringElement;
	if (triggeringElement === "") {
		triggeringElementSelector = ".item:not(.magic-item) .item-name h4";
	}

	let buttonContainerSelector = buttonContainer;
	if (buttonContainer === "") {
		buttonContainerSelector = ".item-properties";
	}

	// adding an event for when the description is shown
	html.find(triggeringElementSelector).on("click", async (event) => {
		const li = $(event.currentTarget).parents(".item");

		await addButtonsToItemLi(li, actor, buttonContainerSelector);
	});

	for (const element of html.find(triggeringElementSelector)) {
		const li = $(element).parents(".item:not(.magic-item)");

		await addButtonsToItemLi(li, actor, buttonContainerSelector);
	}
}

type CreateButtonOptions = {
	/** The text to display inside the button. */
	content: string;

	/** The value of the data-action attribute. */
	action: string;

	/** The value of the data-value attribute. */
	value?: string | number | null;
};

/**
 * Helper function for creating roll buttons.
 */
function createButton({ content, action, value = null }: CreateButtonOptions) {
	return `<span class="tag">
		<button data-action=${action} ${value == null ? "" : `data-value="${value}"`}>
			${content}
		</button>
	</span>`;
}

async function addButtonsToItemLi(li: JQuery, actor: Actor5e, buttonContainer: string) {
	if (!li.hasClass("expanded")) {
		return; // this is a way to not continue if the items description is not shown, but its only a minor gain to do this while it may break this module in sheets that dont use "expanded"
	}

	const itemId = String(li.attr("data-item-id") ?? "");
	if (!itemId) {
		return;
	}

	const item = actor.items.get(itemId) as Item5e;
	ItemUtils.ensureFlags(item);

	const flags = item.data.flags.betterRolls5e;

	// Check settings
	const settings = getSettings();
	const contextEnabled = settings.damageContextPlacement !== "0";

	// Create the buttons
	const buttons = $(`<div class="item-buttons"></div>`);
	let buttonsWereAdded = false;

	// TODO: Make the logic in this switch statement simpler.
	switch (item.data.type) {
		case "weapon":
		case "feat":
		case "spell":
		case "consumable": {
			const itemData = item.data.data;

			buttonsWereAdded = true;
			buttons.append(
				createButton({ content: i18n("br5e.buttons.roll"), action: "quickRoll" }),
				createButton({ content: i18n("br5e.buttons.altRoll"), action: "altRoll" })
			);

			if (isAttack(item)) {
				buttons.append(createButton({ content: i18n("br5e.buttons.attack"), action: "attackRoll" }));
			}

			if (isSave(item)) {
				const saveData = ItemUtils.getSave(item);

				const { dc, ability } = saveData;
				const translatedAbility = ability !== "" ? CONFIG.DND5E.abilities[ability] : undefined;

				buttons.append(
					createButton({
						content: `${i18n("br5e.buttons.saveDC")} ${dc} ${translatedAbility}`,
						action: "save",
					})
				);
			}

			if (itemData.damage?.parts.length) {
				buttons.append(
					createButton({ content: i18n("br5e.buttons.damage"), action: "damageRoll", value: "all" })
				);

				if (itemData.damage?.versatile) {
					buttons.append(
						createButton({ content: i18n("br5e.buttons.verDamage"), action: "verDamageRoll", value: "all" })
					);
				}

				// Make a damage button for each damage type
				if (itemData.damage.parts.length > 1) {
					buttons.append(`<br>`);

					itemData.damage.parts.forEach(([_, damageType], i) => {
						const damageString =
							(contextEnabled ? flags?.quickDamage?.context[i] : null) ??
							CONFIG.betterRolls5e.combinedDamageTypes[damageType];

						let content = `${i}: ${damageString}`;

						if (i === 0 && itemData.damage.versatile) {
							content += ` (${CONFIG.DND5E.weaponProperties.ver})`;
						}

						buttons.append(createButton({ content, action: "damageRoll", value: i }));
					});
				}
			}

			if (itemData.formula?.length) {
				const otherString =
					(contextEnabled ? flags?.quickOther?.context : null) ?? i18n("br5e.settings.otherFormula");

				buttons.append(createButton({ content: otherString, action: "otherFormulaRoll" }));
			}

			break;
		}
		case "tool": {
			const itemData = item.data.data;
			buttonsWereAdded = true;

			buttons.append(
				createButton({
					content: `${i18n("br5e.buttons.itemUse")} ${item.name}`,
					action: "toolCheck",
					value: itemData.ability,
				})
			);

			break;
		}
		default: {
			break;
		}
	}

	if (buttonsWereAdded) {
		buttons.append(`<br>`);
	}

	// Add info button
	buttons.append(createButton({ content: i18n("br5e.buttons.info"), action: "infoRoll" }));

	// Add default roll button
	buttons.append(createButton({ content: i18n("br5e.buttons.defaultSheetRoll"), action: "vanillaRoll" }));

	if (buttonsWereAdded) {
		buttons.append(`<br><header style="margin-top: 6px"></header>`);
	}

	// adding the buttons to the sheet
	const targetHTML = li; // $(event.target.parentNode.parentNode)
	targetHTML.find(buttonContainer).prepend(buttons);

	// adding click event for all buttons
	buttons.find("button").on("click", async (ev) => {
		ev.preventDefault();
		ev.stopPropagation();

		// The arguments compounded into an object and an array of fields, to be served to the roll() function as the params and fields arguments
		const params: Partial<CustomItemRollParams> = {
			forceCrit: ev.altKey,
			event: ev,
		};
		const fields: ModelFields[] = [];
		if (params.forceCrit) {
			fields.push(["flavor", { text: `${getSettings().critString}` }]);
		}

		// Sets the damage roll in the argument to the value of the button
		function setDamage(versatile = false) {
			if (ev.target.dataset.value === "all") {
				fields.push(["damage", { index: "all", versatile }]);
			} else {
				fields.push(["damage", { index: Number(ev.target.dataset.value) }]);
			}
		}

		const { action } = ev.target.dataset;
		switch (action) {
			case "quickRoll":
				params.preset = 0;
				break;

			case "altRoll":
				params.preset = 1;
				break;

			case "attackRoll":
				fields.push(["attack"]);
				break;

			case "save":
				fields.push(["savedc"]);
				break;

			case "damageRoll":
				setDamage();
				break;

			case "verDamageRoll":
				setDamage(true);
				params.versatile = true;
				break;

			case "toolCheck":
				fields.push(["toolcheck"]);
				break;

			case "otherFormulaRoll":
				fields.push(["other"]);
				break;

			case "infoRoll":
				fields.push(["header"], ["desc"]);
				params.consume = false;
				params.properties = true;
				params.infoOnly = true;
				break;

			case "vanillaRoll":
				await item.roll({ vanilla: true });
				break;

			default:
				betterRollsError(`Invalid action type "${action}" from dataset in event:`, ev);
				return;
		}

		if (action !== "vanillaRoll") {
			await new CustomItemRoll(item, params, fields).toMessage();
		}
	});
}

type ActorSheetItemDragData = {
	actorId: string;
	sceneId: string;
	tokenId: string;
	type: "Item";
	data: Item5e["data"];
};

/** Frontend for macros */
export function BetterRolls() {
	async function assignMacro(item: ActorSheetItemDragData, slot: string | number, mode: "vanillaRoll" | "id") {
		await waitForReady();

		function command() {
			const vanilla = mode === "vanillaRoll" ? "true" : "false";
			return `
// HotbarUses5e: ActorID="${item.actorId}" ItemID="${item.data._id}"
const actorId = "${item.actorId}";
const itemId = "${item.data._id}";
const actorToRoll = canvas.tokens.placeables.find(t => t.actor?.id === actorId)?.actor ?? game.actors.get(actorId);
const itemToRoll = actorToRoll?.items.get(itemId);

if (game.modules.get('itemacro')?.active && itemToRoll?.hasMacro() && game.settings.get('itemacro', 'defaultmacro')) {
	return itemToRoll.executeMacro();
}

if (!itemToRoll) {
	return ui.notifications.warn(game.i18n.format("DND5E.ActionWarningNoItem", { item: itemId, name: actorToRoll?.name ?? "[Not Found]" }));
}

return await itemToRoll.roll({ vanilla: ${vanilla} });
`;
		}

		let macro = game.macros?.find((m) => m.name === item.data.name && m.data.command === command.toString());
		if (macro == null) {
			macro = await Macro.create(
				{
					name: item.data.name,
					type: "script",
					img: item.data.img,
					command: command(),
					flags: { "dnd5e.itemMacro": true },
				},
				{
					renderSheet: false,
				}
			);
		}
		await game.user?.assignHotbarMacro(macro ?? null, slot);
	}

	// Performs a vanilla roll message, searching the actor and item by ID.
	async function vanillaRoll(actorId: string, itemId: string) {
		const actor = getActorById(actorId);
		if (actor == null) {
			return ui.notifications.warn(`${i18n("br5e.error.noActorWithId")}`);
		}

		const item = actor.items.get(itemId);
		if (item == null) {
			return ui.notifications.warn(`${i18n("br5e.error.noItemWithId")}`);
		}

		if (actor.permission !== 3) {
			return ui.notifications.warn(`${i18n("br5e.error.noActorPermission")}`);
		}

		return await item.roll({ vanilla: true, event });
	}

	// Performs a Quick Roll, searching for an item in the controlled actor by name.
	async function quickRoll(itemName: string) {
		const speaker = ChatMessage.getSpeaker();
		const actor = speaker.actor != null ? getActorById(speaker.actor) : null;
		const item = actor ? actor.items.find((i) => i.name === itemName) : null;
		if (actor == null) {
			return ui.notifications.warn(`${i18n("br5e.error.noSelectedActor")}`);
		}

		if (item == null) {
			return ui.notifications.warn(`${actor.name} ${i18n("br5e.error.noKnownItemOnActor")} ${itemName}`);
		}

		return await item.roll({ vanilla: false, event });
	}

	// Performs a Quick Roll, searching the actor and item by ID.
	async function quickRollById(actorId: string, itemId: string) {
		const actor = getActorById(actorId);
		if (!actor) {
			return ui.notifications.warn(`${i18n("br5e.error.noActorWithId")}`);
		}

		const item = actor.items.get(itemId);
		if (item == null) {
			return ui.notifications.warn(`${i18n("br5e.error.noItemWithId")}`);
		}

		if (actor.permission !== 3) {
			return ui.notifications.warn(`${i18n("br5e.error.noActorPermission")}`);
		}

		return await item.roll({ vanilla: false, event });
	}

	// Performs a Quick Roll, searching the actor and item by name.
	async function quickRollByName(actorName: string, itemName: string) {
		const actor = getActorByName(actorName);
		if (actor == null) {
			return ui.notifications.warn(`${i18n("br5e.error.noKnownActorWithName")}`);
		}

		const item = actor.items.find((i) => i.name === itemName);
		if (item == null) {
			return ui.notifications.warn(`${actor.name} ${i18n("br5e.error.noKnownItemOnActor")} ${itemName}`);
		}

		if (actor.permission !== 3) {
			return ui.notifications.warn(`${i18n("br5e.error.noActorPermission")}`);
		}

		return await item.roll({ vanilla: false, event });
	}

	// Prefer synthetic actors over game.actors to avoid consumables and spells being missdepleted.
	function getActorById(actorId: string) {
		let actor = canvas?.tokens?.placeables.find((t) => t.actor?.id === actorId)?.actor;
		if (actor == null) {
			actor = game.actors?.get(actorId);
		}

		return actor;
	}

	// Prefer token actors over game.actors to avoid consumables and spells being missdepleted.
	function getActorByName(actorName: string) {
		let actor = canvas?.tokens?.placeables?.find((p) => p.data.name === actorName)?.actor;
		if (actor == null) {
			actor = game.actors?.find((e) => e.name === actorName);
		}

		return actor;
	}

	// TODO this is making sure we're the first hook called, done asynchronously. Maybe file an issue for a new pre hook or priority system.
	// @ts-expect-error ts(2445) expect an error because we're accessing the protected property `_hooks`.
	const hooks = Hooks._hooks as Record<string, AnyFunction[] | null>;

	const hotbarDrop = async (_bar: Hotbar, data: ActorSheetItemDragData, slot: number) => {
		if (data.type !== "Item") {
			return true;
		}

		if ((event as BasicEvent)?.altKey) {
			// not using isAlt(event) because it's not related to alternative roll
			await assignMacro(data, slot, "vanillaRoll");
		} else {
			await assignMacro(data, slot, "id");
		}

		return false;
	};

	const callbacks = hooks.hotbarDrop ?? [];
	hooks.hotbarDrop = [
		async (...args: Parameters<typeof hotbarDrop>) => {
			await hotbarDrop(...args);

			// Call every other callback after.
			for (const fn of callbacks) {
				// @ts-expect-error ts(2445) expect an error because we're accessing the protected property `_call`.
				const call = Hooks._call;
				call("hotbarDrop", fn, args);
			}
		},
	];

	// Make it so that callbacks is the true location of where hooks are.
	hooks.hotbarDrop.push = callbacks.push.bind(callbacks);

	return {
		version: Utils.getVersion(),
		assignMacro,
		vanillaRoll,
		quickRoll,
		quickRollById,
		quickRollByName,
		addItemContent,
		rollCheck: CustomRoll.rollCheck,
		rollSave: CustomRoll.rollSave,
		rollSkill: CustomRoll.rollSkill,
		rollItem: CustomRoll.newItemRoll,
		getRollState: (params: GetRollStateOptions) => Utils.getRollState({ event, ...(params ?? {}) }),
	};
}

import type { DICE_ROLL_MODES } from "@league-of-foundry-developers/foundry-vtt-types/src/foundry/common/constants.mjs";
import type { D20RollOptions } from "../lib/dnd5e/dice5e";

/**
 * These are the core D&D 5e RollOptions.
 */
export type RollOptions = {
	/** Display a configuration dialog for the item roll, if applicable? */
	configureDialog: boolean;

	/** The roll display mode with which to display (or not) the card */
	rollMode: DICE_ROLL_MODES;

	/** Whether to automatically create a chat message (if true) or simply return the prepared chat message data (if false). */
	createMessage: boolean;
};

/**
 * These are the options we add.
 */
export type ExtendedOptions<T> = T & {
	vanilla: boolean;
};

type _Roll<D extends object = object> = Roll<D>;
type _Abilities = DND5e.Abilities;

export namespace SourceFunctions {
	export namespace DND5e {
		export namespace Item {
			/**
			 * Roll the item to Chat, creating a chat card which contains follow up attack or damage roll options
			 */
			export type Roll = <O extends Partial<RollOptions>>(
				options: O
			) => Promise<(O["createMessage"] extends true ? ChatMessage : object) | void>;

			type Item5eRollData = ReturnType<Item5e["getRollData"]>;

			export type RollToolCheck<D extends Item5eRollData> = (options: D20RollOptions<D>) => Promise<_Roll<D>>;
			export type RollAttack<D extends Item5eRollData> = (options: D20RollOptions<D>) => Promise<_Roll<D>>;
		}

		export namespace Actor {
			type Actor5eRollData = ReturnType<Actor5e["getRollData"]>;

			export type RollSkill<D extends Actor5eRollData> = (
				skillId: string,
				options: D20RollOptions<D>
			) => Promise<Roll<D>>;
			export type RollAbilityTest<D extends Actor5eRollData> = (
				abl: _Abilities,
				options: D20RollOptions<D>
			) => Promise<Roll<D>>;
			export type RollAbilitySave<D extends Actor5eRollData> = (
				abl: _Abilities,
				options: D20RollOptions<D>
			) => Promise<Roll<D>>;
		}
	}
}

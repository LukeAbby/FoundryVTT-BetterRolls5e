import {
	i18n,
	ActorUtils,
	ItemUtils,
	Utils,
	getRoll,
	betterRollsError,
	type SaveData,
	waitForReady,
} from "./utils/index.js";
import { type BRSettings, getSettings } from "./settings.js";
import { isSave } from "./betterrolls5e.js";

import type {
	MultiRollDataProps,
	HeaderDataProps,
	DamageDataProps,
	CritDataProps,
	ButtonSaveProps,
	RenderModelEntry,
	OtherDataProps,
	DamageFullProps,
	DamageBaseProps,
} from "./renderer";

import type { ProcessRollResult } from "./utils";
import type { FullPartial, Optional } from "./lib/utils.js";

import type { Settings } from "./settings";
import { throwWithContext } from "./utils/guards.js";
import { betterRollsInfo } from "./utils/logging.js";

type AmmoFieldData = DamageBaseProps;

type AttackFieldData = BaseCheckData & {
	triggersCrit: boolean;
};

type HeaderFieldData = FullPartial<{ img: string }> & { title: Optional<string> };
type CheckFieldData = BaseCheckData & {
	formula: Formula;
	critThreshold: Optional<number>;
	rollType: Optional<RollType>;
};

type DamageFieldData = {
	index: InputDamageIndex;
	versatile?: Optional<boolean>;
};

type FlavorFieldData = {
	text: string;
};

type CustomFieldData = BaseCheckData & {
	title: string;
	formula: Formula;
	rolls: number;
};

type DescriptionFieldData = FullPartial<{
	text: string;
	content: string;
}>;

type BaseCheckData = FullPartial<{
	rollState: RollState;
}>;

export type ModelToData = MergeInto<
	{
		ammo: AmmoFieldData;
		ammosavedc: never;
		attack: AttackFieldData;
		check: CheckFieldData;
		crit: never;
		custom: CustomFieldData;
		damage: DamageFieldData;
		desc: DescriptionFieldData;
		description: DescriptionFieldData;
		flavor: FlavorFieldData;
		header: HeaderFieldData;
		other: OtherDataProps;
		savedc: never;
		text: DescriptionFieldData;
		tool: CheckFieldData;
		toolcheck: CheckFieldData;
	},
	FullPartial<{
		actor: Actor5e | string;
		item: Item5e | string;
	}>
>;

export type ModelFields = keyof ModelToData extends unknown ? GetModelField<keyof ModelToData> : never;

type GetModelField<K extends keyof ModelToData, V extends ModelToData[K] = ModelToData[K]> =
	| [K]
	| (V extends never ? never : [K, V]);

type ConstructMultiRollOptions = ConstructAttackRollOptions &
	FullPartial<{
		/** metadata param for attack vs damage. */
		rollType: string;

		/** whether the actor should apply elven accuracy */
		elvenAccuracy: boolean;

		/** optional flag to force a crit result */
		forceCrit: boolean;

		/** additional settings to override */
		settings: typeof BRSettings;
	}>;

type ItemType = "weapon" | "spell" | undefined;

type ConstructAttackRollOptions = FullPartial<{
	/** optional formula to use instead of the attack formula */
	formula: Formula;

	/** Actor to derive roll data from if item is not given */
	actor: Actor5e;

	/** Item to derive attack formula or roll data from */
	item: Item5e;

	/** Type of attack. Used if item is null. */
	itemType: ItemType;

	/** number of rolls to perform */
	numRolls: number;

	/** Alternative title to use */
	title: string;

	/** optional minimum roll on the dice to cause a critical roll. */
	critThreshold: number;

	/** override for the default item ability mod */
	abilityMod: DND5e.Abilities;
	rollState: RollState;
	slotLevel: DND5e.SpellLevels;
}>;

type ConstructDamageRollOptions = FullPartial<{
	/** optional formula to use, higher priority over the item formula */
	formula: Formula;
	actor: Actor5e;
	item: Item5e;
	damageIndex: number | "versatile" | "other";
	slotLevel: DND5e.SpellLevels;

	/** Optional damage context. Defaults to the configured damage context */
	context: string;
	damageType: string;

	/** title to display. If not given defaults to damage type */
	title: string;

	/** Whether to roll crit damage */
	isCrit: boolean;

	/** sets the savage property. Falls back to using the item if not given, or false otherwise. */
	extraCritDice: number | false;

	/** Override config to use for the roll */
	settings: typeof BRSettings;
}>;

type InputDamageIndex = number[] | number | "all";
type DamageIndex = number | "versatile" | "other";

type ConstructCritDamageRollOptions = FullPartial<{
	/** optional formula to use, higher priority over the item formula */
	formula: Formula;
	actor: Actor5e;
	item: Item5e;
	damageIndex: DamageIndex;
	slotLevel: DND5e.SpellLevels;

	/** Optional damage context. Defaults to the configured damage context */
	context: string;
	damageType: string;

	/** title to display. If not given defaults to damage type */
	title: string;

	/** Override config to use for the roll */
	settings: typeof BRSettings;
}>;

type ConstructItemDamageRangeOptions = (Partial<DamageFullProps> | undefined) &
	ModelsMetadata &
	FullPartial<{
		formula: Formula;

		/** one more or damage indices to roll for or "all" for every indice */
		index: InputDamageIndex;

		/** should the first damage entry be replaced with versatile */
		versatile: boolean;

		context?: string;
	}>;

export type ModelsMetadata = FullPartial<{
	item: Item5e;
	actor: Actor5e;
	rollState: RollState;
	ammo: Item5e;
	slotLevel: DND5e.SpellLevels;
	isCrit: boolean;
	settings: Settings;
}>;

type ConstructSaveButtonOptions = FullPartial<{
	item: Item5e;
	abl: DND5e.Abilities | "";
	dc: number;
	context: string;
	settings: Settings;
}>;

/**
 * Provides utility functions that can be used to create model elements
 * for new rolls.
 */
export class RollFields {
	/**
	 * Returns header data to be used for rendering
	 */
	static constructHeaderData(options: Optional<HeaderFieldData & ModelsMetadata>): HeaderDataProps {
		const { item, slotLevel } = options ?? {};
		const actor = options?.actor ?? item?.actor;
		const img = options?.img ?? item?.img ?? ActorUtils.getImage(actor);

		let title = options?.title ?? item?.name ?? actor?.name ?? "";
		if (item?.data.type === "spell" && slotLevel != null && slotLevel !== item.data.data.level) {
			title += ` (${CONFIG.DND5E.spellLevels[slotLevel]})`;
		}

		return { type: "header", img, title };
	}

	/**
	 * Constructs multiroll data to be used for rendering
	 */
	static async constructMultiRoll(options: ConstructMultiRollOptions): Promise<Optional<MultiRollDataProps>> {
		const { critThreshold, title, rollType, elvenAccuracy } = options;
		if (options.formula == null || options.formula === "") {
			throwWithContext({
				message: ["Missing formula from options! No formula provided."],
				context: ["Input options:", options],
			});
		}

		// Extract info from the formula, to know if it was rolled with advantage/disadvantage
		// The rollstate in the options has higher priority than whatever was part of the original
		const parsedData = Utils.parseD20Formula(options.formula);
		const { formula } = parsedData;
		const rollState = parsedData.rollState ?? options.rollState;

		const { d20Mode } = getSettings(options.settings);

		let numRolls;
		if (d20Mode === CONFIG.betterRolls5e.d20Mode.query) {
			numRolls = 1;
		} else {
			numRolls = options.numRolls ?? d20Mode;
		}

		if (!options.numRolls) {
			if (rollState === "first" && !options.numRolls) {
				numRolls = 1;
			} else if (rollState && numRolls === 1) {
				numRolls = 2;
			}

			// Apply elven accuracy
			if (numRolls === 2 && elvenAccuracy && rollState !== "lowest") {
				numRolls = 3;
			}
		}

		try {
			// Split the D20 and bonuses.
			const fullRoll = new Roll(formula);
			const baseRoll = new Roll(fullRoll.terms[0].formula ?? fullRoll.terms[0]);

			const restTerms = fullRoll.terms.slice(1); // We assume the first is always the main d20 roll
			const bonusRollFormula = restTerms.map((t) => t.formula ?? t).join(" ") || "0";

			const bonusRoll = await new Roll(bonusRollFormula).roll({ async: true });

			// Populate the roll entries
			const entries: ProcessRollResult[] = [];
			for (let i = 0; i < numRolls; i++) {
				const processedRoll = Utils.processRoll(
					await baseRoll.reroll({ async: true }),
					critThreshold,
					[20],
					bonusRoll
				);

				if (processedRoll != null) {
					entries.push(processedRoll);
				}
			}

			let finalEntries = entries;

			// Mark ignored rolls if advantage/disadvantage
			if (rollState != null) {
				const rollTotals = entries.map((r) => r.roll.total).filter((t): t is number => t != null);
				let chosenResult = rollTotals[0];
				if (rollState === "highest") {
					chosenResult = Math.max(...rollTotals);
				} else if (rollState === "lowest") {
					chosenResult = Math.min(...rollTotals);
				}

				// Mark the non-results as ignored
				finalEntries = entries.map((r) => ({
					...r,
					ignored: r.roll.total !== chosenResult,
				}));
			}

			return {
				type: "multiroll",
				title,
				critThreshold,
				elvenAccuracy,
				rollState,
				rollType,
				formula,
				entries: finalEntries,
				forceCrit: options.forceCrit,
				isCrit: options.forceCrit || entries.some((e) => !e.ignored && e.isCrit),
				bonus: bonusRoll,
			};
		} catch (err) {
			await logErrorMessage(err);

			throw err; // propagate the error
		}
	}

	/**
	 * Constructs multiroll (attack) data to be used for data.
	 */
	static async constructAttackRoll(options: Optional<ConstructAttackRollOptions>) {
		const { formula, item, rollState, slotLevel } = options ?? {};
		const actor = options?.actor ?? item?.actor;

		// Get critical threshold
		const critThreshold =
			options?.critThreshold ??
			ItemUtils.getCritThreshold(item) ??
			(actor != null ? ActorUtils.getCritThreshold(actor, options?.itemType) : null) ??
			20;

		const abilityMod = options?.abilityMod ?? item?.abilityMod;
		const elvenAccuracy = actor != null ? ActorUtils.testElvenAccuracy(actor, abilityMod) : null;

		let title = options?.title;

		// Get ammo bonus and add to title if title not given
		// Note that "null" is a valid title, so we can't override that
		if (typeof title === "undefined") {
			title = i18n("br5e.chat.attack");
			const consume = item?.data.data.consume;
			if (consume?.type === "ammo" && actor?.items != null) {
				const ammo = actor.items.get(consume.target);
				title += ` [${ammo?.name}]`;
			}
		}

		// Get Roll. Use Formula if given, otherwise get it from the item
		let roll: Optional<Roll<object>>;
		if (formula != null) {
			const rollData = Utils.getRollData({ item, actor, abilityMod, slotLevel });
			roll = getRoll(formula, rollData);
		} else if (item != null) {
			roll = await ItemUtils.getAttackRoll(item);
		} else {
			return null;
		}

		// Construct the multiroll
		return await RollFields.constructMultiRoll({
			...options,
			formula: roll,
			rollState,
			title,
			critThreshold,
			elvenAccuracy,
			rollType: "attack",
		});
	}

	/**
	 * Constructs and rolls damage data for a damage entry in an item.
	 * Can roll for an index, versatile (replaces first), or other.
	 */
	static async constructDamageRoll(options: ConstructDamageRollOptions = {}): Promise<Optional<DamageDataProps>> {
		await waitForReady();

		const { item, damageIndex, slotLevel, isCrit } = options;
		const actor = options?.actor ?? item?.actor;
		const isVersatile = damageIndex === "versatile";
		const isFirst = damageIndex === 0 || isVersatile;
		const extraCritDice = options.extraCritDice ?? ItemUtils.getExtraCritDice(item);

		const settings = getSettings(options.settings);
		const { critBehavior } = settings;

		const rollData = item != null ? ItemUtils.getRollData(item, { slotLevel }) : actor?.getRollData();

		// Bonus parts to add to the formula
		const parts: string[] = [];

		// Determine certain fields based on index
		let { title, context, damageType, formula } = options;

		// If no formula was given, derive from the item
		if (formula == null && item != null) {
			const itemData = item.data.data;
			const flags = item.data.flags.betterRolls5e;

			if (damageIndex === "other") {
				formula = itemData.formula;
				title ??= i18n("br5e.chat.other");
				context ??= flags?.quickOther?.context;
			} else {
				// If versatile, use properties from the first entry
				const trueIndex = isFirst ? 0 : damageIndex ?? -1;

				formula = isVersatile ? itemData.damage?.versatile : itemData.damage?.parts?.[trueIndex][0];
				damageType = damageType ?? itemData.damage?.parts[trueIndex][1];
				context ??= flags?.quickDamage?.context?.[trueIndex];

				// Scale damage if its the first entry
				if (formula && isFirst) {
					formula = (await ItemUtils.scaleDamage(item, slotLevel, damageIndex, rollData)) ?? formula;
				}

				// Add any roll bonuses but only to the first entry
				const isAmmo = item.data.type === "consumable" && item.data.data.consumableType === "ammo";
				if (isFirst && rollData?.bonuses && !isAmmo) {
					const { actionType } = itemData;
					const bonus = actionType != null ? rollData?.bonuses[actionType]?.damage : null;
					if (bonus && parseInt(bonus, 10) !== 0) {
						parts.unshift(bonus);
					}
				}
			}
		}

		// Require a formula to continue
		if (!formula) {
			if (isVersatile) {
				ui.notifications.warn("br5e.error.noVersatile", { localize: true });
			}
			return null;
		}

		// Assemble roll data and defer to the general damage construction
		try {
			const rollFormula = [formula, ...parts].join("+");
			const baseRoll = (await new Roll(rollFormula, rollData).roll({ async: true })) as Evaluated<Roll>;
			const { total } = baseRoll;

			// Roll crit damage if relevant
			let critRoll: Optional<Roll> = null;
			if (damageIndex !== "other") {
				if (isCrit && critBehavior !== "0" && total != null) {
					critRoll = await ItemUtils.getCritRoll(baseRoll.formula, total, { settings, extraCritDice });
				}
			}

			return {
				type: "damage",
				damageIndex,
				title: options.title ?? title,
				damageType,
				context,
				extraCritDice,
				baseRoll,
				critRoll,
			};
		} catch (err) {
			await logErrorMessage(err);

			throw err; // propagate the error
		}
	}

	/**
	 * Constructs and rolls damage for a crit bonus entry.
	 * This is a simpler version of the regular damage entry.
	 */
	static async constructCritDamageRoll(
		options: ConstructCritDamageRollOptions = {}
	): Promise<Optional<CritDataProps>> {
		const { item, slotLevel } = options;
		const actor = options?.actor ?? item?.actor;
		const rollData = item ? ItemUtils.getRollData(item, { slotLevel }) : actor?.getRollData();

		// Determine certain fields based on index
		const { title, context, damageType } = options;
		let { formula } = options;

		// If no formula was given, derive from the item
		if (!formula && item) {
			formula = item.data.data.critical?.damage;
		}

		// Require a formula to continue
		if (!formula) {
			return null;
		}

		// Assemble roll data and defer to the general damage construction
		try {
			const critRoll = await getRoll(formula, rollData).roll({ async: true });

			return {
				type: "crit",
				title: options.title ?? title,
				damageType,
				context,
				critRoll,
			};
		} catch (err) {
			await logErrorMessage(err);

			throw err; // propagate the error
		}
	}

	/**
	 * Creates multiple item damage rolls. This returns an array,
	 * so when adding to a model list, add them separately or use the splat operator.
	 */
	static async constructItemDamageRange(options: ConstructItemDamageRangeOptions): Promise<DamageDataProps[]> {
		let indexes = options.index;
		const { formula, item } = options;

		// If formula is given or there is a single index, fallback to the singular function
		if (formula != null || Number.isInteger(indexes) || indexes == null) {
			let damageIndex = Number.isInteger(indexes) ? Number(indexes) : options.damageIndex;
			if (indexes === 0 && options.versatile) {
				damageIndex = "versatile";
			}

			if ((formula == null || formula === "") && damageIndex == null) {
				betterRollsError("Missing damage index on item range roll! Invalid data: ", options);

				return [];
			}

			const damageProp = await RollFields.constructDamageRoll({ ...options, damageIndex });
			if (damageProp == null) {
				return [];
			}

			return [damageProp];
		}

		const wasAll = indexes === "all";

		// If all, damage indices between a sequential list from 0 to length - 1
		if (indexes === "all") {
			const numEntries = item?.data.data.damage?.parts.length;
			indexes = [...Array(numEntries).keys()];
		}

		if (!Array.isArray(indexes)) {
			betterRollsError("Missing damage index on item range roll... invalid data");

			return [];
		}

		const damageProps: DamageDataProps[] = [];
		for (let i = 0; i < indexes.length; i++) {
			const dIndex = indexes[i];

			const damageIndex = options.versatile && wasAll && i === 0 ? "versatile" : dIndex;

			const damageProp = await RollFields.constructDamageRoll({ ...options, item, damageIndex });
			if (damageProp != null) {
				damageProps.push(damageProp);
			}
		}

		return damageProps;
	}

	/**
	 * Generates the html for a save button to be inserted into a chat message. Players can click this button to perform a roll through their controlled token.
	 */
	static constructSaveButton({
		item,
		abl = null,
		dc = null,
		context = null,
		settings,
	}: ConstructSaveButtonOptions): ButtonSaveProps {
		const actor = item?.actor;
		const saveData: Partial<SaveData> = (item != null ? ItemUtils.getSave(item) : null) ?? {};
		if (abl != null && abl !== "") {
			saveData.ability = abl;
		}

		if (dc != null) {
			saveData.dc = dc;
		}

		if (context != null) {
			saveData.context = context;
		}

		const { hideDC } = CONFIG.betterRolls5e;

		// Determine whether the DC should be hidden
		const hideDCSetting = getSettings(settings).hideDC;
		const shouldHideDC =
			hideDCSetting === hideDC.always || (hideDCSetting === hideDC.npcs && actor?.data.type === "npc");

		return { type: "button-save", hideDC: shouldHideDC, ...saveData };
	}

	/**
	 * Construct one or more model entries from a field and some metadata
	 * @param settings - BetterRoll settings overrides
	 */
	static async constructModelsFromField<F extends ModelFields>(
		field: F,
		metadata: ModelsMetadata,
		settings: Optional<FullPartial<Settings>> = {}
	): Promise<RenderModelEntry[]> {
		const result = await this._constructModelsFromField(field, metadata, settings);

		const [fieldType, sourceData] = field;
		betterRollsInfo(
			`Constructed field of type ${fieldType} with:`,
			"\nData:",
			sourceData,
			"\nMetadata:",
			metadata,
			"\nSettings:",
			settings,
			"\nResult:",
			result
		);

		return result;
	}

	static async _constructModelsFromField<F extends ModelFields>(
		field: F,
		metadata: ModelsMetadata,
		settings: Optional<FullPartial<Settings>> = {}
	): Promise<RenderModelEntry[]> {
		const [fieldType, sourceData] = field;

		const data = mergeObject(metadata, sourceData, { recursive: false });

		const { item: maybeItem, actor: maybeActor } = data;

		const item = typeof maybeItem === "string" ? ((await fromUuid(maybeItem)) as Item5e) : maybeItem;
		const actor = typeof maybeActor === "string" ? ((await fromUuid(maybeActor)) as Actor5e) : maybeActor;

		const fullSettings = getSettings(settings);

		type FieldDataType<K extends keyof ModelToData> = typeof metadata &
			(K extends unknown ? NeverOr<ModelToData[K], Record<string, unknown>> | undefined : never);

		// We have to cast `data` in each branch as the instance of `ModelFields` the type implies merged with the metadata.
		// There doesn't seem to be a nice way to get it to be inferred automatically.
		switch (fieldType) {
			case "header":
				return [RollFields.constructHeaderData(data as FieldDataType<"header">)];
			case "attack": {
				const attackRoll = await RollFields.constructAttackRoll(data as FieldDataType<"attack">);
				return attackRoll != null ? [attackRoll] : [];
			}

			case "toolcheck":
			case "tool":
			case "check": {
				const checkData = data as FieldDataType<"toolcheck" | "tool" | "check">;

				let rollFormula: Optional<string | Roll> = checkData?.formula;
				if (rollFormula == null && checkData?.item != null && checkData?.rollState != null) {
					rollFormula ??= (await ItemUtils.getToolRoll(checkData?.item, checkData?.rollState)).formula;
				}

				const multiRoll = await RollFields.constructMultiRoll({
					...checkData,
					formula: rollFormula,
				});
				return multiRoll != null ? [multiRoll] : [];
			}

			case "damage":
				return await RollFields.constructItemDamageRange(data as FieldDataType<"damage">);
			case "other":
				return await RollFields.constructItemDamageRange({
					...(data as FieldDataType<"other">),
					damageIndex: "other",
				});
			case "ammo": {
				if (data.ammo == null) {
					return [];
				}

				// Only add ammo damage if the ammunition is a consumable with type ammo
				const { ammo } = data;
				if (ammo.data.type !== "consumable" || ammo.data.data.consumableType !== "ammo") {
					return [];
				}

				return await RollFields.constructItemDamageRange({
					...(data as FieldDataType<"ammo">),
					item: ammo,
					index: "all",
					context: `${ammo.name}`,
				});
			}

			case "savedc":
				// {customAbl: null, customDC: null}
				return [
					RollFields.constructSaveButton({ settings: fullSettings, ...(data as FieldDataType<"other">) }),
				];
			case "ammosavedc":
				// {customAbl: null, customDC: null}
				if (data?.ammo == null || !isSave(data.ammo)) {
					return [];
				}

				return [
					RollFields.constructSaveButton({
						settings: fullSettings,
						...data,
						item: data.ammo,
						context: `${data.ammo.name}`,
					}),
				];
			case "custom": {
				const { title, rolls, formula, rollState } = data as FieldDataType<"custom">;
				const rollData = Utils.getRollData({ item, actor });
				const resolvedFormula = getRoll(formula, rollData).formula;
				const multiRollField = await RollFields.constructMultiRoll({
					title,
					rollState,
					formula: resolvedFormula || "1d20",
					numRolls: rolls || 1,
					rollType: "custom",
				});

				return multiRollField != null ? [multiRollField] : [];
			}

			case "description":
			case "desc":
			case "text": {
				const textData = data as FieldDataType<"description" | "desc" | "text">;
				const textFieldValue = textData.text ?? textData.content ?? item?.data.data.description?.value;
				if (textFieldValue) {
					return [
						{
							type: "description",
							content: TextEditor.enrichHTML(textFieldValue ?? "").trim(),
						},
					];
				}
				break;
			}

			case "flavor": {
				const message = (data as FieldDataType<"flavor">)?.text ?? item?.data.data?.chatFlavor;
				if (message) {
					return [
						{
							type: "description",
							isFlavor: true,
							content: message,
						},
					];
				}
				break;
			}

			case "crit": {
				const critResult = await RollFields.constructCritDamageRoll({
					item,
					...(data as FieldDataType<"crit">),
				});
				return critResult != null ? [critResult] : [];
			}

			default: {
				betterRollsError(`could not construct field for unknown type "${fieldType}" with data:`, data);
			}
		}

		return [];
	}
}

async function logErrorMessage(err: unknown) {
	await waitForReady();

	let msg: string;
	if (typeof err === "string") {
		msg = err;
	} else if (err instanceof Error) {
		msg = err.message;
	} else {
		msg = JSON.stringify(err);
	}

	ui.notifications.error(i18n("br5e.error.rollEvaluation", { msg }));
}

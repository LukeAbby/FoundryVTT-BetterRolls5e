import type { DICE_ROLL_MODES } from "@league-of-foundry-developers/foundry-vtt-types/src/foundry/common/constants.mjs";
import { waitForReady, getGame } from "./utils/readyTools";
import type { WhisperData } from "./utils/dice-collection";

import { isAttack, isSave, isCheck } from "./betterrolls5e.js";
import {
	i18n,
	DiceCollection,
	ActorUtils,
	ItemUtils,
	Utils,
	FoundryProxy,
	pick,
	findLast,
	betterRollsInfo,
	betterRollsError,
	betterRollsWarn,
	betterRollsVerbose,
} from "./utils/index.js";
import { getSettings } from "./settings.js";
import { isDND5eSkill, isEntryType, throwWithContext } from "./utils/guards.js";

import type { Optional, FullPartial } from "./lib/utils";
import {
	type DamageGroup,
	Renderer,
	type RenderModelEntry,
	type InitializedEntry,
	type RenderModelEntries,
	type MaybeInitializedEntry,
} from "./renderer.js";
import { type ModelsMetadata, type ModelFields, RollFields } from "./fields.js";
import type { GetRollStateOptions } from "./utils/utils";

import type { Settings } from "./settings";
import type { UsageUpdates } from "./lib/dnd5e/item5e";
import type * as DND5e from "./global.js";

/**
 * Parameters used when full rolling an actor
 */
type FullRollActorParams = GetRollStateOptions &
	FullPartial<{
		critThreshold: number;
	}>;

type ChatMessageData = Exclude<ConstructorParameters<typeof foundry.data.ChatMessageData>[0], undefined>;

/**
 * General class for macro support, actor rolls, and most static rolls.
 * It provides utility functions that can be used to create a new roll,
 * as well as the most common Actor roll functions.
 */
export class CustomRoll {
	/**
	 * Internal method to perform a basic actor full roll of "something".
	 * It creates and display a chat message on completion.
	 * @param actor - The actor being rolled for
	 * @param title - The title to show on the header card
	 * @param formula - The formula to multiroll
	 */
	static async _fullRollActor(
		actor: Actor5e,
		title: Optional<string>,
		formula: Formula,
		rollType?: RollType,
		params?: FullRollActorParams
	): Promise<CustomItemRoll> {
		// Entries to show for the render
		const rollState = Utils.getRollState({ event, ...params });
		const roll = new CustomItemRoll(actor, { rollState }, [
			["header", { title }],
			[
				"check",
				{
					formula,
					critThreshold: params?.critThreshold,
					rollType,
				},
			],
		]);
		await roll.toMessage();

		return roll;
	}

	/**
	 * Creates and displays a chat message to show a full skill roll
	 * @param skill - shorthand referring to the skill name
	 * @param params - parameters
	 */
	static async rollSkill(
		actor: Actor5e,
		skill: DND5e.Skills,
		params: FullRollActorParams = {}
	): Promise<CustomItemRoll> {
		if (!isDND5eSkill(skill)) {
			throwWithContext({
				message: [
					`Skill ${skill} does not exist.",
                    "Valid values are expected to be found in CONFIG.DND5E.skills, currently:"
                    "${Object.keys(CONFIG.DND5E.skills)}`,
				],
			});
		}

		const label = i18n(CONFIG.DND5E.skills[skill]);
		const { formula } = await ActorUtils.getSkillCheckRoll(actor, skill);

		return await CustomRoll._fullRollActor(actor, label, formula, "skill", params);
	}

	/**
	 * Rolls a skill check for an actor
	 * @param ability - Ability shorthand
	 */
	static async rollCheck(actor: Actor5e, ability: DND5e.Abilities, params: FullRollActorParams) {
		return await CustomRoll.rollAttribute(actor, ability, "check", params);
	}

	/**
	 * Rolls a saving throw for an actor
	 * @param ability - Ability shorthand
	 */
	static async rollSave(actor: Actor5e, ability: DND5e.Abilities, params: FullRollActorParams) {
		return await CustomRoll.rollAttribute(actor, ability, "save", params);
	}

	/**
	 * Creates and displays a chat message with the requested ability check or saving throw.
	 * @param actor - The actor object to reference for the roll.
	 * @param ability - The ability score to roll.
	 * @param rollType - String of either "check" or "save"
	 */
	static async rollAttribute(
		actor: Actor5e,
		ability: DND5e.Abilities,
		rollType: "check" | "save",
		params: FullRollActorParams = {}
	): Promise<CustomItemRoll> {
		const label = CONFIG.DND5E.abilities[ability];

		let titleString: Optional<string>;
		let formula = "";
		if (rollType === "check") {
			formula = (await ActorUtils.getAbilityCheckRoll(actor, ability)).formula;
			titleString = `${i18n(label)} ${i18n("br5e.chat.check")}`;
		} else if (rollType === "save") {
			formula = (await ActorUtils.getAbilitySaveRoll(actor, ability)).formula;
			titleString = `${i18n(label)} ${i18n("br5e.chat.save")}`;
		}

		return await CustomRoll._fullRollActor(actor, titleString, formula, rollType, params);
	}

	static newItemRoll(...args: ConstructorParameters<typeof CustomItemRoll>) {
		return new CustomItemRoll(...args);
	}
}

const defaultParams = {
	title: "",
	forceCrit: false,
	preset: null,
	properties: true,
	slotLevel: null,
	useCharge: {},
	useTemplate: false,
	event: null,
	advantage: 0 as const,
	disadvantage: 0 as const,
	consume: true,
	infoOnly: false,
	settings: null,
};

/*
	CustomItemRoll(
        item,
        {
            forceCrit: false,
            quickRoll: false,
            properties: true,
            slotLevel: null,
            useCharge: {},
            useTemplate: false,
            adv: 0,
            disadv: 0,
        },
        [
            ["attack", {triggersCrit: true}],
            ["damage", {index: 0, versatile: true}],
            ["damage", {index: [1, 2, 4]}],
        ]
	).toMessage();
*/

export type CustomItemRollParams = {
	title: string;
	forceCrit: boolean;
	preset: Optional<0 | 1>;
	properties: boolean;
	slotLevel: Optional<DND5e.SpellLevels>;
	useCharge: BetterRolls.UseCharge;
	useTemplate: boolean;
	event?: Optional<BasicEvent>;
	advantage: Optional<0 | 1 | boolean>;
	disadvantage: Optional<0 | 1 | boolean>;
	consume: boolean;
	consumeSpellLevel?: Optional<DND5e.ConsumeSpellLevels>;
	versatile?: Optional<boolean>;
	infoOnly: boolean;
	rollState?: Optional<RollState>;
	settings?: Optional<Settings>;
};

type ToMessageParams = FullPartial<{
	rollMode: DICE_ROLL_MODES;
	createMessage: boolean;
}>;

// A custom roll with data corresponding to an item on a character's sheet.
export class CustomItemRoll {
	/**
	 * Current id that is auto-incremented.
	 * IDs need to be unique within a card
	 */
	_currentId = 1;

	// Data results from fields, which get turned into templates
	// Entries represent "results"
	public entries: InitializedEntry[] = [];
	public error = false;
	public tokenId: Optional<string>;
	public isCrit: Optional<boolean>;
	public properties: string[];
	public params: CustomItemRollParams;
	public rolled: boolean;
	public dicePool: DiceCollection;
	public storedItemData: Optional<Item5e["data"]>;
	public actorId: Optional<string>;
	public messageId: Optional<string>;

	private _item: Optional<Item5e>;
	private itemId: Optional<string>;
	private _actor: Optional<Actor5e>;

	constructor(
		itemOrActor: Optional<Item5e | Actor5e>,
		params: Partial<CustomItemRollParams>,

		// Where requested roll fields are stored, in the order they should be rendered.
		// Fields represent "intents"
		public fields: ModelFields[] = []
	) {
		if (itemOrActor) {
			const { item, actor } = Utils.resolveActorOrItem(itemOrActor);
			if (item != null) {
				this.item = item;
			} else if (actor != null) {
				this.actor = actor;
			}
		}

		// params.event is an available parameter for backwards compatibility reasons
		// but really we're interested in the rollState. Replace it here.
		// We don't want event to be serialized when flags are set

		this.params = {
			...defaultParams,
			...(params ?? {}),
		};
		this.params.rollState = Utils.getRollState(this.params);
		delete this.params.event;

		this.entries = [];

		this.properties = [];
		this.rolled = false;
		this.isCrit = !!this.params.forceCrit; // Becomes "true" when a valid attack or check first crits.
		this.dicePool = new DiceCollection();
	}

	static fromMessage(message: ChatMessage) {
		const data = message.data.flags.betterrolls5e;
		const roll = new CustomItemRoll(null, data?.params ?? {}, data?.fields ?? []);

		roll._currentId = -1;
		roll.messageId = message.id;
		roll.rolled = true;

		if (data != null) {
			roll.isCrit = data.isCrit;
			roll.entries = FoundryProxy.create(data.entries);
			roll.properties = data.properties;
			roll.params = data.params;

			// Set these up so that lazy loading can be done
			roll.actorId = data.actorId;
			roll.itemId = data.itemId;
			roll.tokenId = data.tokenId;
		}

		roll.storedItemData = message.getFlag("dnd5e", "itemData");

		return roll;
	}

	/**
	 * Returns the actor associated this item, loading it from the game
	 */
	async getActor() {
		await waitForReady();

		if (this._actor) {
			return this._actor;
		}

		let actor: Optional<Actor5e>;
		if (this.tokenId) {
			const token = (await fromUuid(this.tokenId)) as Optional<TokenDocument>;
			actor = token?.actor;
		} else {
			actor = this.actorId != null ? game.actors?.get(this.actorId) : null;
		}

		this._actor = actor;
		return actor;
	}

	/**
	 * Retrieves the item associated with this roll. This may have to load the item from the associated actor,
	 * and in those cases can throw an exception if the actor/item no longer exists.
	 */
	async getItem(): Promise<Optional<Item5e>> {
		await waitForReady();

		if (this._item) {
			return this._item;
		}

		let storedData: Optional<Item5e["data"]>;
		if (this.messageId) {
			const message = game.messages?.get(this.messageId);
			storedData = message?.getFlag("dnd5e", "itemData");
		}

		if (storedData == null && !this.itemId) {
			return null;
		}

		const actor = await this.getActor();

		const embeddedItem =
			storedData != null && actor != null
				? ((await actor.createEmbeddedDocuments("Item", [
						storedData as unknown as Record<string, unknown>,
				  ])) as unknown as Item5e)
				: null;
		const item = embeddedItem ?? (this.itemId != null ? (actor?.items.get(this.itemId) as Item5e) : null);

		if (item != null) {
			betterRollsInfo(`Card loaded existing item data ${item.name}`);
		}

		ItemUtils.ensureFlags(item);

		this._item = item; // store a backup so we don't need to fetch again

		return item;
	}

	get actor() {
		if (this._actor) {
			return this._actor;
		}

		if (this.tokenId || this.actorId) {
			throwWithContext({
				message: ["Attempted to retrieve actor without loading, use getActor() instead"],
			});
		}

		return null;
	}

	set actor(actor: Optional<Actor5e>) {
		this._actor = actor;
		this.actorId = actor?.id;
		this.tokenId = actor?.token?.uuid;
	}

	get item() {
		if (this._item) {
			return this._item;
		}

		if (this.itemId) {
			throwWithContext({
				message: ["Attempted to retrieve item without loading, use getItem() instead"],
			});
		}

		return null;
	}

	set item(item: Optional<Item5e>) {
		ItemUtils.ensureFlags(item);
		this._item = item;
		this.itemId = item?.id;
		this.actor = item?.actor;
	}

	get settings() {
		return getSettings(this.params.settings);
	}

	/**
	 * Getter to retrieve if the current user has advanced permissions over the chat card.
	 */
	get hasPermission(): boolean {
		const game = getGame();

		const message = this.messageId != null ? game.messages?.get(this.messageId) : null;

		return !!game.user?.isGM || !!message?.isAuthor;
	}

	get totalDamage() {
		let total = 0;
		for (const entry of this.entries) {
			if (entry.type === "damage-group") {
				for (const subEntry of entry.entries) {
					total += subEntry.baseRoll?.total ?? 0;
					if (subEntry.revealed || entry.isCrit) {
						total += subEntry.critRoll?.total ?? 0;
					}
				}
			}
		}

		return total;
	}

	/**
	 * Generator to create an iterable flattened list
	 */
	*iterEntries(list: Optional<InitializedEntry[]> = null): IterableIterator<InitializedEntry> {
		for (const entry of list ?? this.entries) {
			yield entry;
			if (Array.isArray(entry.entries) && entry.type !== "multiroll") {
				yield* this.iterEntries(entry.entries);
			}
		}
	}

	/**
	 * Returns a list of all entries flattened, including the damage entries.
	 * Recomputes each time
	 */
	entriesFlattened(): InitializedEntry[] {
		return [...this.iterEntries()];
	}

	/**
	 * Returns an entry contained in this roll
	 */
	getEntry(id: Optional<string>): Optional<InitializedEntry> {
		for (const entry of this.iterEntries()) {
			if (entry.id === id) {
				return entry;
			}
		}

		return null;
	}

	/**
	 * Returns true if a damage entry can crit
	 */
	canCrit(entry: InitializedEntry) {
		const group = this.getEntry(entry?.group) ?? this.getEntry(entry?.id);
		if (group?.type !== "damage-group") {
			return false;
		}

		if (entry.critRoll || group.isCrit || entry?.damageIndex === "other") {
			return false;
		}

		const formula = entry.formula ?? entry.baseRoll?.formula;
		return ItemUtils.getBaseCritRoll(formula) != null;
	}

	/**
	 * Rolls crit dice if its not already rolled for the current card.
	 * This is used when *augmenting* an existing roll to a crit.
	 * @param groupId - updates the crit status for the specified group
	 * @param isCrit - Whether to enable or disable crits
	 * @returns if the crit roll went through
	 */
	async updateCritStatus(groupId: Optional<string>, isCrit: boolean): Promise<boolean> {
		const group = this.getEntry(groupId);
		if (group == null) {
			return false;
		}

		// If crits were forced on, can't turn them off
		if (!isCrit && group?.forceCrit) {
			return false;
		}

		// Do nothing if crits are disabled or if we don't have permission
		const { critBehavior } = this.settings;
		if ((isCrit && critBehavior === CONFIG.betterRolls5e.critBehavior.noExtraDamage) || !this.hasPermission) {
			return false;
		}

		const item = await this.getItem();
		const baseExtraCritDice = ItemUtils.getExtraCritDice(item);
		let updated = false;

		// Update group crit status
		group.isCrit = isCrit;

		// Update group entry crit statuses
		for (const entry of group?.entries ?? []) {
			if (!("type" in entry)) {
				continue;
			}

			// "Other" damage entries never crit
			if (entry.type === "damage" && entry.damageIndex !== "other") {
				if (isCrit && entry.critRoll == null) {
					// Enable Crit (from backup if available)
					if (entry._critBackup != null) {
						entry.critRoll = entry._critBackup;
					} else {
						const { formula, total } = entry.baseRoll;
						const extraCritDice = entry.extraCritDice ?? baseExtraCritDice;

						entry.extraCritDice = extraCritDice;
						entry.critRoll = await ItemUtils.getCritRoll(formula, total, {
							settings: this.settings,
							extraCritDice,
						});

						entry._critBackup = entry.critRoll; // prevent undoing the crit

						await this.dicePool.push(entry.critRoll);
					}

					updated = true;
				} else if (!isCrit && entry.critRoll && !entry._critBackup) {
					// Disable crit but keep a backup (so we don't re-roll the crit)
					entry._critBackup = entry.critRoll;
					entry.critRoll = undefined;
					updated = true;
				}
			}

			// If crit extra, show/hide depending on setting
			if (entry.type === "crit") {
				entry.revealed = entry._diceRolled ? true : isCrit;
				if (!entry._diceRolled && entry.revealed) {
					await this.dicePool.push(entry.critRoll);
				}

				// Always set to true. If hidden > revealed, we just rolled it.
				// If revealed > hidden, it was rolled before, so set to true anyways
				entry._diceRolled = true;
			}
		}

		return updated;
	}

	/**
	 * Like setCritStatus(), but sets the forceCrit flag, preventing the crit from being unset by things like the attack roll being converted to disadvantage.
	 * @param group - group to update. If not set will force crit on the whole roll.
	 * @returns true if the status changed for a group.
	 */
	async forceCrit(group?: Optional<string>): Promise<boolean> {
		// Force crit on the whole roll recursively
		if (group == null) {
			let updated = false;

			const damageGroups = new Set(
				this.entries
					.filter((g): g is InitializedEntry<"damage-group"> => g.type === "damage-group")
					.map((g) => g.id)
			);

			for (const damageGroup of damageGroups) {
				if (await this._forceCritForGroup(damageGroup)) {
					updated = true;
				}
			}

			return updated;
		}

		return await this._forceCritForGroup(group);
	}

	/**
	 * Internal helper to force crit for a specific group
	 */
	async _forceCritForGroup(groupId: Optional<string>): Promise<boolean> {
		const updated = await this.updateCritStatus(groupId, true);

		// Note: if there's no group then forceCrit can't be set,
		// however currently the only way a crit could be unset is by converting to disadvantage
		// If there's no attack roll...it can't be unset anyways, so no big deal
		const group = this.getEntry(groupId);
		if (updated || (group?.isCrit && !group?.forceCrit)) {
			if (group != null) {
				group.forceCrit = true;
			}

			return true;
		}

		return updated;
	}

	/**
	 * Rolls damage for a damage group. Returns true if successful.
	 * This works by revealing all relevant hidden damage data, and visually rolling dice
	 */
	async rollDamage(id: Optional<string>): Promise<boolean> {
		if (id == null || id === "") {
			let updated = false;
			const groups = this.entries.filter((e) => e.type === "damage-group");
			for (const group of groups) {
				if (await this.rollDamage(group.id)) {
					updated = true;
				}
			}

			return updated;
		}

		const group = this.getEntry(id) as InitializedEntry<"damage-group">;
		const wasHidden = group?.prompt;
		if (!this.hasPermission || group == null || !wasHidden) {
			return false;
		}

		// Get whether this was a crit or not
		const isCrit = group.isCrit || this.isCrit;

		// Disable the prompt for this group
		group.prompt = false;

		// Add to dicepool for dice so nice
		for (const entry of group.entries) {
			if (entry.type === "damage") {
				await this.dicePool.push(entry.baseRoll);
			}

			if (entry.type === "crit" && isCrit && entry.critRoll != null) {
				await this.dicePool.push(entry.critRoll);
			}
		}

		return true;
	}

	/**
	 * Assigns a RollState to an existing multiroll entry. Cannot be used to unset it.
	 * @param id - The id of the rollstate entry to update
	 */
	async updateRollState(id: Optional<string>, rollState: Optional<RollState>): Promise<boolean> {
		if (!this.hasPermission || !this.entries.length || rollState == null) {
			return false;
		}

		const multiroll = this.getEntry(id);
		if (multiroll?.type !== "multiroll" || multiroll.rollState != null) {
			return false;
		}

		// Calculate required number of rolls
		let numRolls = Math.max(multiroll.entries.length, 2);

		// In the case of elven accuracy allow 3 rolls
		if (numRolls === 2 && multiroll.elvenAccuracy && rollState !== "lowest") {
			numRolls = 3;
		}

		// Add more rolls if necessary, all copies of the first roll.
		const firstRoll = multiroll.entries[0].roll;

		while (multiroll.entries.length < numRolls) {
			const roll = await firstRoll.reroll({ async: true });

			const entry = Utils.processRoll(roll, multiroll.critThreshold, [20], multiroll.bonus);
			if (entry != null) {
				multiroll.entries.push(entry);
			}

			await this.dicePool.push(roll);
		}

		// Determine roll result
		const rollTotals = multiroll.entries.map((r) => r.roll.total).filter((t): t is number => t != null);
		let chosenResult = rollTotals[0];
		if (rollState === "highest") {
			chosenResult = Math.max(...rollTotals);
		} else if (rollState === "lowest") {
			chosenResult = Math.min(...rollTotals);
		}

		// Mark the non-results as ignored
		multiroll.entries = multiroll.entries.map((r) => ({
			...r,
			ignored: r.roll.total !== chosenResult,
		}));

		// Update remaining properties
		// Update crit status if not forcing crit
		multiroll.rollState = rollState;
		if (!multiroll.forceCrit) {
			const group = this.entries.find((e) => e.attackId === multiroll.id);
			await this.updateCritStatus(group?.id, multiroll.isCrit);
		}

		return true;
	}

	/**
	 * Internal function to process fields and populate the internal data.
	 * Call toMessage() to create the final chat message and show the result
	 */
	async roll() {
		if (this.rolled) {
			betterRollsInfo("Already rolled!", this);
			return;
		}

		Hooks.call("preRollItemBetterRolls", this);

		const item = await this.getItem();
		const actor = await this.getActor();

		// Pre-update item configurations which updates the params
		if (item != null) {
			// Set up preset but only if there aren't fields
			if (this.fields == null || this.fields.length === 0) {
				this.params.preset ??= 0;
				if (Number.isInteger(this.params.preset)) {
					await this.updateForPreset();
				}
			}
		}

		// Get Params
		const { params } = this;
		const { consume, slotLevel } = params;

		let placeTemplate = params.useTemplate;

		// Determine spell level and configuration settings
		if (item?.data.type === "spell" && consume && !slotLevel) {
			const config = await this.configureSpell();
			if (config === "error") {
				this.error = true;
				return;
			}

			placeTemplate = config.placeTemplate;
		}

		// Update item casted level property to match the slot level
		// This ensure things like the property list showing the correct spell level
		if (item && slotLevel) {
			item.data.data.castedLevel = slotLevel;
		}

		const couldQuery = this._queryRollState();
		if (!couldQuery) {
			this.error = true;
			return;
		}

		// Check if we need to consume ammo. It will be consumed later (so that attack calcs work properly)
		// This consumes even if consuming is globally disabled. Roll repeats need to consume ammo.
		const [couldGet, ammo, ammoUpdate] = await this._getAmmoConsume(actor, item);
		if (!couldGet) {
			this.error = true;
			return;
		}

		// Process all fields (this builds the data entries)
		for (const field of this.fields) {
			await this._processField(field);
		}

		// Consume ammo (now that fields have been processed)
		if (ammo && !isObjectEmpty(ammoUpdate)) {
			await ammo.update(ammoUpdate);
		}

		// Post-build item updates
		if (item) {
			// Check to consume charges. Prevents the roll if charges are required and none are left.
			if (consume) {
				const chargeCheck = await this.consume();
				if (chargeCheck === "error") {
					this.error = true;
					return;
				}
			}

			// Load Item Footer Properties if params.properties is true
			if (params.properties) {
				this.properties = ItemUtils.getPropertyList(item);
			}

			// Place the template if applicable
			if (placeTemplate) {
				await ItemUtils.placeTemplate(item);
			}
		}

		this.rolled = true;
		this.error = false;

		await Hooks.callAll("rollItemBetterRolls", this);
		await new Promise((r) => {
			setTimeout(r, 25);
		});

		// Add models (non-hidden) to dice pool (for 3D Dice)
		await this._addToDicePool();
	}

	async _queryRollState(): Promise<boolean> {
		if (this.params.rollState != null) {
			return true;
		}

		const { queryAdvantageEnabled } = getSettings();

		// Only show Advantage/Normal/Disadvantage dialog if allowed to by settings.
		if (queryAdvantageEnabled) {
			// Skip showing the dialog if a roll state is already given in all required fields.
			const rollStateKnown = this.fields.every((field) => {
				const [fieldType, fieldData] = field;

				if (!["attack", "check", "custom", "tool", "toolcheck"].includes(fieldType)) {
					return true;
				}

				return fieldData != null && "rollState" in fieldData && fieldData.rollState != null;
			});

			if (rollStateKnown) {
				return true;
			}

			const rollState = await this._openRollStateDialog();
			if (rollState === "cancel") {
				return false;
			}

			this.params.rollState = rollState;
		}

		return true;
	}

	async _openRollStateDialog(): Promise<RollState | "cancel"> {
		const rollState = await new Promise((resolve: (value: RollState | "cancel") => void) => {
			new Dialog({
				title: i18n("br5e.querying.title"),
				content: "",
				default: "",
				buttons: {
					disadvantage: {
						label: i18n("br5e.querying.disadvantage"),
						callback: () => resolve("lowest"),
					},
					normal: {
						label: i18n("br5e.querying.normal"),
						callback: () => resolve("first"),
					},
					advantage: {
						label: i18n("br5e.querying.advantage"),
						callback: () => resolve("highest"),
					},
				},
				close: () => resolve("cancel"),
			}).render(true);
		});

		return rollState;
	}

	async _getAmmoConsume(
		actor: Optional<Actor5e>,
		item: Optional<Item5e>
	): Promise<[boolean, ItemOfType<"consumable"> | undefined, Record<string, string>]> {
		let ammo: ItemOfType<"consumable"> | undefined;
		let ammoUpdate: Record<string, string> = {};
		if (actor != null && item != null) {
			const request = this.params.useCharge;
			const consumeData = item.data.data.consume;
			if (request.resource && consumeData?.type === "ammo") {
				ammo = actor.items.get(consumeData.target) as ItemOfType<"consumable"> | undefined;
				const usage = item._getUsageUpdates({ consumeResource: true });
				if (usage === false) {
					return [false, undefined, {}];
				}
				ammoUpdate = usage.resourceUpdates || {};
			}
		}

		return [true, ammo, ammoUpdate];
	}

	async _addToDicePool(): Promise<void> {
		for (const entry of this.entries) {
			if (entry.type === "multiroll") {
				await this.dicePool.push(...entry.entries.map((e) => e.roll), entry.bonus);
			} else if (entry.type === "damage-group" && !entry.prompt) {
				for (const subEntry of entry.entries) {
					if (subEntry.type === "damage" || (subEntry.type === "crit" && subEntry.revealed)) {
						await this.dicePool.push(subEntry.baseRoll, subEntry.critRoll);
					}
				}
			}
		}
	}

	public content: Optional<string>;

	async render(): Promise<string> {
		this.content = await Renderer.renderCard(this);

		await Hooks.callAll("renderBetterRolls", this);

		return this.content;
	}

	/**
	 * Allows this roll to be serialized into message flags.
	 */
	_getFlags() {
		// Make sure fields are serializable
		// This involves normalising the formula field into a string and swapping out actors and items for their uuids.
		const fields = this.fields.map((field) => {
			const [fieldName, fieldData] = deepClone(field) as typeof field;

			if (fieldData != null) {
				if ("formula" in fieldData) {
					const { formula } = fieldData;

					fieldData.formula = typeof formula === "string" ? formula : formula.formula;
				}

				if (fieldData.actor instanceof Actor) {
					fieldData.actor = fieldData.actor.uuid;
				}

				if (fieldData.item instanceof Item) {
					fieldData.item = fieldData.item.uuid;
				}
			}

			return [fieldName, fieldData];
		}) as typeof this["fields"];

		const flags: FlagConfig["ChatMessage"] = {
			betterrolls5e: {
				version: Utils.getVersion(),
				actorId: this.actorId,
				itemId: this.itemId,
				tokenId: this.tokenId,
				isCrit: this.isCrit,
				entries: this.entries,
				properties: this.properties,
				params: this.params,
				fields,
			},

			core: {
				canPopout: true,
			},
		};

		if (this.fields.some((f) => f[0] === "attack")) {
			setProperty(flags, "dnd5e.roll.type", "attack");
		}

		if (this.itemId) {
			setProperty(flags, "dnd5e.roll.itemId", this.itemId);
		}

		// If the item was destroyed in the process of displaying its card or is a temp item with non-transfer effects,
		// then embed the item data in the chat message
		// We retrieve the private internal cached actor/items so that this method can stay synchronous.
		const { _actor, _item } = this;
		const wasConsumed = _item?.data.type === "consumable" && _item?.id != null && !_actor?.items.has(_item?.id);
		if (wasConsumed || (_item && !_item.id && _item?.data.effects.find((ae) => !ae.data.transfer))) {
			setProperty(flags, "dnd5e.itemData", _item.data);
		}

		return flags;
	}

	/**
	 * Creates and sends a chat message to all players (based on whisper config).
	 * If not already rolled and rendered, roll() is called first.
	 * @returns the created chat message
	 */
	async toMessage({ rollMode = null, createMessage = true }: ToMessageParams = {}): Promise<
		Optional<ChatMessage | (ChatMessageData & WhisperData)>
	> {
		await waitForReady();

		betterRollsVerbose("Creating custom roll chat message with arguments:", { rollMode, createMessage });

		if (!this.rolled) {
			betterRollsVerbose("Rolling for chat message");

			await this.roll();
		}

		if (this.error) {
			return null;
		}

		const item = await this.getItem();
		const actor = (await this.getActor()) ?? item?.actor;
		const hasMaestroSound = item && ItemUtils.hasMaestroSound(item);

		// Create the chat message
		const chatAndWhisperData: ChatMessageData & WhisperData = {
			user: game.user?.id,
			content: await this.render(),
			speaker: ChatMessage.getSpeaker({ actor }),
			flags: this._getFlags(),
			type: CONST.CHAT_MESSAGE_TYPES.ROLL,
			...Utils.getWhisperData(rollMode),

			// If not blank, D&D will try to modify the card...
			roll: await new Roll("0").roll({ async: true }),
		};

		await Hooks.callAll("messageBetterRolls", this, chatAndWhisperData);
		await this.dicePool.flush({ hasMaestroSound, whisperData: chatAndWhisperData });

		// Send the chat message
		if (createMessage) {
			betterRollsVerbose("Creating chat message with data:", chatAndWhisperData);

			const message = await ChatMessage.create(chatAndWhisperData);
			this.messageId = message?.id;

			return message;
		}

		return chatAndWhisperData;
	}

	/**
	 * Returns true if the item can be rerolled.
	 * Items that only have text field types cannot be rerolled.
	 */
	canRepeat() {
		if (!this.hasPermission || !this.fields || this.fields.length === 0) {
			return false;
		}

		const surplusTypes = new Set(["header", "flavor", "description", "desc"]);
		for (const field of this.fields) {
			if (!surplusTypes.has(field[0])) {
				return true;
			}
		}

		return false;
	}

	/**
	 * Repeats the roll, sending out a new chat message.
	 * This repeat does not consume uses, and overrides advantage/disadvantage / prompts
	 */
	async repeat(options: GetRollStateOptions = {}): Promise<Optional<CustomItemRoll>> {
		await waitForReady();

		if (!this.canRepeat()) {
			return null;
		}

		// Show error messages if the item/actor was deleted
		const subject = (await this.getItem()) ?? (await this.getActor());
		if ((this.itemId || this.actorId) && !subject) {
			const actor = await this.getActor();
			const message = actor ? i18n("br5e.error.noItemWithId") : i18n("br5e.error.noActorWithId");
			ui.notifications.error(message);

			throw new Error(message);
		}

		const invalidFields = ["description", "desc"];
		const fields = duplicate(this.fields.filter((f) => !invalidFields.includes(f[0]))) as ModelFields[]; // duplicate needs the hint that `this.fields` is really fully duplicate-able.

		const params = duplicate(this.params) as typeof this.params;
		params.consume = false;
		params.rollState = Utils.getRollState(options);

		const newRoll = new CustomItemRoll(subject, params, fields);
		await newRoll.toMessage();
		return newRoll;
	}

	/**
	 * Updates the associated chat message to have this HTML as its content.
	 * Nothing updates until this method is called.
	 * Requires the chat message to already exist.
	 */
	async update(additional = {}) {
		await waitForReady();

		const chatMessage = this.messageId != null ? game.messages?.get(this.messageId) : null;
		if (chatMessage == null) {
			betterRollsWarn("Could not update", this, "\nno message for id:", this.messageId);

			return;
		}

		const item = await this.getItem();

		const hasMaestroSound = ItemUtils.hasMaestroSound(item);
		const content = await this.render();

		betterRollsVerbose(
			"updating CustomItemRoll:",
			this,
			"\nitem:",
			item,
			"\nhasMaestroSound:",
			hasMaestroSound,
			"\nadditional:",
			additional
		);
		await Hooks.callAll("updateBetterRolls", this, content);

		await this.dicePool.flush({ hasMaestroSound });

		await chatMessage.update(
			{
				...flattenObject({ flags: duplicate(this._getFlags()) }),
				content,
				...additional,
			},
			{ diff: true }
		);
	}

	/**
	 * Equivalent to addField()
	 */
	fieldToTemplate(field: ModelFields) {
		return this.addField(field);
	}

	/**
	 * Adds to the list of fields, and if already rolled, processes it immediately
	 */
	async addField(field: ModelFields): Promise<void>;
	async addField(fieldName: ModelFields[0], data: ModelFields[1]): Promise<void>;

	async addField(fieldOrFieldName: ModelFields | ModelFields[0], data?: ModelFields[1]) {
		// Backwards compatibility. String+data or an array of [name, data] are both supported
		let field: ModelFields;
		if (typeof fieldOrFieldName === "string") {
			field = [fieldOrFieldName, data] as ModelFields;
		} else {
			if (!Array.isArray(fieldOrFieldName)) {
				betterRollsError(
					`Invalid arguments to addField. Expected a field name or an entire field as the first parameter, got "${fieldOrFieldName}"`
				);

				return;
			}

			field = fieldOrFieldName as ModelFields;
		}

		if (this.rolled) {
			await this._processField(field);
		} else {
			this.fields.push(field);
		}
	}

	/**
	 * Function that immediately processes the field and adds the result to data
	 */
	async _processField(field: ModelFields) {
		const actor = await this.getActor();
		const item = await this.getItem();

		const consume = item?.data.data.consume;

		const ammo = consume?.type === "ammo" ? actor?.items.get(consume.target) : null;

		const metadata: ModelsMetadata = {
			item,
			actor,
			rollState: this.params.rollState,
			ammo,
			slotLevel: this.params.slotLevel,
			isCrit: this.isCrit,
			settings: this.settings,
		};

		// Add non-null entries
		const newEntries = await RollFields.constructModelsFromField(field, metadata, this.settings);
		newEntries.forEach(this._addRenderEntry.bind(this));
	}

	/**
	 * Adds a render entry to the list. Does not add to dicepool, but does flag for crit.
	 * If damage prompt is enabled, any damage entries will be hidden unless hidden has a value.
	 */
	private _addRenderEntry<K extends keyof RenderModelEntries>(entry: Optional<RenderModelEntry<K>>) {
		if (entry == null) {
			return;
		}

		if (isEntryType(entry, "multiroll")) {
			if (!this.params.forceCrit && (entry.triggersCrit ?? true)) {
				this.isCrit = entry.isCrit;
			}
		}

		const clonedEntry = {
			...(entry as RenderModelEntries[K]),
			id: randomID(),
		} as MaybeInitializedEntry<K>;

		// Assign groups for damage
		if (clonedEntry.type === "damage" || clonedEntry.type === "crit") {
			// Try to add to the last existing group, however multiroll/saves are junctions (stopping points)
			let groupEntry = findLast(
				this.entries,
				(e): e is InitializedEntry<"damage-group" | "multiroll" | "button-save"> =>
					["damage-group", "multiroll", "button-save"].includes(e?.type)
			);

			// Create group if does not exist
			if (groupEntry?.type !== "damage-group") {
				// Find last attack roll, and link to it
				const lastAttack = findLast(
					this.entries,
					(e): e is InitializedEntry<"multiroll"> => e.type === "multiroll"
				);
				const hasAttackOrSave = this.entries.some((e) => e.type === "multiroll" || e.type === "button-save");

				const damageGroup: DamageGroup = {
					id: randomID(),
					type: "damage-group",
					attackId: lastAttack?.id,
					isCrit: !!lastAttack?.isCrit,
					prompt: hasAttackOrSave && this.settings.damagePromptEnabled,
					entries: [],
				};

				groupEntry = damageGroup;

				this.entries.push(groupEntry);
			}

			// Reveal if this was a crit entry, and the group crit
			if (clonedEntry.type === "crit" && groupEntry.isCrit) {
				clonedEntry.revealed = true;
			}

			// Assign an id and add to group
			clonedEntry.group = groupEntry.id;
			groupEntry.entries.push(clonedEntry as InitializedEntry<"damage" | "crit">);
		} else {
			// Non-damage entry. Assign an id and add to list
			this.entries.push(clonedEntry as InitializedEntry);
		}
	}

	/**
	 * Updates the rollRequests based on the br5e flags.
	 * This creates the default set of fields to process.
	 */
	async updateForPreset() {
		const item = await this.getItem();
		if (item == null) {
			return;
		}

		const itemData = item.data.data;
		const { flags } = item.data;
		const brFlags = flags.betterRolls5e;
		const { preset } = this.params;

		let properties = false;
		let useCharge: BetterRolls.UseCharge = {};
		let useTemplate = false;

		const fields: ModelFields[] = [];
		const val = preset === 1 ? ("altValue" as const) : ("value" as const);

		fields.push(["header"]);

		if (brFlags != null) {
			// Assume new action of the button based on which fields are enabled for Quick Rolls
			const flagIsTrue = function (flag: keyof typeof brFlags): boolean {
				return !!brFlags[flag]?.[val];
			};

			// Should Typescript add proper invariance ever this would be a great case for it

			// Returns the flag or alt-flag depending on setting
			const getFlag = function (flag: keyof typeof brFlags): unknown {
				return brFlags[flag]?.[val];
			};

			if (flagIsTrue("quickFlavor") && itemData.chatFlavor) {
				fields.push(["flavor"]);
			}

			if (flagIsTrue("quickDesc")) {
				fields.push(["desc"]);
			}

			if (flagIsTrue("quickAttack") && isAttack(item)) {
				fields.push(["attack"]);
			}

			if (flagIsTrue("quickCheck") && isCheck(item)) {
				fields.push(["check"]);
			}

			if (flagIsTrue("quickSave") && isSave(item)) {
				betterRollsInfo("quickSave true and isSave =", item);
				fields.push(["savedc"]);
			}

			if (flagIsTrue("quickSave")) {
				fields.push(["ammosavedc"]);
			}

			const quickDamage = Object.entries((getFlag("quickDamage") as Optional<boolean[]>) ?? []);
			if (quickDamage.length > 0) {
				for (const [i, damage] of quickDamage) {
					const index = Number(i);
					const versatile = index === 0 && flagIsTrue("quickVersatile");
					if (damage) {
						fields.push(["damage", { index, versatile }]);
					}
				}

				// Roll ammo after damage (if any)
				fields.push(["ammo"]);
			}

			if (flagIsTrue("quickOther") && itemData?.formula) {
				fields.push(["other"]);
			}

			if (flagIsTrue("quickProperties")) {
				properties = true;
			}

			if (brFlags.quickCharges) {
				useCharge = duplicate(getFlag("quickCharges") as BetterRolls.UseCharge);
			}

			if (flagIsTrue("quickTemplate")) {
				useTemplate = true;
			}

			fields.push(["crit"]);
		} else {
			// betterRollsInfo("Request made to Quick Roll item without flags!");
			fields.push(["desc"]);
			properties = true;
		}

		const params = {
			properties,
			useCharge,
			useTemplate,
		};

		this.params = mergeObject(this.params, params);

		this.fields = fields.concat((this.fields || []).slice());

		betterRollsVerbose("Got preset data from item:", item, "\nPreset params:", params, "\nPreset fields:", fields);
	}

	/**
	 * Determine the spell level and spell slot consumption if this is an item,
	 * and returns the spell configuration, or "error" on forced close.
	 */
	async configureSpell() {
		await waitForReady();

		const item = await this.getItem();
		const actor = await this.getActor();

		let spellLevel: Optional<DND5e.UseSpellLevels> = null;
		let consumeSlot = false;
		let placeTemplate = false;

		const data = item?.data.data;

		// Only run the dialog if the spell is not a cantrip
		const isSpell = item?.type === "spell";
		const requireSpellSlot =
			isSpell &&
			data?.level != null &&
			data.level > 0 &&
			(CONFIG.DND5E.spellUpcastModes as string[]).includes(data.preparation.mode);

		if (requireSpellSlot) {
			// The ability use dialog shows consumption prompts, but we cannot control the default values
			// therefore we have to remove them then add them back.
			const propsToRemove = ["uses", "recharge"] as const;
			let extracted = {};
			try {
				extracted = pick(data, propsToRemove);
				propsToRemove.forEach((prop) => {
					delete data[prop];
				});

				const spellFormData = await game.dnd5e.applications.AbilityUseDialog.create(item);
				if (spellFormData == null) {
					return "error";
				}

				spellLevel = spellFormData.level as DND5e.UseSpellLevels;
				consumeSlot = Boolean(spellFormData.consumeSlot);
				placeTemplate = Boolean(spellFormData.placeTemplate);
			} catch (error) {
				betterRollsError(error);
				return "error";
			} finally {
				// Restore the stripped props
				mergeObject(data, extracted);
			}
		} else {
			// If there's no dialog, always show the template if enabled
			placeTemplate = !!item?.hasAreaTarget && this.params.useTemplate;
		}

		// If consume is enabled, mark which slot is getting consumed
		let consume: Optional<DND5e.ConsumeSpellLevels>;

		// `spellLevel != null` is implied by `consumeSlot` which can only be set to true alongside setting `spellLevel`
		// However we have to hint this to Typescript's inference engine.
		if (consumeSlot && spellLevel != null) {
			consume = spellLevel === "pact" ? "pact" : `spell${spellLevel}`;
		}

		if (spellLevel === "pact") {
			spellLevel = actor != null ? getProperty(actor, "data.data.spells.pact.level") : null;
		}

		const lvl =
			spellLevel != null
				? (parseInt(spellLevel as Exclude<typeof spellLevel, "pact">, 10) as DND5e.SpellLevels)
				: null;

		// Update params temporarily
		this.params.slotLevel = lvl;
		this.params.consumeSpellLevel = consume;

		return { lvl, consume, placeTemplate };
	}

	/**
	 * Consumes charges & resources assigned on an item.
	 * NOTE: As of D&D System 1.2, all of this can now be handled internally by Item._handleConsumeResource.
	 * This was tweaked to support 1.2, but we are waiting and seeing before moving everything over.
	 * We might no longer need specialized use/consume code.
	 */
	private async consume() {
		await waitForReady();

		const actor = await this.getActor();
		const baseItem = await this.getItem();
		if (baseItem?.id == null) {
			return null;
		}

		// The error message for spells uses the item level, so we tweak so that it reports correctly
		const itemLevel = this.params.slotLevel ?? baseItem.data.data.level;
		const item = await baseItem.clone({ "data.level": itemLevel }, { save: false, keepId: true });
		if (item == null) {
			betterRollsWarn("Could not clone item:", baseItem);
			return null;
		}

		let actorUpdates = {};
		let itemUpdates: Record<string, unknown> = {};
		let resourceUpdates = {};

		// Merges update data from _getUsageUpdates() into the result dictionaries
		// Note: mergeObject() also resolves "." nesting which is not what we want
		function mergeUpdates(updates: UsageUpdates) {
			actorUpdates = { ...actorUpdates, ...(updates.actorUpdates ?? {}) };
			itemUpdates = { ...itemUpdates, ...(updates.itemUpdates ?? {}) };
			resourceUpdates = { ...resourceUpdates, ...(updates.resourceUpdates ?? {}) };
		}

		const itemData = item.data.data;
		const hasUses = !!(Number(itemData.uses?.value) || itemData.uses?.per); // Actual check to see if uses exist on the item, even if params.useCharge.use == true
		const hasResource = !!itemData.consume?.target; // Actual check to see if a resource is entered on the item, even if params.useCharge.resource == true

		const request = this.params.useCharge; // Has bools for quantity, use, resource, and charge
		const { recharge } = itemData;
		const { quantity } = itemData;
		const { autoDestroy } = itemData.uses || {};

		let output = "success";

		// Identify what's being consumed. Note that ammo is consumed elsewhere
		const { consumeSpellLevel } = this.params;
		const consumeResource = hasResource && request?.resource && itemData.consume?.type !== "ammo";
		const consumeUsage = request?.use && hasUses;
		const consumeQuantity = request?.quantity || autoDestroy;
		const consumeCharge = request?.charge && recharge?.value;

		// Check for consuming quantity, but not uses
		if (request.quantity && !consumeUsage) {
			if (!quantity) {
				ui.notifications.warn(game.i18n.format("DND5E.ItemNoUses", { name: item.name }));
				return "error";
			}
		}

		// Check for consuming charge ("Action Recharge")
		if (consumeCharge && !recharge.charged) {
			ui.notifications.warn(game.i18n.format("DND5E.ItemNoUses", { name: item.name }));
			return "error";
		}

		// Consume resources and spell slots
		if (consumeResource || consumeSpellLevel) {
			const updates = item._getUsageUpdates({ consumeResource, consumeSpellLevel: consumeSpellLevel ?? null });
			if (!updates) {
				return "error";
			}
			mergeUpdates(updates);
		}

		// Consume quantity and uses
		if (consumeQuantity && !consumeUsage) {
			// Handle quantity when uses are not consumed
			// While the rest can be handled by Item._getUsageUpdates() as of DND 1.2.0, this one thing cannot
			itemUpdates["data.quantity"] = Math.max(0, (quantity ?? 0) - 1);
		} else if (consumeUsage) {
			// Handle cases where charge consumption is a thing (uses with quantity consumption OR auto destroy)
			const updates = item._getUsageUpdates({ consumeUsage, consumeQuantity });
			if (!updates) {
				return "error";
			}

			mergeUpdates(updates);

			// Work around a bug in Item._getUsageUpdates(). Once this bug is fixed we can remove this code
			if (itemUpdates["data.quantity"] === 0) {
				itemUpdates["data.uses.value"] = 0;
			}
		}

		// Handle charge ("Action Recharge")
		if (request.charge) {
			itemUpdates["data.recharge.charged"] = false;
		}

		if (!isObjectEmpty(itemUpdates)) {
			await item.update(itemUpdates);
		}

		if (!isObjectEmpty(actorUpdates)) {
			await actor?.update(actorUpdates);
		}

		if (!isObjectEmpty(resourceUpdates) && itemData.consume?.target != null) {
			const resource = actor?.items.get(itemData.consume.target);
			if (resource != null) {
				await resource.update(resourceUpdates);
			}
		}

		// Destroy item if it gets consumed
		if (itemUpdates["data.quantity"] === 0 && autoDestroy) {
			output = "destroy";
			await actor?.deleteEmbeddedDocuments("Item", [baseItem?.id]);
		}

		return output;
	}
}

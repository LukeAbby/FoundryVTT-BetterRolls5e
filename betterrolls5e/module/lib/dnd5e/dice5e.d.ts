import type D20Roll from "./dice/d20-roll";

export { default as D20Roll } from "./dice/d20-roll";

/** Formatting options */
export type SimplifyRollFormulaOptions = {
	/** Preserve flavor text in the simplified formula */
	preserveFlavor: boolean;
};

/**
 * A standardized helper function for simplifying the constant parts of a multipart roll formula.
 *
 * @returns The resulting simplified formula
 */
export declare function simplifyRollFormula(formula: string, { preserveFlavor }: SimplifyRollFormulaOptions): string;

export type D20RollOptions<D extends object> = Partial<{
	/** The dice roll component parts, excluding the initial d20 */
	parts: string[];

	/** Actor or item data against which to parse the roll */
	data: D;

	/** Apply advantage to the roll (unless otherwise specified) */
	advantage: boolean;

	/** Apply disadvantage to the roll (unless otherwise specified) */
	disadvantage: boolean;

	/** The value of d20 result which represents a critical success */
	critical: number;

	/** The value of d20 result which represents a critical failure */
	fumble: number;

	/** Assign a target value against which the result of this roll should be compared */
	targetValue: number;

	/** Allow Elven Accuracy to modify this roll? */
	elvenAccuracy: boolean;

	/** Allow Halfling Luck to modify this roll? */
	halflingLucky: boolean;

	/** Allow Reliable Talent to modify this roll? */
	reliableTalent: boolean;

	/** Choose the ability modifier that should be used when the roll is made */
	chooseModifier: boolean;

	/** Allow fast-forward advantage selection */
	fastForward: boolean;

	/** The triggering event which initiated the roll */
	event: BasicEvent;

	/** The HTML template used to render the roll dialog */
	template: string;

	/** The dialog window title */
	title: string;

	/** Modal dialog options */
	dialogOptions: Record<never, never>;

	/** Automatically create a Chat Message for the result of this roll */
	chatMessage: boolean;

	// Only defines known configuration properties.
	/** Additional data which is applied to the created Chat Message, if any */
	messageData: {
		user: string;
		type: typeof foundry.CONST.CHAT_MESSAGE_TYPES["ROLL"];
		content: number;
		sound: typeof CONFIG.sounds.dice;
		"flags.dnd5e.roll": {
			type: "attack";
			itemId: string;
		};
	};

	/** A specific roll mode to apply as the default for the resulting roll */
	rollMode: CONFIG.Dice.RollModes;

	/** The ChatMessage speaker to pass when creating the chat */
	speaker: Record<never, never>;

	/** Flavor text to use in the posted chat message */
	flavor: string;
}>;

export declare function d20Roll<D extends object>({
	parts,
	data,
	advantage,
	disadvantage,
	fumble,
	critical,
	targetValue,
	elvenAccuracy,
	halflingLucky,
	reliableTalent,
	chooseModifier,
	fastForward,
	event,
	template,
	title,
	dialogOptions,
	chatMessage,
	messageData,
	rollMode,
	speaker,
	flavor,
}: D20RollOptions<D>): Promise<D20Roll<D> | null>;

import { waitForReady } from "./readyTools";
import type { FullPartial, Optional } from "../lib/utils";
import { Utils } from "./utils.js";
import { betterRollsInfo } from "./logging";

export type WhisperData = {
	rollMode: keyof CONFIG.Dice.RollModes;
	speaker?: Optional<ChatSpeakerDataConstructorData>;
	whisper: Optional<StoredDocument<User>[]>;
	blind: boolean;
};

type FlushOptions = FullPartial<{
	hasMaestroSound: boolean;
	whisperData: WhisperData;
}>;

/**
 * Class used to build a growing number of dice
 * that will be flushed to a system like Dice So Nice.
 */
export class DiceCollection {
	/** Roll object containing all the dice */
	public pool: Optional<Roll>;

	private _pool: Promise<Roll> | undefined;
	private _rolls: Optional<Roll>[] | undefined;
	private _isInitialized: boolean;

	/**
	 * Creates a new DiceCollection object
	 * @param initialRolls - optional additional dice to start with
	 */
	constructor(...initialRolls: Optional<Roll>[]) {
		// Constructors unfortunately can't handle asynchronous actions but we need to set up a Roll which requires asynchronous behavior.
		// While we could attempt maximal eagerness by using `.then` to get the class into a readied public state as soon as possible. However there's an race condition that means intuitive code like this won't necessarily work:
		// ```js
		// const collection = new DiceCollection(...rolls);
		// console.log(collection.pool.rolls);
		// ```
		// To avoid this one must call `.init()` to finish initialization or call any asynchronous function in this class first.
		this._pool = new Roll("0").roll({ async: true });
		this._rolls = initialRolls;
		this._isInitialized = false;
	}

	async init(): Promise<void> {
		if (this._isInitialized) {
			return;
		}

		this._isInitialized = true;

		this.pool = await this._pool;

		if (this._rolls != null) {
			await this.push(...this._rolls);
		}

		delete this._pool;
		delete this._rolls;
	}

	/**
	 * Creates a new dice pool from a set of rolls
	 * and immediately flushes it, returning a promise that is
	 * true if any rolls had dice.
	 */
	static createAndFlush(rolls: Roll[]): Promise<boolean> {
		return new DiceCollection(...rolls).flush();
	}

	/**
	 * Adds one or more rolls to the dice collection,
	 * for the purposes of 3D dice rendering.
	 */
	async push(...rolls: Optional<Roll>[]) {
		await this.init();

		for (const roll of rolls.filter((r): r is Roll => r != null)) {
			// @ts-expect-error ts(2445) expect an error because we're accessing `_dice`.
			const dice = this.pool._dice;

			dice.push(...roll.dice);
		}
	}

	/**
	 * Displays the collected dice to any subsystem that is interested.
	 * Currently its just Dice So Nice (if enabled).
	 * @returns if there were dice in the pool
	 */
	async flush({ hasMaestroSound = false, whisperData = null }: FlushOptions = {}): Promise<boolean> {
		await waitForReady();

		// Get and reset immediately (stacking flush calls shouldn't roll more dice)
		const pool = await this.pop();

		const hasDice = pool.dice.length > 0;
		if (game.dice3d != null && hasDice) {
			const wd = whisperData ?? Utils.getWhisperData();
			await game.dice3d.showForRoll(pool, game.user, true, wd.whisper, wd.blind ?? false, null, wd.speaker);
		}

		const sound = Utils.getDiceSound(hasMaestroSound);
		if (sound && hasDice) {
			// Note: emitted events aren't caught by the same client
			// the push argument didn't work for me, so using sockets instead
			await Utils.playDiceSound();
			game.socket?.emit(
				"module.betterrolls5e",
				{
					action: "roll-sound",
					user: game.user?.id,
				},
				() => betterRollsInfo("Roll Sound Message Sent")
			);
		}

		return hasDice;
	}

	/**
	 * Retrieves the dice pool and clears it without rolling it.
	 */
	async pop(): Promise<Roll> {
		await this.init();

		const { pool } = this;
		this.pool = await new Roll("0").roll({ async: true });

		return pool as Roll; // We know it's not null because we ran `this.init`
	}
}

export function migrate(): Promise<void>;
export function migrateChatMessage(message: ChatMessage): Promise<boolean>;

import { attackTypes, isSave } from "../betterrolls5e.js";
import { getSettings, type BRSettings } from "../settings.js";
import { isItemType } from "./guards.js";
import { betterRollsError } from "./logging.js";
import { getGame, assertReady } from "./readyTools";

import type { FullPartial, Optional } from "../lib/utils.js";

import type { WhisperData } from "./dice-collection";
import type * as DND5e from "../global";

export type SaveData = {
	ability: DND5e.Abilities | "";
	dc: number | null;
	context?: Optional<string>;
};

/**
 * Shorthand for both game.i18n.format() and game.i18n.localize() depending
 * on whether data is supplied or not
 * @param data - optional data that if given will do a format() instead
 */
export function i18n(...args: Parameters<Game["i18n"]["format"]>) {
	const game = getGame();

	if (args.length === 1) {
		return game.i18n.localize(args[0]);
	}

	return game.i18n.format(...args);
}

/**
 * Check if the maestro module is enabled and turned on.
 */
function maestroItemTrackEnabled(): boolean {
	const game = getGame();

	return game.modules.get("maestro") != null && game.settings.get("maestro", "enableItemTrack");
}

type CritType = "mixed" | "success" | "failure";

type GetTargetActorsParams = Partial<{
	/** True if a warning should be shown if the list is empty */
	required: boolean;
}>;

type GetRollDataOptions = FullPartial<{
	abilityMod: DND5e.Abilities;
	slotLevel: DND5e.SpellLevels;
}>;

type UtilsGetRollDataOptions = GetRollDataOptions &
	FullPartial<{
		item: Item5e;
		actor: Actor5e;
		slotServer: number;
	}>;

export type ProcessRollResult = {
	roll: Roll;
	total: number;

	/**
	 * In advantage/disadvantage context a roll will be marked ignored when it doesn't go into the final result.
	 * e.g. if you roll a 10 and a 15 with advantage, the 10 will be marked ignored because of the rules of advantage.
	 */
	ignored: Optional<boolean>;
	critType: Optional<CritType>;
	isCrit: boolean;
};

export type GetRollStateOptions = FullPartial<{
	rollState: RollState;
	event: BasicEvent;
	advantage: 0 | 1 | boolean;
	disadvantage: 0 | 1 | boolean;
	adv: 0 | 1 | boolean;
	disadv: 0 | 1 | boolean;
}>;

/** EventToAdvantageResult describes either advantage, disadvantage, or neither using numeric keys 0 for false and 1 for true. */
type EventToAdvantageResult =
	| {
			advantage: 1;
			disadvantage: 0;
	  }
	| {
			advantage: 0;
			disadvantage: 1;
	  }
	| {
			advantage: 0;
			disadvantage: 0;
	  };

type AdvantageOptions =
	| {
			advantage: true;
			disadvantage?: false;
	  }
	| {
			advantage?: false;
			disadvantage: true;
	  }
	| {
			advantage?: false;
			disadvantage?: false;
	  };

type GetTargetTokensOptions = FullPartial<{
	required: boolean;
}>;

type ParseD20FormulaResult = {
	formula: string;
	numRolls?: Optional<number>;
	rollState?: Optional<RollState>;
};

type GetCritRollOptions = FullPartial<{
	settings: typeof BRSettings;

	/** extra crit dice */
	extraCritDice: number | false;
}>;

function isItem(maybeItem: unknown): maybeItem is Item5e {
	/**
	 * Normally x instanceof Item would be enough to narrow something down to being an item. The only problem is an Item5e is secretly not really an Item at least as far as types go. Unfortunately, for instance the method `getRollData` does not inherit strictly from Item, it overrides it. This breaks polymorphism so hence `Item5e` is NOT an instance of `Item`.
	 *
	 * This is an adequate check at runtime because the concrete instance extends Item it's just doing method overriding things Typescript.
	 */
	return maybeItem instanceof Item;
}

export class Utils {
	static _playSoundLock: boolean;

	static getVersion(): string {
		const game = getGame();

		const currentModule = game.modules.get("betterrolls5e");
		if (currentModule == null) {
			betterRollsError("Could not get the version of the module!");

			return "";
		}

		return currentModule.data.version;
	}

	/**
	 * The sound to play for dice rolling. Returns null if an alternative sound
	 * from maestro or dice so nice is registered.
	 * This should be added to the chat message under sound.
	 * @param hasMaestroSound - optional parameter to denote that maestro is enabled
	 */
	static getDiceSound(hasMaestroSound: Optional<boolean> = false): Optional<string> {
		const game = getGame();

		const playRollSounds = game.settings.get("betterrolls5e", "playRollSounds");
		if (playRollSounds && !game.dice3d?.isEnabled() && !hasMaestroSound) {
			return CONFIG.sounds.dice;
		}

		return null;
	}

	static async playDiceSound(): Promise<void> {
		if (!Utils._playSoundLock) {
			Utils._playSoundLock = true;
			await AudioHelper.play({ src: CONFIG.sounds.dice });
			setTimeout(() => {
				Utils._playSoundLock = false;
			}, 300);
		}
	}

	/**
	 * Additional data to attach to the chat message.
	 */
	static getWhisperData(rollMode: keyof CONFIG.Dice.RollModes | null = null): WhisperData {
		const game = getGame();

		const finalRollMode = rollMode ?? game.settings.get("core", "rollMode");

		const blind = finalRollMode === "blindroll";

		let whisper: StoredDocument<User>[] | undefined;
		if (finalRollMode === "selfroll" && game.user !== null) {
			whisper = [game.user];
		} else if (finalRollMode === "gmroll" || finalRollMode === "blindroll") {
			whisper = ChatMessage.getWhisperRecipients("GM");
		}

		return { rollMode: finalRollMode, whisper, blind };
	}

	/**
	 * Tests a roll to see if it crit, failed, or was mixed.
	 * @param roll - the roll to test
	 * @param threshold - optional crit threshold
	 * @param critChecks - indexes of dice to test, true for all
	 * @param bonus - optional bonus roll to add to the total
	 */
	static processRoll(
		roll?: Optional<Ignorable<Roll>>,
		threshold?: Optional<number>,
		critChecks: true | number[] = true,
		bonus: Optional<Roll> = null
	): Optional<ProcessRollResult> {
		if (roll == null) {
			return null;
		}

		let high = 0;
		let low = 0;
		for (const d of roll.dice) {
			if (d.faces > 1 && (critChecks === true || critChecks.includes(d.faces))) {
				for (const result of d.results.filter((r) => !r.rerolled)) {
					if (result.result >= (threshold || d.faces)) {
						high += 1;
					} else if (result.result === 1) {
						low += 1;
					}
				}
			}
		}

		let critType: Optional<CritType>;
		if (high > 0 && low > 0) {
			critType = "mixed";
		} else if (high > 0) {
			critType = "success";
		} else if (low > 0) {
			critType = "failure";
		}

		return {
			roll,
			total: (roll.total ?? 0) + (bonus?.total ?? 0),
			ignored: roll.ignored ? true : undefined,
			critType,
			isCrit: high > 0,
		};
	}

	/**
	 * Returns an advantage and disadvantage object when given an event.
	 */
	static eventToAdvantage(ev: BasicEvent = {}): EventToAdvantageResult {
		if (ev.shiftKey) {
			return { advantage: 1, disadvantage: 0 };
		}

		if (ev.ctrlKey || ev.metaKey) {
			return { advantage: 0, disadvantage: 1 };
		}

		return { advantage: 0, disadvantage: 0 };
	}

	static getAdvantage(...args: Parameters<typeof Utils.getRollState>): AdvantageOptions {
		const rollState = Utils.getRollState(...args);

		if (rollState === "highest") {
			return { advantage: true };
		}

		if (rollState === "lowest") {
			return { disadvantage: true };
		}

		return {};
	}

	/**
	 * Determines rollstate based on several parameters
	 */
	static getRollState({
		rollState = null,
		event = null,
		advantage = null,
		disadvantage = null,
		adv = null,
		disadv = null,
	}: GetRollStateOptions = {}): RollState {
		if (rollState != null) {
			return rollState;
		}

		const hasAdvantage = advantage ?? adv ?? 0;
		if (hasAdvantage === 1 || hasAdvantage === true) {
			return "highest";
		}

		const hasDisadvantage = disadvantage ?? disadv ?? 0;
		if (hasDisadvantage === 1 || hasDisadvantage === true) {
			return "lowest";
		}

		if (event != null) {
			const modifiers = Utils.eventToAdvantage(event);
			if (modifiers.advantage || modifiers.disadvantage) {
				return Utils.getRollState(modifiers);
			}
		}

		return null;
	}

	/**
	 * Returns an item and its actor if given an item, or just the actor otherwise.
	 * @param actorOrItem - the source of actor and/or item data
	 */
	static resolveActorOrItem(actorOrItem: Optional<Item5e | Actor5e>): Partial<{ item: Item5e; actor: Actor5e }> {
		if (actorOrItem == null) {
			return {};
		}

		if (isItem(actorOrItem)) {
			return { item: actorOrItem, actor: actorOrItem?.actor ?? undefined };
		}

		return { actor: actorOrItem };
	}

	/**
	 * Returns roll data for an arbitrary item or actor.
	 * Returns the item's roll data first, and then falls back to actor
	 */
	static getRollData({ item = null, actor = null, abilityMod, slotLevel = undefined }: UtilsGetRollDataOptions) {
		return item ? ItemUtils.getRollData(item, { abilityMod, slotLevel }) : actor?.getRollData() ?? {};
	}

	/**
	 * Retrieves all tokens currently selected on the canvas. This is the normal select,
	 * not the target select.
	 */
	static getTargetTokens({ required = false }: GetTargetTokensOptions = {}): Optional<
		Array<Token5e> | [StoredDocument<Actor5e>]
	> {
		assertReady();

		const character = game.user?.character;
		const controlled = canvas?.tokens?.controlled;
		if (character != null && !controlled?.length) {
			return [character];
		}

		const results = controlled?.filter((a) => a);
		if (required != null && !controlled?.length) {
			ui.notifications.warn(game.i18n.localize("DND5E.ActionWarningNoToken"));
		}

		return results;
	}

	/**
	 * Returns all selected actors
	 */
	static getTargetActors({ required = false }: GetTargetActorsParams = {}): Actor5e[] | undefined {
		return Utils.getTargetTokens({ required })
			?.map((character) => ("actor" in character ? character.actor : null))
			.filter((a): a is Actor5e => a != null);
	}

	/**
	 * Returns all roll context labels used in roll terms.
	 * Catches things like +1d8[Thunder] active effects
	 * @param rolls - One or more rolls to extract roll flavors from
	 */
	static getRollFlavors(...rolls: Optional<Roll>[]) {
		const flavors = new Set();
		for (const roll of rolls) {
			if (roll == null) {
				continue;
			}

			for (const term of roll.terms) {
				if (term.options?.flavor != null) {
					flavors.add(term.options.flavor);
				}
			}
		}

		return Array.from(flavors);
	}

	static findD20Term(d20Roll: Optional<Roll>): Optional<Die> {
		if (d20Roll == null) {
			return null;
		}

		for (const term of d20Roll.terms) {
			if (term instanceof Die && term.faces === 20) {
				return term;
			}
		}

		return null;
	}

	static findD20Result(d20Roll: Roll) {
		return Utils.findD20Term(d20Roll)?.total;
	}

	/**
	 * Parses a d20 roll to get the "base" formula, the rollstate,
	 * and the number of times the d20 was rolled.
	 * @param finalRoll - string or Roll object to extract info from
	 * @returns
	 */
	static parseD20Formula(roll: string | Roll): ParseD20FormulaResult {
		const finalRoll = typeof roll === "string" ? new Roll(roll) : roll;

		// Determine if advantage/disadvantage, and how many rolls
		const d20Term = Utils.findD20Term(finalRoll);
		if (!d20Term) {
			return { formula: finalRoll.formula };
		}

		const numRolls = d20Term.number;
		const rollState = Utils.getRollStateFromModifiers(d20Term);

		// Remove the advantage/disadvantage from the attack roll
		// At least in the current version of foundry, the formula is not cached
		// We need to do this because rolls consolidate to a single total, and we want separate totals

		if (typeof roll !== "string") {
			// @ts-expect-error we're deleting a private value that's not supposed to be optional.
			// eslint-disable-next-line no-param-reassign
			delete roll._formula; // just in case it ever caches using this value in a future release
		}

		d20Term.number = 1;
		d20Term.modifiers = d20Term.modifiers.filter((m) => m !== "kh" && m !== "kl");

		return { formula: finalRoll.formula, numRolls, rollState };
	}

	static getRollStateFromModifiers(d20Term: Die): RollState {
		if (d20Term.modifiers.includes("kh")) {
			return "highest";
		}

		if (d20Term.modifiers.includes("kl")) {
			return "lowest";
		}

		return null;
	}
}

export class ActorUtils {
	/**
	 * True if the actor has the halfling luck special trait.
	 */
	static isHalfling(actor: Actor5e) {
		return getProperty(actor, "data.flags.dnd5e.halflingLucky");
	}

	/**
	 * True if the actor has the reliable talent special trait.
	 */
	static hasReliableTalent(actor: Actor5e) {
		return getProperty(actor, "data.flags.dnd5e.reliableTalent");
	}

	/**
	 * True if the actor has the elven accuracy feature
	 */
	static hasElvenAccuracy(actor: Actor5e) {
		return getProperty(actor, "data.flags.dnd5e.elvenAccuracy");
	}

	/**
	 * True if the actor has elven accuracy and the ability
	 * successfully procs it.
	 * @param ability - ability mod shorthand
	 */
	static testElvenAccuracy(actor: Actor5e, ability: Optional<DND5e.Abilities>) {
		return ActorUtils.hasElvenAccuracy(actor) && ability != null && ["dex", "int", "wis", "cha"].includes(ability);
	}

	/**
	 * Returns the number of additional melee extra critical dice.
	 */
	static getMeleeExtraCritDice(actor: Actor5e): number {
		return actor?.getFlag("dnd5e", "meleeCriticalDamageDice") ?? 0;
	}

	/**
	 * Returns the crit threshold of an actor. Returns null if no actor is given.
	 * @param actor - the actor who's data we want
	 * @param itemType - the item type we're dealing with
	 */
	static getCritThreshold(actor: Optional<Actor5e>, itemType: Optional<DND5e.ItemType>) {
		if (actor == null) {
			return 20;
		}

		const actorFlags = actor.data.flags.dnd5e;
		if (itemType === "weapon" && actorFlags?.weaponCriticalThreshold) {
			return toInt(actorFlags?.weaponCriticalThreshold);
		}

		if (itemType === "spell" && actorFlags?.spellCriticalThreshold) {
			return toInt(actorFlags?.spellCriticalThreshold);
		}

		return 20;
	}

	/**
	 * Returns the image to represent the actor. The result depends on BR settings.
	 */
	static getImage(actor: Optional<Actor5e>): Optional<string> {
		if (actor == null) {
			return null;
		}

		const actorImage =
			actor.data.img && actor.data.img !== CONST.DEFAULT_TOKEN && !actor.data.img.includes("*")
				? actor.data.img
				: null;

		const tokenImage = actor.token?.data?.img ? actor.token.data.img : actor.data.token.img;

		const defaultRollArt = game.settings.get("betterrolls5e", "defaultRollArt");
		switch (defaultRollArt) {
			case "actor":
				return actorImage || tokenImage;

			case "token":
				return tokenImage || actorImage;

			default:
				betterRollsError(`Invalid defaultRollArt option "${defaultRollArt}"`);
		}

		return null;
	}

	/**
	 * Returns a roll object for a skill check
	 */
	static async getSkillCheckRoll(actor: Actor5e, skill: DND5e.Skills, rollState?: RollState): Promise<Roll> {
		return await actor.rollSkill(skill, {
			fastForward: true,
			chatMessage: false,
			advantage: rollState === "highest",
			disadvantage: rollState === "lowest",
		});
	}

	/**
	 * Returns a roll object for an ability check
	 */
	static async getAbilityCheckRoll(actor: Actor5e, abl: DND5e.Abilities, rollState?: RollState): Promise<Roll> {
		return await actor.rollAbilityTest(abl, {
			fastForward: true,
			chatMessage: false,
			advantage: rollState === "highest",
			disadvantage: rollState === "lowest",
		});
	}

	/**
	 * Returns a roll object for an ability save
	 */
	static async getAbilitySaveRoll(actor: Actor5e, abl: DND5e.Abilities, rollState?: RollState): Promise<Roll> {
		return await actor.rollAbilitySave(abl, {
			fastForward: true,
			chatMessage: false,
			advantage: rollState === "highest",
			disadvantage: rollState === "lowest",
		});
	}
}

export function toInt(n: string | number): number {
	if (typeof n === "number") {
		return n;
	}

	return parseInt(n, 10);
}

export class ItemUtils {
	static getActivationData(item: Item5e) {
		const { activation } = item.data.data;
		const activationCost = activation?.cost ?? "";

		if (activation?.type && activation?.type !== "none") {
			return `${activationCost} ${CONFIG.DND5E.abilityActivationTypes[activation.type]}`.trim();
		}

		return null;
	}

	/**
	 * Creates the lower of the item crit threshold, the actor crit threshold, or 20.
	 * Returns null if no item is given.
	 */
	static getCritThreshold(item: Optional<Item5e>) {
		if (item == null) {
			return null;
		}

		// Get item crit, favoring the smaller between it and the actor's crit threshold
		const itemCrit = item.data.data.critical?.threshold || 20;
		const characterCrit = ActorUtils.getCritThreshold(item.actor, item.data.type);
		return Math.min(20, characterCrit, itemCrit);
	}

	static getDuration(item: Item5e) {
		const { duration } = item.data.data;

		if (!duration?.units) {
			return null;
		}

		return `${duration.value ?? ""} ${CONFIG.DND5E.timePeriods[duration.units]}`.trim();
	}

	static getRange(item: Item5e) {
		const { range } = item.data.data;

		if (!range?.value && !range?.units) {
			return null;
		}

		const standardRange = range.value || "";
		const longRange = range.long && range.long !== range.value ? `/${range.long}` : "";
		const rangeUnit = range.units ? CONFIG.DND5E.distanceUnits[range.units] : "";

		return `${standardRange}${longRange} ${rangeUnit}`.trim();
	}

	static getSpellComponents(item: Item5e) {
		const { vocal, somatic, material } = item.data.data.components ?? {};

		let componentString = "";

		if (vocal) {
			componentString += i18n("br5e.chat.abrVocal");
		}

		if (somatic) {
			componentString += i18n("br5e.chat.abrSomatic");
		}

		if (material) {
			const { materials } = item.data.data;
			componentString += i18n("br5e.chat.abrMaterial");

			if (materials?.value) {
				const materialConsumption = materials?.consumed ? i18n("br5e.chat.consumedBySpell") : "";
				componentString += ` (${materials.value} ${materialConsumption})`;
			}
		}

		return componentString || null;
	}

	static getTarget(item: Item5e) {
		const { target } = item.data.data;

		if (!target?.type) {
			return null;
		}

		const targetDistance =
			target.units && target?.units !== "none"
				? ` (${target.value} ${CONFIG.DND5E.distanceUnits[target.units]})`
				: "";

		return i18n("Target: ") + CONFIG.DND5E.targetTypes[target.type] + targetDistance;
	}

	/**
	 * Ensures that better rolls flag data is set on the item if applicable.
	 * Does not perform an item update, only assigns to data
	 * @param item - item to update flags for
	 */
	static ensureFlags(item: Optional<Item5e>) {
		if (item?.data == null) {
			return;
		}

		const flags = this.createFlags(item?.data);
		if (flags == null) {
			return;
		}

		// eslint-disable-next-line no-param-reassign
		item.data.flags = {
			...(item.data.flags ?? {}),
			betterRolls5e: flags,
		};
	}

	/**
	 * Creates the flags that should be assigned to the the item. Does not save to database.
	 * @param itemData - The item.data property to be updated
	 */
	static createFlags(itemData: Optional<Item5e["data"]>): BetterRolls.AllFlags | null {
		if (!itemData || CONFIG.betterRolls5e.validItemTypes.indexOf(itemData.type) === -1) {
			return null;
		}

		const baseFlags: BetterRolls.AllFlags = duplicate(CONFIG.betterRolls5e.allFlags[`${itemData.type}Flags`]);

		let flags = duplicate(itemData.flags.betterRolls5e ?? {}) as BetterRolls.AllFlags;
		flags = mergeObject(baseFlags, flags ?? {}) as BetterRolls.AllFlags;

		// If quickDamage flags should exist, update them based on which damage formulae are available
		if (flags?.quickDamage != null) {
			const newQuickDamageValues: boolean[] = [];
			const newQuickDamageAltValues: boolean[] = [];

			// Make quickDamage flags if they don't exist
			flags.quickDamage ??= { type: "Array", value: [], altValue: [], context: [] };

			const partsLength = itemData.data.damage?.parts.length ?? 0;
			for (let i = 0; i < partsLength; i++) {
				newQuickDamageValues[i] = flags.quickDamage.value[i] ?? true;
				newQuickDamageAltValues[i] = flags.quickDamage.altValue[i] ?? true;
			}

			flags.quickDamage.value = newQuickDamageValues;
			flags.quickDamage.altValue = newQuickDamageAltValues;
		}

		return flags;
	}

	static async placeTemplate(item: Item5e): Promise<void> {
		if (item?.hasAreaTarget) {
			const template = game.dnd5e.canvas.AbilityTemplate.fromItem(item);
			template?.drawPreview();

			if (item.sheet?.rendered) {
				await item.sheet.minimize();
			}
		}
	}

	/**
	 * Finds if an item has a Maestro sound on it, in order to determine whether or not the dice sound should be played.
	 */
	static hasMaestroSound(item: Optional<Item5e>): boolean {
		if (item == null) {
			return false;
		}

		return maestroItemTrackEnabled() && !!item.data.flags.maestro?.track;
	}

	/**
	 * Checks if the item applies savage attacks (bonus crit).
	 * Returns false if the actor doesn't have savage attacks, if the item
	 * is not a weapon, or if there is no item.
	 */
	static getExtraCritDice(item: Optional<Item5e>): number | false {
		if (item?.actor && item?.data.type === "weapon") {
			return ActorUtils.getMeleeExtraCritDice(item.actor);
		}

		return false;
	}

	/**
	 * Gets the item's roll data.
	 * This uses item.getRollData(), but allows overriding with additional properties
	 */
	static getRollData(
		item: Item5e,
		{ abilityMod, slotLevel = undefined }: GetRollDataOptions = {}
	): ReturnType<Item5e["getRollData"]> {
		const rollData = item.getRollData();

		if (rollData != null) {
			const abl = abilityMod ?? item?.abilityMod;
			rollData.mod = abl != null ? rollData.abilities[abl]?.mod : 0;

			if (slotLevel) {
				rollData.item.level = slotLevel;
			}
		}

		return rollData;
	}

	/**
	 * Returns a roll object that is used to roll the item attack roll.
	 */
	static async getAttackRoll(item: Item5e, rollState?: RollState): Promise<Roll | null> {
		return await item.rollAttack({
			fastForward: true,
			chatMessage: false,
			advantage: rollState === "highest",
			disadvantage: rollState === "lowest",
		});
	}

	/**
	 * Gets the tool roll (skill check) for a specific item.
	 */
	static async getToolRoll(item: Item5e, rollState: RollState) {
		return await item.rollToolCheck({
			fastForward: true,
			chatMessage: false,
			advantage: rollState === "highest",
			disadvantage: rollState === "lowest",
		});
	}

	/**
	 * Returns the base crit formula, before applying settings to it.
	 * Only useful really to test if a crit is even possible
	 * @returns the base crit formula, or null if there is no dice
	 */
	static getBaseCritRoll(baseFormula: Optional<Formula>): Optional<Roll> {
		if (baseFormula == null || baseFormula === "") {
			return null;
		}

		// Remove all flavor from the formula so we can use the regex
		// In the future, go through the terms to determine the bonus crit damage instead
		const strippedRoll = getRoll(baseFormula);
		for (const term of strippedRoll.terms) {
			if (term.options) {
				term.options = {};
			}
		}

		const critRegex = /[+-]+\s*(?:@[a-zA-Z0-9.]+|[0-9]+(?![Dd]))/g;
		const critFormula = strippedRoll.formula.replace(critRegex, "").concat();
		const critRoll = new Roll(critFormula);
		if (critRoll.terms.length === 1 && typeof critRoll.terms[0] === "number") {
			return null;
		}

		return critRoll;
	}

	/**
	 * Derives the formula for what should be rolled when a crit occurs.
	 * Note: Item is not necessary to calculate it.
	 * @returns the crit result, or null if there is no dice
	 */
	static async getCritRoll(
		baseFormula: string,
		baseTotal: number,
		{ settings = null, extraCritDice = null }: GetCritRollOptions = {}
	): Promise<Optional<Evaluated<Roll>>> {
		const critRoll = ItemUtils.getBaseCritRoll(baseFormula);
		if (critRoll == null) {
			return null;
		}

		critRoll.alter(1, extraCritDice || 0);
		await critRoll.roll({ async: true });

		const { critBehavior } = getSettings(settings);

		let evaluatedCritRoll = critRoll as Evaluated<Roll>;

		const critConfig = CONFIG.betterRolls5e.critBehavior;

		// If critBehavior = 2, maximize base dice
		if (critBehavior === critConfig.rollBaseMaxCritical) {
			evaluatedCritRoll = (await new Roll(critRoll.formula).evaluate({
				maximize: true,
				async: true,
			})) as Evaluated<Roll>;
		}

		// If critBehavior = 3, maximize base and maximize crit dice
		// Need to get the difference because we're not able to change the base roll from here so we add it to the critical roll
		else if (critBehavior === critConfig.maxBaseMaxCritical) {
			const rollTotal = (await new Roll(baseFormula).evaluate({ maximize: true, async: true }))?.total ?? 0;
			const maxDifference = rollTotal - baseTotal;
			const newFormula = `${critRoll.formula}+${maxDifference}`;
			evaluatedCritRoll = (await new Roll(newFormula).evaluate({
				maximize: true,
				async: true,
			})) as Evaluated<Roll>;
		}

		// If critBehavior = 4, maximize base dice and roll crit dice
		// Need to get the difference because we're not able to change the base roll from here so we add it to the critical roll
		else if (critBehavior === critConfig.maxBaseRollCritical) {
			const maxRoll = (await new Roll(baseFormula).evaluate({ maximize: true, async: true })) as Evaluated<Roll>;
			const maxDifference = maxRoll.total ?? 0 - baseTotal;
			const newFormula = `${critRoll.formula}+${maxDifference}`;
			evaluatedCritRoll = (await new Roll(newFormula).evaluate({ async: true })) as Evaluated<Roll>;
		}

		return evaluatedCritRoll;
	}

	/**
	 * Returns the scaled damage formula of the spell
	 * @param damageIndex - The index to scale, or versatile
	 * @returns the formula if scaled, or null if its not a spell
	 */
	static async scaleDamage(
		item: Item5e,
		spellLevel: Optional<number>,
		damageIndex: number | "versatile",
		rollData?: ReturnType<Item5e["getRollData"] | Actor5e["getRollData"]>
	): Promise<Optional<string>> {
		if (isItemType(item.data, "spell")) {
			const itemData = item.data.data;
			const actorData = item.actor?.data.data;

			const scale = itemData.scaling.formula;

			const formula =
				damageIndex === "versatile" ? itemData.damage?.versatile : itemData.damage?.parts[damageIndex][0];

			const parts = formula != null ? [formula] : [];

			// Scale damage from up-casting spells
			if (itemData.scaling?.mode === "cantrip") {
				const level =
					item.actor?.data.type === "character"
						? actorData?.details.level
						: actorData?.details.spellLevel || actorData?.details.cr;

				// TODO handle unset level better
				await item._scaleCantripDamage(parts, scale, level ?? 0, rollData ?? {});
			} else if (spellLevel && itemData.scaling?.mode === "level" && itemData.scaling?.formula != null) {
				await item._scaleSpellDamage(parts, itemData.level, spellLevel, scale, rollData ?? {});
			}

			return parts[0];
		}

		return null;
	}

	/**
	 * A function for returning the properties of an item, which can then be printed as the footer of a chat card.
	 */
	static getPropertyList(item: Optional<Item5e>): string[] {
		if (item == null) {
			return [];
		}

		let properties: Array<string | null> = [];

		const range = ItemUtils.getRange(item);
		const target = ItemUtils.getTarget(item);
		const activation = ItemUtils.getActivationData(item);
		const duration = ItemUtils.getDuration(item);

		switch (item.data.type) {
			case "weapon": {
				const { data } = item.data;

				properties = [
					CONFIG.DND5E.weaponTypes[data.weaponType],
					range,
					target,
					data.proficient ? "" : i18n("Not Proficient"),
					data.weight ? `${data.weight} ${i18n("lbs.")}` : null,
				];

				break;
			}

			case "spell": {
				const { data } = item.data;

				// Spell attack labels
				data.damageLabel = data.actionType === "heal" ? i18n("br5e.chat.healing") : i18n("br5e.chat.damage");
				data.isAttack = attackTypes.includes(data.actionType);

				properties = [
					CONFIG.DND5E.spellLevels[data.castedLevel ?? data.level],
					CONFIG.DND5E.spellSchools[data.school],
					data.components?.ritual ? i18n("Ritual") : null,
					activation,
					duration,
					data.components?.concentration ? i18n("Concentration") : null,
					ItemUtils.getSpellComponents(item),
					range,
					target,
				];

				break;
			}

			case "feat": {
				const { data } = item.data;

				properties = [data.requirements, activation, duration, range, target];

				break;
			}

			case "consumable": {
				const { data } = item.data;

				properties = [
					data.weight ? `${data.weight} ${i18n("lbs.")}` : null,
					activation,
					duration,
					range,
					target,
				];

				break;
			}

			case "equipment": {
				const { data } = item.data;

				properties = [
					CONFIG.DND5E.equipmentTypes[data.armor.type],
					data.equipped ? i18n("Equipped") : null,
					data.armor.value ? `${data.armor.value} ${i18n("AC")}` : null,
					data.stealth ? i18n("Stealth Disadv.") : null,
					data.weight ? `${data.weight} ${i18n("lbs.")}` : null,
				];

				break;
			}

			case "tool": {
				const { data } = item.data;

				properties = [
					CONFIG.DND5E.proficiencyLevels[data.proficient],
					data.ability ? CONFIG.DND5E.abilities[data.ability] : null,
					data.weight ? `${data.weight} ${i18n("lbs.")}` : null,
				];

				break;
			}

			case "loot": {
				const { data } = item.data;

				properties = [data.weight ? `${data.totalWeight} ${i18n("lbs.")}` : null];

				break;
			}

			default:
		}

		const output = properties.filter((p): p is string => p != null && p.length !== 0 && p !== " ");
		return output;
	}

	/**
	 * Returns an object with the save DC of the item.
	 * If no save is written in, one is calculated.
	 */
	static getSave(item: ItemOfType<"spell">): SaveData;
	static getSave(item: Item5e): Optional<SaveData>;

	static getSave(item: Optional<Item5e>): Optional<SaveData> {
		if (!item || !isSave(item)) {
			return null;
		}

		return {
			ability: item.data.data.save.ability,
			dc: item.getSaveDC(),
		};
	}
}

export function getRoll<D extends object>(formula: Formula, rollData?: D): Roll<D> {
	let finalFormula: string;
	if (typeof formula === "string") {
		finalFormula = formula;
	} else {
		finalFormula = formula.formula;
	}

	return new Roll(finalFormula, rollData);
}

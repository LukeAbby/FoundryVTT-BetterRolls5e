import type {
	ActorRollAbilitySaveOutput,
	ActorRollAbilityTest,
	ActorRollAbilityTestOutput,
	ActorRollSkill,
	ActorRollSkillOutput,
} from "../../patching";
import type { ExtendedOptions, SourceFunctions } from "../../patching/baseTypes";
import type { InvariantOver } from "../utils";
import type { DND5e } from "./dnd5e";
import type { D20RollOptions } from "./dice5e";

declare namespace Actor5eTemplates {
	export type AllTemplates = {
		common: object;
		creature: object;
	};

	export type BaseTemplates = {
		common: BaseCommon;
		creature: BaseCreature;
	};

	export type DerivedTemplates = {
		common: DerivedCommon;
		creature: DerivedCreature;
	};

	type BaseCommonAbilities = {
		value: number;
		proficient: number;
		bonuses: {
			check: string;
			save: string;
		};
	};

	type BaseCommon = {
		abilities: Record<DND5e.Abilities, BaseCommonAbilities>;

		attributes: {
			ac: {
				flat: number | null;
				calc: "flat" | "natural" | "default" | "custom";
				formula: Formula;
			};
			hp: {
				value: number;
				min: number;
				max: number;
				temp: number;
				tempmax: number;
			};
			init: {
				value: number;
				bonus: number;
			};
			movement: {
				burrow: number;
				climb: number;
				fly: number;
				swim: number;
				walk: number;
				units: string;
				hover: boolean;
			};
		};

		details: {
			biography: {
				value: string;
				public: string;
			};
		};
	};

	type DerivedCommon = BaseCommon & {
		abilities: Record<
			DND5e.Abilities,
			BaseCommonAbilities & {
				mod: number;
			}
		>;
	};

	type BaseCreature = {
		details: {
			alignment: string;
			race: string;
		};

		bonuses: InvariantOver<
			{
				mwak: {
					attack: string;
					damage: string;
				};
				rwak: {
					attack: string;
					damage: string;
				};
				msak: {
					attack: string;
					damage: string;
				};
				rsak: {
					attack: string;
					damage: string;
				};
				abilities: {
					check: string;
					save: string;
					skill: string;
				};
				spell: {
					dc: string;
				};
			},
			DND5e.ItemActionTypes
		>;

		skills: Record<
			DND5e.Skills,
			{
				value: number;
				ability: DND5e.Abilities;
				bonuses: {
					check: string;
					passive: string;
				};
			}
		>;
	};

	type DerivedCreature = BaseCreature;
}

type Actor5eTemplateData<Templates extends Actor5eTemplates.AllTemplates> = {
	character: Templates["common"] &
		Templates["creature"] & {
			details: {
				background: string;
				originalClass: string;

				xp: {
					value: number;
					min: number;
					max: number;
				};

				appearance: string;
				trait: string;
				ideal: string;
				bond: string;
				flaw: string;
			};
		};
	npc: Templates["common"] &
		Templates["creature"] & {
			details: {
				type: {
					value: string;
					subtype: string;
					swarm: string;
					custom: string;
				};
				environment: string;
				cr: number;
				spellLevel: number;
				xp: {
					value: number;
				};
				source: string;
			};
		};
	vehicle: Templates["common"];
};

type Actor5ePreparedData = {
	character: {
		details: {
			level: number;
		};

		attributes: {
			hd: number;
			prof: number;
		};
	};
	npc: object;
	vehicle: object;
};

type Actor5eBase<Type extends Actor5eTypes = Actor5eTypes> = {
	type: Type;
};

export type Actor5eDataSourceData<Type extends Actor5eTypes = Actor5eTypes> = Type extends unknown
	? Actor5eBase<Type> & {
			data: Actor5eTemplateData<Actor5eTemplates.BaseTemplates>[Type];
	  }
	: never;

export type Actor5eData<Type extends Actor5eTypes = Actor5eTypes> = Type extends unknown
	? Actor5eBase<Type> & {
			data: Actor5eTemplateData<Actor5eTemplates.DerivedTemplates>[Type] & Actor5ePreparedData[Type];
	  }
	: never;

export type Actor5eTypes = "character" | "npc" | "vehicle";

type Actor5eRollData = Actor5e["data"]["data"] & {
	prof: Proficiency;
	classes: Record<string, ItemOfType<"class">["data"]["data"]>;
};

// The type duplication for skills etc. is unfortunate but it's the only way to keep function overrides and generic parameters without HKT.
type SkillId = Parameters<ActorRollSkill>[0];
type Options<D extends object> = Partial<ExtendedOptions<D20RollOptions<D>>>;

type Ability = Parameters<ActorRollAbilityTest>[0];

export declare class Actor5e extends Actor {
	getRollData(): Actor5eRollData;

	applyDamage(amount: number, multiplier: number): Promise<Actor5e>;

	rollSkill<D extends Actor5eRollData>(
		skillId: SkillId,
		options: (Options<D> & { vanilla: true }) | (Options<D> & { chatMessage: false })
	): Promise<ReturnType<SourceFunctions.DND5e.Actor.RollSkill<D>>>;
	rollSkill<D extends Actor5eRollData>(skillId: DND5e.Skills, options: Options<D>): ActorRollSkillOutput;

	rollAbilityTest<D extends Actor5eRollData>(
		ability: Ability,
		options: (Options<D> & { vanilla: true }) | (Options<D> & { chatMessage: false })
	): Promise<ReturnType<SourceFunctions.DND5e.Actor.RollAbilityTest<D>>>;
	rollAbilityTest<D extends Actor5eRollData>(ability: Ability, options: Options<D>): ActorRollAbilityTestOutput;

	rollAbilitySave<D extends Actor5eRollData>(
		ability: Ability,
		options: (Options<D> & { vanilla: true }) | (Options<D> & { chatMessage: false })
	): Promise<ReturnType<SourceFunctions.DND5e.Actor.RollAbilitySave<D>>>;
	rollAbilitySave<D extends Actor5eRollData>(ability: Ability, options: Options<D>): ActorRollAbilitySaveOutput;
}

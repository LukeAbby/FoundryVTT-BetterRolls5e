// @ts-check

const chalk = require("chalk");
const browserSync = require("browser-sync").create();
const fs = require("fs");
const { spawn } = require("child_process");

const prettyHrtime = require("pretty-hrtime");

const esbuild = require("esbuild");

const { sassPlugin } = require("esbuild-sass-plugin");
const { foundryPlugin } = require("esbuild-foundry-plugin");

const name = "betterrolls5e";
const sourceDirectory = `./${name}`;
const distDirectory = "./dist";

const isWatch = process.env.WATCH === "true";
const buildOnly = process.env.BUILD_ONLY === "true";

async function build() {
	/** @type {import("esbuild").BuildOptions["watch"]} */
	let watch = false;
	if (isWatch) {
		await serve();

		watch = {
			onRebuild(error, _result) {
				if (error) {
					console.error("Watch build failed:", error);
					return;
				}

				browserSync.reload();
			},
		};
	}

	/** @type {esbuild.Plugin[]} */
	const plugins = [
		sassPlugin(),
		foundryPlugin({
			packageName: name,
			packageType: "module",
			ci: true,
		}),
	];

	if (!buildOnly) {
		plugins.unshift(typecheckPlugin());
	}

	return await esbuild.build({
		entryPoints: [`${sourceDirectory}/module/index.ts`, `${sourceDirectory}/styles/${name}.scss`],
		outdir: distDirectory,
		outbase: sourceDirectory,
		bundle: true,
		sourcemap: true,
		minify: true,
		incremental: isWatch,
		define: {
			BUILD_REPLACES_VERBOSE: (process.env.BETTER_ROLLS_5E_VERBOSE === "true").toString(),
		},
		watch,
		plugins,
		assetNames: "[dir]/[name]",
		loader: {
			".svg": "file",
		},
	});
}

async function serve() {
	return new Promise((resolve) =>
		browserSync.init(
			{
				server: false,
				proxy: {
					target: "localhost:30000",
					ws: true,
				},
				open: false,
				ui: false,
			},
			() => resolve()
		)
	);
}

/** @type {() => import("esbuild").Plugin} */
const typecheckPlugin = () => ({
	name: "typecheck-plugin",
	setup(build) {
		build.onStart(onStart);
		build.onEnd(onEnd);
	},
});

let buildStart;
let typecheck;

/**
 * @returns {Promise<void>}
 */
async function onStart() {
	stopTypecheck();

	buildStart = process.hrtime();
	console.log(`${chalk.yellow("[ESBuild]")} Starting build`);

	await cleanup();

	fs.mkdirSync(distDirectory);
}

/**
 * @param {import("esbuild").BuildResult} result
 */
async function onEnd(result) {
	stopTypecheck();

	const buildDuration = process.hrtime(buildStart);
	const durationStr = chalk.magenta(prettyHrtime(buildDuration));

	const succeeded = result.errors.length === 0;

	let tag;
	if (succeeded) {
		tag = `${chalk.green("[ESBuild]")} Finished build`;
	} else {
		tag = `${chalk.red("[ESBuild]")} Build failed`;
	}

	console.log(`${tag} after ${durationStr}.\n`);

	startTypecheck();
}

async function cleanup() {
	return new Promise((resolve, reject) => {
		fs.rm(distDirectory, { recursive: true }, (err) => {
			// Ignores when the directory doesn't exist (ENOENT)
			if (err == null || err.code === "ENOENT") {
				resolve();
			} else {
				reject(err);
			}
		});
	});
}

function stopTypecheck() {
	if (typeof typecheck !== "undefined") {
		typecheck.removeAllListeners("exit");
		typecheck.kill("SIGTERM");
		typecheck = undefined;

		const duration = process.hrtime(timecheckStart);
		const durationStr = chalk.magenta(prettyHrtime(duration));

		console.log(`${chalk.yellow("[Typecheck]")} Cancelled by another build after ${durationStr}.\n`);
	}
}

function startTypecheck() {
	process.on("exit", () => {
		if (typeof typecheck !== "undefined") {
			typecheck.kill("SIGTERM");
		}
	});

	process.on("SIGTERM", () => {
		process.exit();
	});

	if (typeof typecheck !== "undefined") {
		typecheck.kill("SIGTERM");
	}

	typecheck = spawn("yarn", ["run", "typecheck"], { stdio: "inherit" });

	timecheckStart = process.hrtime();
	typecheck.on("exit", (code) => typecheckExit(code));

	console.log(`${chalk.yellow("[Typecheck]")} Starting type checking`);
}

let timecheckStart;

/**
 * @param {number} code
 */
function typecheckExit(code) {
	typecheck = undefined;

	// Time how long it took.
	const duration = process.hrtime(timecheckStart);
	const durationStr = chalk.magenta(prettyHrtime(duration));

	if (code === 0) {
		console.log(`${chalk.green("[Typecheck]")} Finished after ${durationStr}.`);
	} else {
		console.error(`${chalk.red("[Typecheck]")} Failed after ${durationStr}.`);
	}
}

build();

type Nullish = null | undefined;

/**
 * Optional exists for the majority case where a singular value is just meant to be optional without distinguishing between `null` or `undefined`
 * @see {@link FullPartial} for making every property of an object optional and partial.
 */
export type Optional<T> = T | Nullish;

export type OptionalChaining<T, K extends AllKeys<Exclude<T, Nullish>>> =
	| (T extends unknown ? (K extends keyof T ? T[K] : never) : never) // Get K for every member it exists on.
	| (K extends keyof Exclude<T, Nullish> ? never : undefined) // Case: one or more values do not contain `K` and chain to undefined.
	| (LooseEquals<Extract<T, Nullish>, never> extends true ? never : undefined); // Case: T itself is nullish and that chains to undefined.

/**
 * FullPartial exists as an extension to {@link Partial} that makes sure each value allows `null`. Partial will transform a type of the format `Record<K, V>`, making every key optional and allows values to include `undefined` but not `null`.
 * @see {@link Optional} for the equivalent for single values.
 */
export type FullPartial<T> = {
	[K in keyof T]?: Optional<T[K]>;
};

export type FullPartialDeep<T> = {
	[K in keyof T]?: Optional<T[K] extends object ? FullPartialDeep<T[K]> : T[K]>;
};

export type Initialized<T> = {
	[K in keyof T]-?: Exclude<T[K], null | undefined>;
};

/**
 * `AllKeys` is designed to get every key of every element in a union. Use `keyof T` if T is not a union or you only want keys every element in the union has.
 *
 * @example
 * ```ts
 * type TestAllKeys1 = AssertTrue<Equals<AllKeys<{ foo: 123 }>, "foo">>>; // use keyof
 * type TestAllKeys2 = AssertTrue<Equals<AllKeys<number, keyof number>>>; // use keyof
 * type TestAllKeys3 = AssertTrue<Equals<AllKeys<{ foo: 123 } | { bar: 456 }, "foo" | "bar">>>;
 * type TestAllKeys4 = AssertTrue<Equals<AllKeys<{ foo: 123, bar: 456 } | { bar: 123 } | { lorem: "ipsum" }, "foo" | "bar" | "lorem"}>>>;
 * type TestAllKeys5 = AssertTrue<Equals<AllKeys<Record<string, unknown>, string>>>;
 * type TestAllKeys6 = AssertTrue<Equals<AllKeys<any, string | number | symbol>>>;
 * ```
 */
export type AllKeys<T> = T extends unknown ? keyof T : never;

/**
 * This type is used to make it easier access properties on a union where each member is assumed to be invariant. This assumption is only made explicit with this transformation and is unsound if that is not true.
 *
 * @remarks
 *
 * Property access, even in the especially ergonomic and idiomatic form `x?.prop`, will not work unless a type defines the property being accessed. Normally this makes sense, a type is like a contract and trying to access properties outside of those defined is counterproductive and should just be added to the type.
 *
 * Unfortunately Typescript must either assert that a property must exist on every member of a union or compromise in specificity of type. A reasonable question to have at this point is _why_ that must be true and the answer boils down to the fact that types in a union are not invariant. Unfortunately that jargon is likely to only answer the question for those who already have knowledge that this aims to provide.
 *
 * Replicating the problem this type solves can be somewhat difficult due to Typescript's inference engine that will helpfully stop you from shooting yourself in the foot or provide you with types that solve the issue before the problem can crop up. Unfortunately we'd like to demonstrate exactly the edge cases where it can still appear.
 *
 * Here's a minimally contrived code example that demonstrates the error that will occur if you try to access properties on an incorrectly arranged union:
 * ```ts
 * type FooOrBar = { foo: number } | { bar: number };
 *
 * const fooOrBar = getFooOrBar();
 * function getFooOrBar(): FooOrBar {
 *    return Math.random() ? { foo: 123 } : { bar: 456 };
 * }
 *
 * fooOrBar?.foo; // One could reasonably expect this to be `foo | undefined` but instead it's an error:
 *                // Property 'foo' does not exist on type 'FooOrBar'.
 *                //   Property 'foo' does not exist on type '{ bar: number; }'.(2339)
 * ```
 *
 * That doesn't make the problem immediately apparent but if you were to change the body like so you might begin to question with a different implementation as such:
 * ```js
 * function getFooOrBar(): FooOrBar {
 *   const x = { foo: "hello", bar: 456 };
 *
 *   return x;
 * }
 * ```
 * While this code is very much an anti-pattern when condensed in this way, it's valid and will have to stay valid. In short this is because `{ foo: "hello", bar: 456 }` can be assigned to the type `{ bar: number }`. The reasons for this is general ease of use and making this intuitive is largely a matter outside this explanation. However it's useful to realise if this was _not_ possible you could also not use, for instance, a `Cat` where you expect any `Animal`.
 *
 * The problem then becomes that any member of the union _without_ the property explicitly defined could still have the property, just with no useful type information. An implementation could be made that returned the type `any` but this is not nearly as narrow as we want. Especially considering the confusing nature of such an implementation it is probably for the best that it is not implemented at all unless first-class language features to specifically choose an exact (invariant) type are implemented.
 *
 * For now this type attempts to compromise between complete correctness and usability by adding every key to every member of the union, set as `undefined` when not already defined explicitly.
 */
export type InvariantUnion<T extends Record<never, never>> = InvariantUnionHelper<T>;
type InvariantUnionHelper<T extends Record<never, never>, AllKey extends AllKeys<T> = AllKeys<T>> = T extends unknown
	? InvariantKeys<T, AllKey>
	: never;

export type InvariantKeys<T extends Record<never, never>, K extends string | number | symbol> = T &
	UndefinedKeys<Exclude<K, keyof T>>;

type UndefinedKeys<K extends string | number | symbol> = {
	[_ in K]?: undefined;
};

type AnyRecord = Record<string | number | symbol, unknown>;

// T is a union, we're passing it twice to preserve the overall union information. That's because doing `T extends unknown ? ... : never` narrows `T` to an individual member of the union in the block signified by `...` but we need to access data about other members.
export type InvariantUnionDeep<T extends AnyRecord | Nullish> = InvariantUnionDeepHelper<T, T>;

export type InvariantUnionDeepHelper<
	AllT extends AnyRecord | Nullish,
	T extends AnyRecord | Nullish,
	AllKey extends AllKeys<AllT> = AllKeys<AllT>
> = T extends unknown
	? {
			[K in keyof T]: InvariantProp<AllT, T, K>;
	  } & UndefinedKeys<Exclude<AllKey, keyof T>>
	: T;

type InvariantProp<
	AllT extends AnyRecord | Nullish,
	T extends AnyRecord | Nullish,
	K extends keyof T,
	Siblings = AllT extends unknown ? (K extends keyof AllT ? AllT[K] : never) : never,
	RecordSiblings extends AnyRecord = Siblings extends unknown ? Extract<Siblings, AnyRecord> : never
> = T[K] extends AnyRecord | Nullish ? InvariantUnionDeepHelper<RecordSiblings, T[K]> : T[K];

export type InvariantOver<T extends AnyRecord, K extends string | number | symbol> = T extends unknown
	? T & UndefinedKeys<Exclude<K, keyof T>>
	: never;

/**
 * This type converts a union of the form `A | B` into an intersection `A & B`.
 *
 * @example
 * ```ts
 * type Foo = { foo: 123 };
 * type Bar = { bar: 456 };
 * type TestUnionToIntersection = AssertTrue<Equals<UnionToIntersection<Foo | Bar>, Foo & Bar>>
 * ```
 *
 * @remarks
 *
 * The goal of this type is likely to make some sense to anyone with knowledge of algebraic type systems. However the mechanisms for how it works are relatively opaque to those without deep knowledge of how Typescript works as well as knowledge of how variance and conditional type inference works.
 *
 * For an audience that understands all that, the short explanation is as follows: We first distribute `T` into a union where each of the elements are put into a contravariant position and then infer `U` back out resulting in an intersection.
 *
 * For someone familiar enough with Typescript to understand the goal of the type but not how it works be that this type is a no-op. That is because the step by step process looks like this:
 * 1. We distribute the union T such that each element ends up in a separate but identical container.
 * 2. We infer the type U from where we inserted T.
 * Those two steps would ordinarily simply reverse each other and be essentially equivalent to an identity function, for example this type will be a no-op:
 * ```ts
 * type PackageT<T> = T extends unknown ? Array<T> : never;
 * type UnionToIntersection<T> = PackageT<T> extends Array<infer U> ? U : never;
 * ```
 *
 * This packaging and unpackaging reversing each other is true for every possible way you could package T _except_ when it's a function parameter. Here's an example of why:
 * ```ts
 * type Foo = { foo: number };
 * type Bar = { bar: string };
 *
 * const fooFunc = ({ foo }: Foo) => foo + 1;
 * const barFunc = ({ bar }: Bar) => `bar: ${bar}`;
 *
 * const fooOrBarFunc = Math.random() ? fooFunc : barFunc;
 * ```
 *
 * If this function `fooOrBarFunc` followed normal rules, the type for its argument would be `Foo | Bar`. Unfortunately that would be unsound because `barFunc` can't take `Foo` and `fooFunc` can't take `Bar`. Since we can't (statically) know which function `fooOrBarFunc` is assigned to it might just look like we made an uncallable function and if `Foo` and `Bar` were different it very well could be.
 *
 * But what about this input?
 * ```ts
 * fooOrBarFunc({
 *     foo: 123,
 *     bar: "someString"
 * });
 * ```
 *
 * This works completely fine. The contract that `fooFunc` and `barFunc` expresses that they don't care if there's extra properties on the type.
 *
 * As might be obvious by the context of this explanation and by the code snippet expressed, the signature of `fooOrBarFunc` is `typeof fooFunc | typeof barFunc` which, as shown above, is equivalent to `(arg: Foo & Bar) => O`. `O`'s type does not matter here but it would be `ReturnType<typeof fooFunc> | ReturnType<typeof barFunc>` or more simply `string | number`. In the type we show we do not care about its type by using `never`.
 *
 * That's why when we package the members of a union `T` in this way and then infer it right back, we can get an intersection of its members.
 */
export type UnionToIntersection<T> = PackageContravariant<T> extends (_: infer U) => never ? U : never;

/**
 * @privateRemarks
 * This essentially distributes the members of T into a contravariant package. Because function parameters are the only source of contravariance in the language, the type will be like so: `((_: T1) => never) | ((_: T2) => never) | ...`. This is NOT the same as `(_: T) => never` because that type will result in something like `(_: T1 | T2 | ...) => never`.
 *
 * @example
 * ```
 * type TestPackageContravariant = AssertTrue<Equals<
 *     PackageContravariant<1 | 2 | 3>,
 *     ((_: 1) => never) | ((_: 2) => never) | ((_: 3) => never)
 * >>;
 * ```
 */
type PackageContravariant<T> = T extends unknown ? (_: T) => never : never;

/**
 * Equals returns true if both T and U are strictly equal. For single types this means they must be exactly the same, not a subset or superset of each other. More generally with unions they must be have the same cardinality over its distinct elements (i.e. number of unique elements) and contain the same elements.
 *
 * There are two edge cases that technically fit into the above explanation but it would be a disservice not to mention:
 * 1. The `never` type represents a cases that at the type level is meant to be guaranteed to _`never`_ happen, e.g. a function with an infinite loop in it is represented better better with `never` than `void`. Because of this property in a union it is an insignificant value, `T | never` is exactly equivalent to `T`.
 * 2. Using the type `any` is normally intentionally unsound in that any value can be assigned to it and it can be assigned to any value (except `never`) hence the name.
 *    This would normally imply equality by the fact that it satisfies the property `(T ⊆ U && U ⊆ T)` where `x ⊆ y` means assigns `x` can be assigned to `y`. The informal proof for this is essentially the same as for the fact that `x <= y && y <= x` must imply `x === y`.
 *    Because `any` making the result `true` in almost every circumstance is usually undesirable `Equal<any, U>` will always return false unless `type U = any;`
 *
 * @example
 * ```
 * type TestEquals1 = AssertFalse<Equals<true, false>>;
 * test TestEquals2 = AssertFalse<Equals<"foo", string>>;
 * type TestEquals3 = AssertFalse<Equals<number, number | string>>;
 *
 * type TestEquals4 = AssertTrue<Equals<1, 1>>;
 * type TestEquals5 = AssertTrue<Equals<number[] | string, string | number>>;
 * test TestEquals6 = AssertTrue<Equals<1 | never, 1>>;
 * test TestEquals7 = AssertTrue<Equals<any, 1 | string>>;
 * test TestEquals8 = AssertTrue<Equals<any, any>>;
 * ```
 */
export type Equals<T, U> = And<LooseEquals<T, U>, LooseEquals<IsAny<T>, IsAny<U>>>;

/**
 * @privateRemarks
 *
 * This is simply checking the property `(T ⊆ U && U ⊆ T)`. This generally checks for equality between `T` and `U` but does not handle the case of one being `any` intuitively. See `Equals` for an implementation that fills this hole.
 *
 * The form `type LooseEquals<T, U> = And<Extends<T, U>, Extends<U, T>>;` would more intuitively express how the type works but unfortunately result in a circularly defined type. Because `And<T, U>` is defined requiring `T` equals `true` and `U` equals `true` is internally uses `LooseEquals`.
 *
 * For how `[T] extends [U] ? ... : ...` works @see {@link Extends}
 */
type LooseEquals<T, U> = [T] extends [U] ? ([U] extends [T] ? true : false) : false;

export type Not<T extends boolean> = LooseEquals<T, true> extends true ? false : true;

/**
 * `IsAny<any>` will return true and false for any other value. This is possible because the type `any` has unsound properties no other type has, namely that any type can be assigned to it and it can be assigned to anything (except `never`).
 *
 * @privateRemarks
 *
 * The most intuitive implementation of testing that unsound property would be `And<LooseEquals<T, A>, LooseEquals<T, B>>` where `A != B != never` because `LooseEquals` merely tests that `T` can be assigned to `U` and vice versa and only `any` can be "equal" in that way to two types. We can actually be a bit more efficient with the form `LooseEquals<A, B | T>`
 *
 * To show the soundness of that optimization type, we can step through how this works. In the type we arbitrarily chose `A = 0` and `B = 1` so we'll use that here as well. When `T` is `any`, `B | T` is the same as `1 | any` which is just the same as `any`. `LooseEquals` will return true because `0` extends `any` and `any` extends `0` which is the extent of `LooseEquals`'s implementation.
 *
 * For any other type this property does not hold. Even for one like `T = 0` because `0` does not equal `1 | 0` by this definition because `1 | 0` cannot be assigned to `0` in the case of `1`.
 */
export type IsAny<T> = LooseEquals<0, 1 | T>;

/**
 * Tests if `T` extends `U`. This requires `T` to be a strict subset of `U`. @see {@link ContainsAny} if you want to test if any element of `T` is contained within `U`.
 *
 * @privateRemarks
 * The reason why it's of the form `[T] extends [U]` is to avoid the distributive behavior of conditional types. In isolated contexts, `[T]` would simply represent an array with a single element of type `T`, and so `[T] extends [U]` could simplify to `T extends U`.
 *
 * This does not leverage any special existing properties of single-value arrays but rather is just syntax to disable distribution in conditional types. See: [Typescript's documentation on this property](https://www.typescriptlang.org/docs/handbook/2/conditional-types.html#distributive-conditional-types).
 */
export type Extends<T, U> = [T] extends [U] ? true : false;

export type And<T extends boolean, U extends boolean> = LooseEquals<T, true> extends true
	? LooseEquals<U, true> extends true
		? true
		: false
	: false;

export type AssertTrue<T extends true> = T;
export type AssertFalse<T extends false> = T;

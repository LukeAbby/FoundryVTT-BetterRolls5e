import type { Data as RollData } from "@league-of-foundry-developers/foundry-vtt-types/src/foundry/foundry.js/roll";

/**
 * Creates objects (proxies) which can be used to deserialize flags efficiently.
 * Currently this is used so that Roll objects unwrap properly.
 */
export const FoundryProxy = {
	// A set of all created proxies. Weaksets do not prevent garbage collection,
	// allowing us to safely test if something is a proxy by adding it in here
	proxySet: new WeakSet(),

	/**
	 * Creates a new proxy that turns serialized objects (like rolls) into objects.
	 * Use the result as if it was the original object.
	 */
	create<Data extends Record<never, unknown>>(data: Data): Data {
		const proxy = new Proxy(data, FoundryProxy);
		FoundryProxy.proxySet.add(proxy);

		return proxy as Data;
	},

	get<K extends number | string | symbol, V extends Record<number | string | symbol, unknown>>(
		target: Record<K, V>,
		key: K
	) {
		const value = target[key];

		// Prevent creating the same proxy again
		if (FoundryProxy.proxySet.has(value)) {
			return value;
		}

		if (value !== null && typeof value === "object") {
			if (value.class === "Roll") {
				// This is a serialized roll, convert to roll object
				return Roll.fromData(value as unknown as RollData);
			}

			if (!Object.prototype.hasOwnProperty.call(target, key)) {
				// this is a getter or setter function, so no proxy-ing
				return value;
			}

			// Create a nested proxy, and save the reference
			const proxy = FoundryProxy.create(value);

			// eslint-disable-next-line no-param-reassign
			target[key] = proxy;

			return proxy;
		}

		return value;
	},

	set<K extends number | string | symbol, V>(target: Record<K, V>, key: K, value: V) {
		// eslint-disable-next-line no-param-reassign
		target[key] = value;

		return true;
	},
};

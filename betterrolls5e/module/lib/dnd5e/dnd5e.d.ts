import type * as dice from "./dice5e";

export namespace DND5e {
	export type AbilityActivationTypes =
		| "none"
		| "action"
		| "bonus"
		| "reaction"
		| "minute"
		| "hour"
		| "day"
		| "special"
		| "legendary"
		| "lair"
		| "crew";

	export type TimePeriods =
		| "inst"
		| "turn"
		| "round"
		| "minute"
		| "hour"
		| "day"
		| "month"
		| "year"
		| "perm"
		| "spec";

	export type MovementUnits = "ft" | "mi" | "m" | "km";

	export type DistanceUnits = "none" | "self" | "touch" | "spec" | "any" | MovementUnits;

	/** spell levels can be used to index localisation, pact is not included because that's just translated to a spell level */
	export type SpellLevels = 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9;

	/** This form of spell levels comes in as a string because it's cast that way anyways and includes pact. Overall more "raw". */
	export type UseSpellLevels = `${SpellLevels}` | "pact";

	/** The D&D 5e system decided to append `spell` to the beginning for resource consumption. If you've dug up _why_ adding it here would be great. */
	export type ConsumeSpellLevels = `spell${SpellLevels}` | "pact";

	export type RollTypes =
		| "attack"
		| "damage"
		| "versatile"
		| "formula"
		| "save"
		| "toolCheck"
		| "placeTemplate"
		| "abilityCheck";

	export type ItemActionTypes = "mwak" | "rwak" | "msak" | "rsak" | "save" | "heal" | "abil" | "util" | "other";

	export type ActionTypes = "mwak" | "rwak" | "msak" | "rsak" | "save" | "heal" | "abil" | "util" | "other";

	export type Abilities = "str" | "dex" | "con" | "int" | "wis" | "cha";

	export type TargetTypes =
		| "none"
		| "self"
		| "creature"
		| "ally"
		| "enemy"
		| "object"
		| "space"
		| "radius"
		| "sphere"
		| "cylinder"
		| "cone"
		| "square"
		| "cube"
		| "line"
		| "wall";

	export type SaveScalings = "";

	export type Skills =
		| "acr"
		| "ani"
		| "arc"
		| "ath"
		| "dec"
		| "his"
		| "ins"
		| "itm"
		| "inv"
		| "med"
		| "nat"
		| "prc"
		| "prf"
		| "per"
		| "rel"
		| "slt"
		| "ste"
		| "sur";

	export type ItemType =
		| "weapon"
		| "equipment"
		| "consumable"
		| "tool"
		| "backpack"
		| "loot"
		| "spell"
		| "feat"
		| "class";

	export type ConsumableTypes = "ammo" | "potion" | "poison" | "food" | "scroll" | "wand" | "rod" | "trinket";

	export type DamageTypes =
		| "acid"
		| "bludgeoning"
		| "cold"
		| "fire"
		| "force"
		| "lightning"
		| "necrotic"
		| "piercing"
		| "poison"
		| "psychic"
		| "radiant"
		| "slashing"
		| "thunder";

	export type HealingTypes = "healing" | "temphp";

	export type SpellProgression = "none" | "full" | "half" | "third" | "pact" | "artificer";
	export type SpellScalingModes = "none" | "cantrip" | "level";
	export type SpellPreparationModes = SpellUpcastModes | "atwill" | "innate";
	export type SpellUpcastModes = "always" | "pact" | "prepared";

	export type WeaponTypes = "simpleM" | "simpleR" | "martialM" | "martialR" | "natural" | "improv" | "siege";

	export class AbilityUseDialog extends Dialog {
		/**
		 * A constructor function which displays the Spell Cast Dialog app for a given Actor and Item.
		 * Returns a Promise which resolves to the dialog FormData once the workflow has been completed.
		 * @param item - Item being used.
		 * @returns Promise that is resolved when the use dialog is acted upon.
		 */
		static create(item: Item5e): Promise<null | ReturnType<FormDataExtended["toObject"]>>;
	}

	export type WeaponProperties =
		| "ada"
		| "amm"
		| "fin"
		| "fir"
		| "foc"
		| "hvy"
		| "lgt"
		| "lod"
		| "mgc"
		| "rch"
		| "rel"
		| "ret"
		| "sil"
		| "spc"
		| "thr"
		| "two"
		| "ver";

	export type SpellSchools = "abj" | "con" | "div" | "enc" | "evo" | "ill" | "nec" | "trs";

	export type MiscEquipmentTypes = "clothing" | "trinket" | "vehicle";

	export type ArmorTypes = "light" | "medium" | "heavy" | "natural" | "shield";

	export type EquipmentTypes = MiscEquipmentTypes | ArmorTypes;

	export type ProficiencyLevels = 0 | 1 | 0.5 | 2;

	export type Config = {
		abilityActivationTypes: Record<DND5e.AbilityActivationTypes, string>;
		consumableTypes: Record<DND5e.ConsumableTypes, string>;
		abilities: Record<DND5e.Abilities, string>;
		abilityAbbreviations: Record<DND5e.Abilities, string>;
		targetTypes: Record<DND5e.TargetTypes, string>;
		damageTypes: Record<DND5e.DamageTypes, string>;
		healingTypes: Record<DND5e.HealingTypes, string>;
		spellPreparationModes: Record<DND5e.SpellPreparationModes, string>;
		spellScalingModes: Record<DND5e.SpellScalingModes, string>;
		skills: Record<DND5e.Skills, string>;
		spellProgression: Record<DND5e.SpellProgression, string>;
		spellLevels: Record<DND5e.SpellLevels, string>;
		weaponTypes: Record<DND5e.WeaponTypes, string>;
		spellUpcastModes: DND5e.SpellUpcastModes[];
		weaponProperties: Record<DND5e.WeaponProperties, string>;
		timePeriods: Record<DND5e.TimePeriods, string>;
		movementUnits: Record<DND5e.MovementUnits, string>;
		distanceUnits: Record<DND5e.DistanceUnits, string>;
		itemActionTypes: Record<DND5e.ItemActionTypes, ItemActionTypes>;
		spellSchools: Record<DND5e.SpellSchools, string>;
		miscEquipmentTypes: Record<DND5e.MiscEquipmentTypes, string>;
		armorTypes: Record<DND5e.ArmorTypes, string>;
		equipmentTypes: Record<DND5e.EquipmentTypes, string>;
		proficiencyLevels: Record<DND5e.ProficiencyLevels, string>;
	};

	export type Game = {
		canvas: {
			AbilityTemplate: typeof AbilityTemplate;
		};

		entities: {
			Item5e: typeof Item5e;
			Actor5e: typeof Actor5e;
		};

		applications: {
			AbilityUseDialog: typeof AbilityUseDialog;
		};

		dice: typeof dice;
	};
}

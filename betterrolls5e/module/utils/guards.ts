import type { RenderModelEntries, RenderModelEntry } from "../renderer";
import type { Item5eDataSourceData, Item5eTypes } from "../lib/dnd5e/item5e";
import type { Optional } from "../lib/utils";

import { betterRollsInfo, betterRollsError } from "./logging.js";
import { getGame } from "./readyTools";

export function isDND5eAbility(ability: Optional<string>): ability is DND5e.Abilities {
	return ability != null && Object.keys(CONFIG.DND5E.abilities).includes(ability);
}

export function isDND5eSkill(skill: Optional<string>): skill is DND5e.Skills {
	return skill != null && Object.keys(CONFIG.DND5E.skills).includes(skill);
}

export const rollStates = ["highest", "lowest", "first"] as const;
export function isValidRollState(rollState: string | null | undefined): rollState is RollState {
	return (
		typeof rollState !== "undefined" &&
		(rollState === null || (rollStates as readonly string[]).includes(rollState))
	);
}

export function isCombinedDamageType(damageType: string): damageType is BetterRolls.CombinedDamageTypes {
	const combinedDamageTypes = Object.keys(CONFIG.betterRolls5e.combinedDamageTypes);

	return combinedDamageTypes.includes(damageType);
}

export function combinedDamageTypeErrorMessage(damageType: Optional<string>): string {
	const combinedDamageTypes = Object.keys(CONFIG.betterRolls5e.combinedDamageTypes);

	return `Invalid damage type "${damageType}". Expected one of ${combinedDamageTypes.join(", ")}.`;
}

type ThrowWithContextOptions = {
	message: string[];
	context?: Optional<Parameters<typeof console.info>>;
	errorConstructor?: ConstructorOf<Error>;
};

const preface = "Better Rolls 5e |";

/**
 * Throws an error with additional context prepended.
 */
export function throwWithContext({ message, context, errorConstructor }: ThrowWithContextOptions): never {
	const ErrorConstructor = errorConstructor ?? Error;

	const betterRolls5eModule = getGame().modules.get("betterrolls5e");

	const errorOccurred = `An error has occurred involving Better Rolls 5e.\nConsider filing an issue at: ${betterRolls5eModule?.data.bugs}\nPlease include a series of steps leading to this behavior starting from a clean install and make sure an existing issue does not exist already.`;
	if (context != null) {
		betterRollsError(errorOccurred, "Additional context for the error has been provided below:");

		const [contextMessage, ...params] = context;
		betterRollsError(`${contextMessage}`, ...params);
	} else {
		betterRollsInfo(errorOccurred);
	}

	throw new ErrorConstructor(`${preface} ${message.join(" ")}`);
}

export function isEntryType<Type extends keyof RenderModelEntries>(
	entry: RenderModelEntry,
	type: Type
): entry is RenderModelEntries[Type] {
	return entry.type === type;
}

export function isItemType<ItemType extends Item5eTypes>(
	itemData: Item5eDataSourceData,
	itemType: ItemType
): itemData is Item5eDataSourceData<ItemType> {
	return itemData.type === itemType;
}

import type { ChatSpeakerData } from "@league-of-foundry-developers/foundry-vtt-types/src/foundry/common/data/data.mjs/chatSpeakerData";

import type { betterRolls5eConfig, BetterRolls as _BetterRolls } from "./betterrolls5e";

import type { CustomItemRoll } from "./custom-roll";
import type { BetterRollsChatCard } from "./chat-message";

import type { rollStates } from "./utils/guards";

import type { Optional, InvariantUnion, FullPartial, InvariantUnionDeep, Equals } from "./lib/utils";

import { Actor5e as _Actor5e } from "./lib/dnd5e/actor5e.js";
import { Item5e as _Item5e } from "./lib/dnd5e/item5e.js";

import type { Actor5eDataSourceData, Actor5eData } from "./lib/dnd5e/actor5e";
import type { Item5eDataSourceData, Item5eData } from "./lib/dnd5e/item5e";
import type { DAE } from "./lib/dae";
import { DND5e } from "./lib/dnd5e/dnd5e.js";

export = DND5e;
export as namespace DND5e;

type SourceChatMessage = ChatMessage;

declare global {
	class Item5e extends _Item5e {}
	class Actor5e extends _Actor5e {}

	interface DocumentClassConfig {
		Actor: typeof Actor5e;
		Item: typeof Item5e;
	}

	interface SourceConfig {
		Item: InvariantUnionDeep<Item5eDataSourceData>;
		Actor: InvariantUnionDeep<Actor5eDataSourceData>;
	}

	interface DataConfig {
		Item: InvariantUnionDeep<Item5eData>;
		Actor: InvariantUnionDeep<Actor5eData>;
	}

	// This enables access on `game` etc. at any time. While these are not guaranteed to be initialized until their individual hooks there should be logic in place to give better errors.
	// Canvas is left off as it can always be disabled.
	interface LenientGlobalVariableTypes {
		game: never;
		socket: never;
		ui: never;
	}

	interface Window {
		DAE?: DAE.Window;

		BetterRolls: ReturnType<typeof _BetterRolls>;
	}

	interface CONFIG {
		DND5E: DND5e.Config;

		betterRolls5e: typeof betterRolls5eConfig;
	}

	type NeverOr<T, U> = [T] extends [never] ? U : T;

	type Nullish = null | undefined;

	type ItemOfType<Type extends foundry.data.ItemData["type"]> = Item5e & {
		data: { type: Type; _source: { type: Type } };
	};

	type ActorOfType<Type extends foundry.data.ActorData["type"]> = Actor5e & {
		data: { type: Type; _source: { type: Type } };
	};

	class AbilityTemplate extends MeasuredTemplate {
		/**
		 * A factory method to create an AbilityTemplate instance using provided data from an Item5e instance
		 * @param item - The Item object for which to construct the template
		 * @returns The template object, or null if the item does not produce a template
		 */
		static fromItem(item: Item5e): AbilityTemplate | null;

		/**
		 * Creates a preview of the spell template
		 */
		drawPreview(): void;
	}

	type Formula<D extends object = object> = string | Roll<D>;

	type MergeInto<Obj, Value> = Obj extends unknown
		? {
				[K in keyof Obj]: Obj[K] & Value;
		  }
		: never;

	/**
	 * Evaluated represents a Roll which has had `.evaluate` called on it either directly or indirectly and therefore has properties related to the value(s) rolled.
	 */
	type Evaluated<R extends Roll> = R & {
		get total(): number;
	};

	type ChatSpeakerDataConstructorData = Exclude<ConstructorParameters<typeof ChatSpeakerData>[0], undefined>;

	interface Game {
		dice3d?: Optional<{
			isEnabled: () => boolean;
			showForRoll(
				roll: Roll,
				user: Optional<User>,
				synchronize: boolean,
				users: Optional<User[]>,
				blind: boolean,
				messageID: Optional<string>,
				speaker: Optional<ChatSpeakerDataConstructorData>
			): Promise<boolean>;
		}>;

		// This isn't actually initialized until the init hook.
		dnd5e: DND5e.Game;
	}

	namespace BetterRolls {
		export type CombinedDamageTypes = DND5e.DamageTypes | DND5e.HealingTypes;

		type ChatMessageBase = {
			BetterRoll: CustomItemRoll;
			BetterRollsCardBinding: BetterRollsChatCard;
		};

		export type ChatMessage = SourceChatMessage & ChatMessageBase;
		export type MaybeInitializedChatMessage = SourceChatMessage & FullPartial<ChatMessageBase>;

		type SourceAllFlags = typeof CONFIG.betterRolls5e.allFlags;
		export type AllFlags = InvariantUnionDeep<SourceAllFlags[keyof SourceAllFlags]>;

		export type UseCharge = Partial<{ quantity: boolean; charge: boolean; use: boolean; resource: boolean }>;
	}

	const unset: unique symbol;
	type Unset = typeof unset;

	type Coalesce<T, U> = Equals<T, Unset> extends false ? T : U;

	// eslint-disable-next-line @typescript-eslint/no-explicit-any
	type AnyFunction = (...args: any) => any;

	type AsyncReturnType<T extends AnyFunction> = PromiseUnwrap<ReturnType<T>>;
	type PromiseUnwrap<T> = T extends Promise<infer U> ? U : T;

	namespace Hooks {
		interface StaticCallbacks {
			renderItemSheet5e: (
				app: ItemSheet5e,
				html: JQuery,
				data: AsyncReturnType<ItemSheet5e["getData"]>
			) => boolean;
			renderActorSheet5e: (
				app: ActorSheet5e,
				html: JQuery,
				data: AsyncReturnType<ItemSheet5e["getData"]>
			) => boolean;
			getChatLogEntryContext: (html: JQuery, options: ContextMenuEntry[]) => ContextMenuEntry[];

			updateBetterRolls: (customRoll: CustomItemRoll, content: number) => Promise<void>;
		}
	}

	type RollAttackOptions = {
		event: Event;
	};

	class Proficiency<P extends number = number, M extends number = number, R extends boolean = boolean> {
		constructor(proficiency: P, multiplier: M, roundDown?: R);

		/**
		 * Base proficiency value of the actor.
		 */
		private _baseProficiency: P;

		/**
		 * Value by which to multiply the actor's base proficiency value.
		 */
		public multiplier: M;

		/**
		 * Direction decimal results should be rounded ("up" or "down").
		 */
		public rounding: R extends true ? "down" : "up";
	}

	type ItemSheet5e = ItemSheet & {
		object: Item5e;
	};

	type ActorSheet5e = ActorSheet & {
		object: Actor5e;
	};

	type RollData5e = {
		prof: Proficiency;
	};

	type AllUndefined<T> = {
		[K in keyof T]?: undefined;
	};

	// type MaybeRoll5e = Roll<RollData5e> | Roll<AllUndefined<RollData5e>>;
	// type MaybeRoll5e = Roll5e;
	type Roll5e = Roll<RollData5e>;

	type Actor5eRollData = ReturnType<Actor5e["getRollData"]>;
	type Item5eRollData = ReturnType<Item5e["getRollData"]>;

	class Token5e extends Token {}

	type ReplaceProps<T, U extends Record<never, never>> = {
		[K in keyof T]: K extends keyof U ? U[K] : T[K];
	};

	/**
	 * OmitClass makes a class omitting some properties.
	 */
	type OverrideClass<
		C extends abstract new (...args: unknown[]) => unknown,
		Props extends Record<never, never>
	> = new (...args: ConstructorParameters<C>) => ReplaceProps<InstanceType<C>, Props>;

	type Ignorable<R extends Roll> = R & FullPartial<{ ignored: boolean }>;

	type RollState = typeof rollStates[number] | null;
	type RollType = "check" | "save" | "skill";

	type BasicEvent = InvariantUnion<
		| object
		| { altKey: boolean; shiftKey: boolean; ctrlKey: boolean; metaKey: boolean; clientX: number; clientY: number }
	>;
}

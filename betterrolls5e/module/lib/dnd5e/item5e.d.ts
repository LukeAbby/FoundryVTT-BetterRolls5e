import type { ItemRoll, ItemRollAttack } from "../../patching";
import type { FullPartial, InvariantUnionDeep, Optional } from "../utils";
import type { D20RollOptions } from "./dice5e";
import type { DND5e } from "./dnd5e";

declare namespace Item5eTemplates {
	export type ActivatedEffect = {
		activation: {
			type: DND5e.AbilityActivationTypes;
			cost: number;
			condition: string;
		};

		consume: {
			type: "ammo";
			target: string;
		};

		duration: {
			value: number | null;
			units: DND5e.TimePeriods;
		};

		uses: {
			value: null;
			max: "";
			per: null;
		};

		target: {
			value: null;
			width: null;
			units: "";
			type: DND5e.TargetTypes | "";
		};

		range: {
			value: number | null;
			long: number | null;
			units: DND5e.DistanceUnits;
		};
	};

	export type Action = {
		formula: string;

		ability: DND5e.Abilities | null;

		actionType: DND5e.ItemActionTypes;
		chatFlavor: string;

		critical: {
			threshold: Optional<number>;
			damage: string;
		};

		damage: {
			parts: [string, BetterRolls.CombinedDamageTypes][];
			versatile: string;
		};

		save: {
			ability: DND5e.Abilities | "";
			dc: number;
			scaling: DND5e.SaveScalings;
		};
	};

	export type PhysicalItem = {
		quantity: number;
		weight: number;
		price: number;
		attunement: number;
		equipped: boolean;
		rarity: string;
		identified: boolean;
	};

	export type ItemDescription = {
		description: {
			value: string;
			chat: string;
			unidentified: string;
		};
		source: string;
	};
}

/**
 * Item5eData describes a minimal portion of the entirety of D&D 5e's template.json file
 * Do not rely on this to be complete or up to date. If D&D 5e's template.json is updated this must be updated too.
 */
type Item5eTemplateData = {
	weapon: Item5eTemplates.ActivatedEffect &
		Item5eTemplates.PhysicalItem &
		Item5eTemplates.Action & {
			proficient: boolean;
			weaponType: DND5e.WeaponTypes;
		};

	equipment: Item5eTemplates.PhysicalItem &
		Item5eTemplates.ActivatedEffect &
		Item5eTemplates.Action & {
			armor: {
				type: DND5e.ArmorTypes;
				value: number;
				base: number;
			};
			stealth: boolean;
			proficient: boolean;
		};

	consumable: Item5eTemplates.PhysicalItem &
		Item5eTemplates.ActivatedEffect &
		Item5eTemplates.Action & {
			consumableType: DND5e.ConsumableTypes;
			uses: {
				autoDestroy: boolean;
			};
		};

	tool: Item5eTemplates.PhysicalItem & {
		proficient: DND5e.ProficiencyLevels;
		chatFlavor: string;
		ability: DND5e.Abilities;
	};

	loot: Item5eTemplates.PhysicalItem;

	feat: Item5eTemplates.ActivatedEffect &
		Item5eTemplates.Action & {
			requirements: string;

			recharge: {
				value: number;
				charged: boolean;
			};
		};

	class: Item5eTemplates.ItemDescription & {
		spellcasting: {
			progression: DND5e.SpellProgression;
			ability: DND5e.Abilities;
		};
	};

	spell: Item5eTemplates.ActivatedEffect &
		Item5eTemplates.Action & {
			level: DND5e.SpellLevels;
			school: DND5e.SpellSchools;

			preparation: {
				mode: DND5e.SpellPreparationModes;
				prepared: boolean;
			};

			components: {
				value: string;
				vocal: boolean;
				somatic: boolean;
				material: boolean;
				ritual: boolean;
				concentration: boolean;
			};

			materials: {
				value: string;
				consumed: boolean;
				cost: boolean;
				supply: boolean;
			};

			scaling: {
				mode: DND5e.SpellScalingModes;
				formula: string | null;
			};
		};

	backpack: Item5eTemplates.PhysicalItem;
};

type Item5ePreparedData = {
	weapon: object;
	equipment: object;
	consumable: object;
	tool: object;
	loot: object;
	class: object;
	feat: object;
	spell: FullPartial<{
		castedLevel: DND5e.SpellLevels;
		damageLabel: string;
		isAttack: boolean;
	}>;
	backpack: object;
};

type Item5eBase<Type extends Item5eTypes = Item5eTypes> = Type extends unknown
	? {
			type: Type;

			flags: {
				betterRolls5e?: typeof CONFIG.betterRolls5e.allFlags[`${Type}Flags`];
				maestro?: {
					track?: number;
				};
			};
	  }
	: never;

type GetAttackToHitResult = {
	rollData: ReturnType<Item5e["getRollData"]>;
	parts: string[];
};

export type Item5eDataSourceData<Type extends Item5eTypes = Item5eTypes> = InvariantUnionDeep<
	Type extends unknown
		? Item5eBase<Type> & {
				data: Item5eTemplateData[Type];
		  }
		: never
>;

export type Item5eData<Type extends Item5eTypes = Item5eTypes> = Type extends unknown
	? Item5eBase<Type> & {
			data: Item5eTemplateData[Type] & Item5ePreparedData[Type] & { totalWeight: number };
	  }
	: never;

export type UsageUpdates = {
	itemUpdates: Record<string, string>;
	actorUpdates: Record<string, string>;
	resourceUpdates: Record<string, string>;
};

type GetUsageUpdatesUpdates = Partial<{
	/** Consume quantity of the item if other consumption modes are not available? */
	consumeQuantity: boolean;

	/** Whether the item consumes the recharge mechanic */
	consumeRecharge: boolean;

	/** Whether the item consumes a limited resource */
	consumeResource: boolean;

	/** The category of spell slot to consume, or null */
	consumeSpellLevel: string | null;

	/** Whether the item consumes a limited usage */
	consumeUsage: boolean;
}>;

export type Item5eTypes =
	| "weapon"
	| "equipment"
	| "consumable"
	| "tool"
	| "loot"
	| "class"
	| "spell"
	| "feat"
	| "backpack";

export declare class Item5e extends Item {
	getRollData(): Actor5eRollData & {
		item: Item5e["data"]["data"];
		mod: number;
	};

	abilityMod: DND5e.Abilities | null;
	parent: Actor5e;
	hasAreaTarget: boolean;

	getSaveDC(): number | null;

	/**
	 * Verify that the consumed resources used by an Item are available.
	 * Otherwise display an error and return false.
	 * @returns A set of data changes to apply when the item is used, or false
	 */
	_getUsageUpdates(options: GetUsageUpdatesUpdates): UsageUpdates | false;

	/**
	 * Prepare data needed to roll a tool check and then pass it off to `d20Roll`.
	 * @param options - Roll configuration options provided to the d20Roll function.
	 * @returns A Promise which resolves to the created Roll instance.
	 */
	rollToolCheck<D extends object>(options: D20RollOptions<D>): Promise<Roll<D>>;

	/**
	 * Adjust a cantrip damage formula to scale it for higher level characters and monsters.
	 * @param parts - The original parts of the damage formula.
	 * @param scale - The scaling formula.
	 * @param level - Level at which the spell is being cast.
	 * @param rollData - A data object that should be applied to the scaled damage roll.
	 * @returns The parts of the damage formula with the scaling applied.
	 */
	_scaleCantripDamage<D extends object>(
		parts: string[],
		scale: Optional<string>,
		level: number,
		rollData: D
	): Promise<Roll<D>>;

	/**
	 * Adjust the spell damage formula to scale it for spell level up-casting.
	 * @param parts - The original parts of the damage formula.
	 * @param baseLevel - Default level for the spell.
	 * @param spellLevel - Level at which the spell is being cast.
	 * @param formula - The scaling formula.
	 * @param rollData - A data object that should be applied to the scaled damage roll.
	 * @returns The parts of the damage formula with the scaling applied.
	 */
	_scaleSpellDamage<D extends object>(
		parts: string[],
		baseLevel: number,
		spellLevel: number,
		formula: Optional<string>,
		rollData: D
	): Promise<string[]>;

	roll: ItemRoll;
	rollAttack: ItemRollAttack;

	getAttackToHit(): GetAttackToHitResult | Nullish;

	/**
	 * Does the Item implement an attack roll as part of its usage?
	 */
	get hasAttack(): boolean;
}

/**
 * A specialized Dialog subclass for an extended prompt for Better Rolls
 */
export default class ExtendedPrompt<Options extends Dialog.Options = Dialog.Options> extends Dialog {
	constructor(
		/**
		 * Store a reference to the Item entity being used
		 */
		public item: Item5e,
		dialogData: Dialog.Data,
		options?: Partial<Options>
	) {
		super(dialogData ?? {}, options ?? {});
		this.options.classes = ["dnd5e", "dialog"];
	}
}
